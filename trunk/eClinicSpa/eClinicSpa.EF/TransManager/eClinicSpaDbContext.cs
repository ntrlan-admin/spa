using System.Data.Entity;
using eClinicSpa.Domain.TransManager;

namespace eClinicSpa.EF.TransManager
{
    public class eClinicSpaDbContext : DbContext
    {
        public IModelCreator ModelCreator { get; private set; }

        public eClinicSpaDbContext(IModelCreator modelCreator)
            : base("eClinicSpa")
        {
            ModelCreator = modelCreator;
            //this.Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            ModelCreator.OnModelCreating(modelBuilder);            
        }
    }
}