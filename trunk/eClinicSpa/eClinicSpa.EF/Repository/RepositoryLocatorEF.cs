﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using eClinicSpa.Domain.Repository;
using eClinicSpa.EF.TransManager;

namespace eClinicSpa.EF.Repository
{
    public class RepositoryLocatorEF
        : RepositoryLocatorBase
    {
        private readonly DbContext _dbContext;

        public RepositoryLocatorEF(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        #region Overrides of RepositoryLocatorBase

        public override void FlushModifications()
        {
            base.FlushModifications();
            _dbContext.GetObjectContext().SaveChanges(SaveOptions.DetectChangesBeforeSave | SaveOptions.AcceptAllChangesAfterSave);
        }

        protected override IRepository<TEntity> CreateRepository<TEntity>()
        {
            return new RepositoryEF<TEntity>(_dbContext.Set<TEntity>());
        }

        #endregion
    }
}
