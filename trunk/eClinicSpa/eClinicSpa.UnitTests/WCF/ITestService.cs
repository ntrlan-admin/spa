﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using eClinicSpa.Common.Message;
using eClinicSpa.Common.ServiceContract;
using System.ServiceModel;

namespace eClinicSpa.UnitTests.WCF
{
    [ServiceContract(Namespace = "http://eDirectory/testservices/")]
    public interface ITestService
        : IContract
    {
        [OperationContract]
        DtoResponse MethodThrowsBusinessException();

        [OperationContract]
        DtoResponse MethodReturnsBusinessWarning();

        [OperationContract]
        DtoResponse MethodReturnsApplicationException();
    }
}
