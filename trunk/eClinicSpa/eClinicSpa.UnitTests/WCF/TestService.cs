﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using eClinicSpa.Common.Message;
using eClinicSpa.Domain.AppServices;
using eClinicSpa.Domain.Repository;
using eClinicSpa.Domain.Services;

namespace eClinicSpa.UnitTests.WCF
{
    public class TestService
            : ServiceBase, ITestService
    {
        #region Implementation of ITestService

        public DtoResponse MethodThrowsBusinessException()
        {
            return ExecuteCommand<DtoResponse>(arg => MethodThrowsBusinessExceptionCommand());
        }

        private DtoResponse MethodThrowsBusinessExceptionCommand()
        {
            throw new BusinessException(BusinessExceptionEnum.Operational, "Business Exception was thrown");
        }

        public DtoResponse MethodReturnsBusinessWarning()
        {
            return ExecuteCommand(arg => MethodReturnsBusinessWarningCommand());
        }

        private DtoResponse MethodReturnsBusinessWarningCommand()
        {
            Container.RequestContext.Notifier.AddWarning(BusinessWarningEnum.Operational, "Warning was added");
            return new DtoResponse();
        }

        public DtoResponse MethodReturnsApplicationException()
        {
            return ExecuteCommand<DtoResponse>(arg => MethodReturnsApplicationExceptionCommand());
        }

        private DtoResponse MethodReturnsApplicationExceptionCommand()
        {
            throw new ApplicationException("Application Exception was thrown");
        }

        #endregion
    }
}
