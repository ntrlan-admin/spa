﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using eClinicSpa.Common.DependencyInjection;
using eClinicSpa.Common.Message;
using eClinicSpa.Domain.AppServices;
using eClinicSpa.Domain.Repository;
using eClinicSpa.Domain.TransManager;
using eClinicSpa.NHibernate.BootStrapper;
using eClinicSpa.NHibernate.TransManager;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace eClinicSpa.UnitTests
{
    [TestClass]
    [DeploymentItem("nhibernate.cfg.xml")]
    [DeploymentItem("nhibernate.ce.cfg.xml")]
    [DeploymentItem("eClinicSpa.sdf")]
    public abstract class eClinicSpaTestBase
    {
        [TestInitialize]
        public virtual void TestsInitialize()
        {
            InitialiseDatabase();
        }

        private static void InitialiseDatabase()
        {
            using (var manager = GlobalContext.Instance().TransFactory.CreateManager())
            {
                manager.ExecuteCommand(locator =>
                                           {
                                               var storeInitialiser = locator as IStoreInitialiser;
                                               if (storeInitialiser == null) return null;
                                               storeInitialiser.ConfigureStore();
                                               return new DtoResponse();
                                           });
            }
        }

        [TestCleanup]
        public virtual void TestCleanUp()
        {
            ResetLocator();
        }

        private static void ResetLocator()
        {            
            using (ITransManager manager = GlobalContext.Instance().TransFactory.CreateManager())
            {
                manager.ExecuteCommand(locator =>
                                           {
                                               var resetable = locator as IResetable;
                                               if (resetable == null) return null;
                                               resetable.Reset();
                                               return new DtoResponse();
                                           });
            }
        }
    }
}
