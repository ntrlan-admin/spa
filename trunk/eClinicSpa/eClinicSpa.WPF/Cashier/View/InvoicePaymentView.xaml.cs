﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.MainWorking.Model;
using eClinicSpa.WPF.Cashier.ViewModel;
using eClinicSpa.Common.Dto.Transaction;
using eClinicSpa.Common.Dto.Product;
using eClinicSpa.Common.Constant;

namespace eClinicSpa.WPF.Cashier.View
{
    /// <summary>
    /// Interaction logic for InvoicePaymentView.xaml
    /// </summary>
    public partial class InvoicePaymentView : UserControl, IView
    {
        private static DispatcherTimer timerRefreshInvoiceList = new DispatcherTimer();

        private readonly Lazy<CashierViewModel> viewModel;

        public InvoicePaymentView()
        {
            InitializeComponent();
            viewModel = new Lazy<CashierViewModel>(() => ViewHelper.GetViewModel<CashierViewModel>(this));

            // Hook up the Elapsed event for the timer.
            //timerRefreshInvoiceList.Tick += new EventHandler(RefreshInvoiceListOnTimedEvent);

            // Set the Interval to 10 seconds (10000 milliseconds).
            //timerRefreshInvoiceList.Interval = new TimeSpan(0, 0, 10);
            //timerRefreshInvoiceList.Start();
        }

        private CashierViewModel ViewModel { get { return viewModel.Value; } }

        // RefreshInvoiceList when the Elapsed event is raised.
        private void RefreshInvoiceListOnTimedEvent(object source, EventArgs e)
        {
            ViewModel.RefreshPaymentList();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            timerRefreshInvoiceList.Start();
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            timerRefreshInvoiceList.Stop();
        }
    }
}
