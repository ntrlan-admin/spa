﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Spring.Util;

using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.Cashier.Model;
using eClinicSpa.WPF.Cashier.ViewModel;

namespace eClinicSpa.WPF.Cashier.View
{
    /// <summary>
    /// Interaction logic for InvoiceSearchByDateView.xaml
    /// </summary>
    public partial class InvoiceSearchByDateView : UserControl, IView
    {
        private readonly Lazy<CashierViewModel> viewModel;

        public InvoiceSearchByDateView()
        {
            InitializeComponent();
            viewModel = new Lazy<CashierViewModel>(() => ViewHelper.GetViewModel<CashierViewModel>(this));
        }

        private CashierViewModel ViewModel { get { return viewModel.Value; } }

        private void ByDateInvoiceGrid_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = (e.Row.GetIndex() + 1).ToString();
        }
    }
}
