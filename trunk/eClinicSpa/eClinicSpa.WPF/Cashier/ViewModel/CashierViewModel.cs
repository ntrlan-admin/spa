﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.Common.ServiceContract;
using eClinicSpa.Common.Properties;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.Cashier.View;
using eClinicSpa.WPF.Cashier.Model;
using eClinicSpa.WPF.Services;
using eClinicSpa.WPF.Util;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.InvoiceBilling.View;
using eClinicSpa.WPF.Wokspace.ViewModel;
using eClinicSpa.WPF.ProductCategory.ViewModel;
using eClinicSpa.Common.Dto.Transaction;
using eClinicSpa.Common.Dto.Employee;
using eClinicSpa.Common.Dto.Product;
using eClinicSpa.Common.Constant;

namespace eClinicSpa.WPF.Cashier.ViewModel
{
    public class CashierViewModel : WorkspaceViewModel
    {
        protected readonly ServiceAdapter<IEmployeeService> EmployeeServiceAdapter;
        protected readonly ServiceAdapter<ICustomerService> CustomerServiceAdapter;
        protected readonly ServiceAdapter<IProductCategoryService> ProductCategoryServiceAdapter;
        protected readonly ServiceAdapter<ITransactionService> TransactionServiceAdapter;

        public CashierViewModel(CashierView view)
            : base(view)
        {
            base.DisplayName = eClinicSpaResources.Cashier_Title;
            
            EmployeeServiceAdapter = new ServiceAdapter<IEmployeeService>();
            CustomerServiceAdapter = new ServiceAdapter<ICustomerService>();
            ProductCategoryServiceAdapter = new ServiceAdapter<IProductCategoryService>();
            TransactionServiceAdapter = new ServiceAdapter<ITransactionService>();

            Model = new CashierModel
            {
                InvoiceSearchDate = DateTime.Now,
                IsEnabled = true
            };
        }

        #region Properties

        public CashierModel Model { get; set; }
        public String InvoiceTitle { get { return base.DisplayName; } }
        
        public Decimal CustomerPaid
        { 
            get 
            {
                return Model.CustomerPaid; 
            }
            set
            {
                if (Model.CustomerPaid != value)
                {
                    Model.CustomerPaid = value;
                    RaisePropertyChanged("CustomerPaid");
                    CalculateCustomerChangeAmount();
                }
            }
        }

        public Decimal CustomerChange
        {
            get
            {
                return Model.CustomerChange;
            }
            set
            {
                if (Model.CustomerChange != value)
                {
                    Model.CustomerChange = value;
                    RaisePropertyChanged("CustomerChange");
                }
            }
        }

        public IList<InvoiceDto> InvoiceList 
        {
            get
            {
                return Model.InvoiceList;
            }
            set
            {
                if (Model.InvoiceList != value)
                {
                    Model.InvoiceList = value;
                    RaisePropertyChanged("InvoiceList");
                }
            }
        }

        public InvoiceDto SelectedInvoice
        {
            get { return Model.SelectedInvoice; }
            set
            {
                if (Model.SelectedInvoice != value)
                {
                    Model.SelectedInvoice = value;
                    ChangeProcessInvoice();
                }
            }
        }

        public InvoiceDto ProcessInvoice
        {
            get { return Model.ProcessInvoice; }
            set
            {
                if (Model.ProcessInvoice != value)
                {
                    Model.ProcessInvoice = value;
                    RaisePropertyChanged("ProcessInvoice");
                    ResetCustomerPayment();
                }
            }
        }

        public void NotifyPropertyChanged(string propertyName)
        {
            RaisePropertyChanged(propertyName);
        }

        #endregion

        #region Commands

        private RelayCommand InvoicePreviewCommandInstance;
        public RelayCommand InvoicePreviewCommand
        {
            get { return InvoicePreviewCommandInstance; }
            set
            {
                if (InvoicePreviewCommandInstance != value)
                {
                    InvoicePreviewCommandInstance = value;
                    RaisePropertyChanged("InvoicePreviewCommand");
                }
            }
        }

        private RelayCommand SearchInvoiceCommandInstance;
        public RelayCommand SearchInvoiceCommand
        {
            get
            {
                if (SearchInvoiceCommandInstance != null) return SearchInvoiceCommandInstance;
                SearchInvoiceCommandInstance = new RelayCommand(a => SearchInvoice());
                return SearchInvoiceCommandInstance;
            }
        }
        private void SearchInvoice()
        {
            Model.IsEnabled = false;
            RaisePropertyChanged(() => Model);

            RefreshPaymentList();
            SelectedInvoice = null;
            ProcessInvoice = null;

            Model.IsEnabled = true;
            RaisePropertyChanged(() => Model);
        }

        private RelayCommand ReturnToBillingCommandInstance;
        public RelayCommand ReturnToBillingCommand
        {
            get
            {
                if (ReturnToBillingCommandInstance != null) return ReturnToBillingCommandInstance;
                ReturnToBillingCommandInstance = new RelayCommand(a => ReturnToBilling(),
                    p => ProcessInvoice != null && ProcessInvoice.Status == PaymentStatus.Paying);
                return ReturnToBillingCommandInstance;
            }
        }

        public void ReturnToBilling()
        {
            ProcessInvoice.Status = PaymentStatus.NotPaid;
            TransactionServiceAdapter.Execute(s => s.UpdateInvoice(ProcessInvoice));
            if (UpdateInvoiceRows())
            {
                MessageBox.Show(Application.Current.MainWindow, "Đã trả về Tư vấn", ApplicationInfo.ProductName);
                SearchInvoice();
            }
        }

        private RelayCommand PaymentFinishCommandInstance;
        public RelayCommand PaymentFinishCommand
        {
            get
            {
                if (PaymentFinishCommandInstance != null) return PaymentFinishCommandInstance;
                PaymentFinishCommandInstance = new RelayCommand(a => SaveInvoicePaymentFinish(), p => ProcessInvoice != null && ProcessInvoice.IsValid());
                return PaymentFinishCommandInstance;
            }
        }

        public void SaveInvoicePaymentFinish()
        {
            ProcessInvoice.DocDate = DateTime.Now;
            ProcessInvoice.Status = PaymentStatus.Paid;
            ProcessInvoice.CollectedBy = Model.LoggedInUser;
            ProcessInvoice = TransactionServiceAdapter.Execute(s => s.UpdateInvoice(ProcessInvoice));
            if (UpdateInvoiceRows())
            {
                SearchInvoice();
            }
        }

        #endregion

        #region Private helpers

        private bool UpdateInvoiceRows()
        {
            for (int i = 0; i < ProcessInvoice.InvoiceItems.Count; i++)
            {
                InvoiceRowDto item = ProcessInvoice.InvoiceItems.ElementAt(i);
                try
                {
                    item.NotPaid = false;
                    item = TransactionServiceAdapter.Execute(s => s.UpdateInvoiceRow(item));
                    ProcessInvoice.InvoiceItems[i] = item;
                }
                catch (Exception e)
                {
                    MessageBox.Show(Application.Current.MainWindow,
                                    String.Format("Lỗi xảy ra khi lưu các mục trong phiếu: dòng {0}, sản phẩm/dịch vụ: {1}.\n\n{2}",
                                                item.ItemNo, item.Product.Name,
                                                e.StackTrace),
                                    ApplicationInfo.ProductName);
                    return false;
                }
            }
            return true;
        }

        public void RefreshPaymentList()
        {
            var result = TransactionServiceAdapter.Execute(service => service.FindInvoiceByDateAndStatus(Model.InvoiceSearchDate, PaymentStatus.Paying));
            InvoiceList = result.Invoices;
        }

        private void ChangeProcessInvoice()
        {
            if (Model.SelectedInvoice == null) return;
            ProcessInvoice = TransactionServiceAdapter.Execute(service => service.GetInvoiceById(Model.SelectedInvoice.Id));
            RaisePropertyChanged(() => Model);
        }

        private void CalculateCustomerChangeAmount()
        {
            if (ProcessInvoice == null)
            {
                return;
            }

            CustomerChange = CustomerPaid - ProcessInvoice.PaidAmount;
        }

        private void ResetCustomerPayment()
        {
            CustomerPaid = 0;
            CustomerChange = 0;
        }

        #endregion
    }
}
