﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eClinicSpa.Common.Constant;
using eClinicSpa.Common.Dto.Employee;
using eClinicSpa.Common.Dto.Product;
using eClinicSpa.Common.Dto.Transaction;

namespace eClinicSpa.WPF.Cashier.Model
{
    public class CashierModel
    {
        public CashierModel()
        {
        }
        
        public EmployeeDto LoggedInUser { get; set; }

        public DateTime InvoiceSearchDate { get; set; }
        public IList<InvoiceDto> InvoiceList { get; set; }
        public InvoiceDto SelectedInvoice { get; set; }
        public InvoiceDto ProcessInvoice { get; set; }

        public Decimal CustomerPaid { get; set; }
        public Decimal CustomerChange { get; set; }

        public bool IsEnabledPrinting { get { return ProcessInvoice != null; } }

        public bool IsEnabled { get; set; }
    }
}
