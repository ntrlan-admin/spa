﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eClinicSpa.Common.Constant;
using eClinicSpa.Common.Dto.Employee;
using eClinicSpa.Common.Dto.Product;
using eClinicSpa.Common.Dto.Transaction;

namespace eClinicSpa.WPF.Product.Model
{
    public class ProductModel
    {
        public string SearchFilter { get; set; }
        public IList<ProductDto> ProductList { get; set; }
        public ProductDto SelectedProduct { get; set; }
        public ProductDto EditingProduct { get; set; }

        public bool IsEnabled { get; set; }
    }
}
