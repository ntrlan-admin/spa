﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.Common.ServiceContract;
using eClinicSpa.Common.Properties;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.Services;
using eClinicSpa.WPF.Util;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Wokspace.ViewModel;
using eClinicSpa.Common.Dto.Transaction;
using eClinicSpa.Common.Dto.Employee;
using eClinicSpa.Common.Dto.Product;
using eClinicSpa.Common.Constant;
using eClinicSpa.WPF.Product.View;
using eClinicSpa.WPF.Product.Model;
using System.Windows;
using eClinicSpa.Common.Dto.ProductCategory;

namespace eClinicSpa.WPF.Product.ViewModel
{
    public class ProductManageViewModel : WorkspaceViewModel
    {
        protected readonly ServiceAdapter<IProductCategoryService> ProductCategoryServiceAdapter;

        public ProductManageViewModel(ProductManageView view)
            : base(view)
        {
            base.DisplayName = eClinicSpaResources.Products;
            
            ProductCategoryServiceAdapter = new ServiceAdapter<IProductCategoryService>();

            Model = new ProductModel
            {
                EditingProduct = new ProductDto(),
                IsEnabled = true
            };
            ProductCategoryList = ProductCategoryServiceAdapter.Execute(s => s.FindAllProductCategory()).ProductCategories;
            SearchProduct();
        }

        #region Properties

        public ProductModel Model { get; set; }

        public IList<ProductDto> ProductList 
        {
            get
            {
                return Model.ProductList;
            }
            set
            {
                if (Model.ProductList != value)
                {
                    Model.ProductList = value;
                    RaisePropertyChanged("ProductList");
                }
            }
        }

        public IList<ProductStatus> ProductStatusList { get { return AllProductStatus.ProductStatusList; } }

        public IList<ProductCategoryDto> ProductCategoryList { get; private set; }

        public ProductDto SelectedProduct
        {
            get { return Model.SelectedProduct; }
            set
            {
                if (Model.SelectedProduct != value)
                {
                    Model.SelectedProduct = value;
                    ChangeEditingProduct();
                }
            }
        }

        public ProductDto EditingProduct
        {
            get { return Model.EditingProduct; }
            set
            {
                if (Model.EditingProduct != value)
                {
                    Model.EditingProduct = value;
                    RaisePropertyChanged("EditingProduct");
                }
            }
        }

        #endregion

        #region Commands

        private RelayCommand CreateCustomerCmdInstance;
        public RelayCommand CreateProductCommand
        {
            get
            {
                if (CreateCustomerCmdInstance != null) return CreateCustomerCmdInstance;
                CreateCustomerCmdInstance = new RelayCommand(a => CreateNewProduct());
                return CreateCustomerCmdInstance;
            }
        }

        private RelayCommand SaveProductCmdInstance;
        public RelayCommand SaveProductCommand
        {
            get
            {
                if (SaveProductCmdInstance != null) return SaveProductCmdInstance;
                SaveProductCmdInstance = new RelayCommand(a => SaveProduct(),
                                                          p => Model.EditingProduct != null && Model.EditingProduct.IsValid());

                return SaveProductCmdInstance;
            }
        }

        private RelayCommand SearchProductCommandInstance;
        public RelayCommand SearchProductCommand
        {
            get
            {
                if (SearchProductCommandInstance != null) return SearchProductCommandInstance;
                SearchProductCommandInstance = new RelayCommand(a => SearchProduct());
                return SearchProductCommandInstance;
            }
        }

        #endregion

        #region Private helpers

        private void SaveProduct()
        {
            try
            {
                if (Model.EditingProduct.Id == 0)
                {
                    Model.EditingProduct.CreatedOn = DateTime.Now;
                    Model.EditingProduct.ModifiedOn = Model.EditingProduct.CreatedOn;
                    Model.EditingProduct = ProductCategoryServiceAdapter.Execute(s => s.CreateNewProduct(Model.EditingProduct));
                }
                else
                {
                    Model.EditingProduct.ModifiedOn = DateTime.Now;
                    Model.EditingProduct = ProductCategoryServiceAdapter.Execute(s => s.UpdateProduct(Model.EditingProduct));
                }
                RaisePropertyChanged("EditingProduct");
                MessageBox.Show(Application.Current.MainWindow, "Đã lưu dữ liệu.",
                                ApplicationInfo.ProductName);
                SearchProduct();
            }
            catch (Exception e)
            {
                MessageBox.Show(Application.Current.MainWindow,
                                "Có lỗi xảy ra, không lưu được dữ liệu.\n\n" + e.StackTrace,
                                ApplicationInfo.ProductName);
            }
        }

        private void CreateNewProduct()
        {
            EditingProduct = new ProductDto { Status = ProductStatus.Active };
            RaisePropertyChanged(() => Model);
        }

        private void SearchProduct()
        {
            Model.IsEnabled = false;
            RaisePropertyChanged(() => Model);

            RefreshProductList();
            //SelectedProduct = null;
            //EditingProduct = null;

            Model.IsEnabled = true;
            RaisePropertyChanged(() => Model);
        }

        public void RefreshProductList()
        {
            if (string.IsNullOrEmpty(Model.SearchFilter))
            {
                var result = ProductCategoryServiceAdapter.Execute(service => service.FindAllProduct());
                ProductList = result.Products;
            }
            else
            {
                var result = ProductCategoryServiceAdapter.Execute(service => service.FindByFilter(Model.SearchFilter.Trim()));
                ProductList = result.Products;
            }
        }

        private void ChangeEditingProduct()
        {
            if (Model.SelectedProduct == null) return;
            EditingProduct = ProductCategoryServiceAdapter.Execute(service => service.GetProductById(Model.SelectedProduct.Id));
        }

        #endregion
    }
}
