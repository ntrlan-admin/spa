﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Practices.Unity;


namespace eClinicSpa.WPF.Applications
{
    /// <summary>
    /// Provides helper methods that perform common tasks involving a viewmodel.
    /// </summary>
    public static class ContainerHelper
    {
        readonly static IUnityContainer _container = new UnityContainer();

        static ContainerHelper()
        {
            ////IUnityContainer container = new UnityContainerFactory().CreateConfiguredContainer();
            //_container
            //    .RegisterType<IUnitOfWork, COMESDbContext>()
            //    .RegisterType<ICategoryRepository, CategoryRepository>()
            //    .RegisterType<ICategoryServices, CategoryServices>();
        }

        public static IUnityContainer Container { get { return _container; } }
    }
}
