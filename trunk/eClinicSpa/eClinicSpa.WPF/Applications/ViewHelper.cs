﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Windows.Threading;
using System.Windows.Controls;

using Microsoft.Practices.Unity;

using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.ProductCategory.View;
using eClinicSpa.WPF.MainWorking.View;
using eClinicSpa.WPF.Wokspace.ViewModel;
using eClinicSpa.WPF.ProductCategory.ViewModel;
using eClinicSpa.WPF.MainWorking.ViewModel;
using eClinicSpa.WPF.InvoiceBilling.View;
using eClinicSpa.WPF.InvoiceBilling.ViewModel;
using eClinicSpa.WPF.Cashier.ViewModel;
using eClinicSpa.WPF.Cashier.View;
using eClinicSpa.WPF.Report.View;
using eClinicSpa.WPF.Report.ViewModel;

namespace eClinicSpa.WPF.Applications
{
    /// <summary>
    /// Provides helper methods that perform common tasks involving a view.
    /// </summary>
    public static class ViewHelper
    {
        /// <summary>
        /// Gets the ViewModel which is associated with the specified view.
        /// </summary>
        /// <param name="view">The view.</param>
        /// <returns>The associated ViewModel, or <c>null</c> when no ViewModel was found.</returns>
        /// <exception cref="ArgumentNullException">view must not be <c>null</c>.</exception>
        public static WorkspaceViewModel GetViewModel(this IView view)
        {
            if (view == null) { throw new ArgumentNullException("view"); }

            object dataContext = view.DataContext;
            // When the DataContext is null then it might be that the ViewModel hasn't set it yet.
            // Enforce it by executing the event queue of the Dispatcher.
            if (dataContext == null && SynchronizationContext.Current is DispatcherSynchronizationContext)
            {
                DispatcherHelper.DoEvents();
                dataContext = view.DataContext;
            }
            return dataContext as WorkspaceViewModel;
        }
        
        /// <summary>
        /// Gets the ViewModel which is associated with the specified view.
        /// </summary>
        /// <typeparam name="T">The type of the ViewModel</typeparam>
        /// <param name="view">The view.</param>
        /// <returns>The associated ViewModel, or <c>null</c> when no ViewModel was found.</returns>
        /// <exception cref="ArgumentNullException">view must not be <c>null</c>.</exception>
        public static T GetViewModel<T>(this IView view) where T : WorkspaceViewModel
        {
            return GetViewModel(view) as T;
        }

        public static IView CreateView<T>() where T : WorkspaceViewModel
        {
            var viewModel = ContainerHelper.Container.Resolve<T>();
            IView view = (viewModel as WorkspaceViewModel).View as IView;
            view.DataContext = viewModel;

            return view;
        }
    }
}
