﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.MainWorking.Model;
using eClinicSpa.WPF.InvoiceBilling.ViewModel;
using eClinicSpa.Common.Dto.Transaction;
using eClinicSpa.Common.Dto.Product;
using eClinicSpa.Common.Constant;
using System.Data;
using Microsoft.Reporting.WinForms;
using eClinicSpa.WPF.InvoiceBilling.Model;
using System.IO;
using System.Drawing.Printing;

namespace eClinicSpa.WPF.InvoiceBilling.View
{
    /// <summary>
    /// Interaction logic for InvoicePrintPreview.xaml
    /// </summary>
    public partial class InvoicePrintPreview : UserControl, IView
    {
        private readonly Lazy<InvoicePrintViewModel> viewModel;

        public InvoicePrintPreview()
        {
            InitializeComponent();
            viewModel = new Lazy<InvoicePrintViewModel>(() => ViewHelper.GetViewModel<InvoicePrintViewModel>(this));
        }

        private InvoicePrintViewModel ViewModel { get { return viewModel.Value; } }

        public void ShowReport(ReportPrintPaperSize paperSize)
        {
            if (_reportViewer.LocalReport.DataSources.Count > 0)
            {
                this._reportViewer.LocalReport.DataSources.RemoveAt(0);
            }
            ReportDataSource reportDataSource = GetReportDataSource();

            this._reportViewer.LocalReport.DataSources.Add(reportDataSource);
            switch (paperSize)
            {
                case ReportPrintPaperSize.A4:
                    this._reportViewer.LocalReport.ReportEmbeddedResource = "eClinicSpa.WPF.InvoiceBilling.View.InvoiceBilling.rdlc";
                    this._reportViewer.PrinterSettings.DefaultPageSettings.PaperSize = new PaperSize();
                    this._reportViewer.PrinterSettings.Collate = true;
                    break;
                case ReportPrintPaperSize.A6:
                    this._reportViewer.LocalReport.ReportEmbeddedResource = "eClinicSpa.WPF.InvoiceBilling.View.InvoicePrint.rdlc";
                    //this._reportViewer.PrinterSettings.DefaultPageSettings.PaperSize = new PaperSize("Receipt - Roll Feed", 295, 11693);
                    //this._reportViewer.PrinterSettings.Collate = true;
                    break;
                default:
                    break;
            }
            this._reportViewer.LocalReport.SetParameters(new ReportParameter("ReportTitle", ViewModel.InvoiceTitle.ToUpperInvariant()));
            this._reportViewer.LocalReport.SetParameters(new ReportParameter("InvoiceId", ViewModel.PrintInvoice.Id.ToString()));
            this._reportViewer.LocalReport.SetParameters(new ReportParameter("InvoiceDate", String.Format("{0:dd/MM/yyy HH:mm}", ViewModel.PrintInvoice.DocDate)));
            this._reportViewer.LocalReport.SetParameters(new ReportParameter("CustomerId", ViewModel.PrintTransaction.Customer.Id.ToString()));
            this._reportViewer.LocalReport.SetParameters(new ReportParameter("CustomerName", String.Format("{0} {1}", ViewModel.PrintTransaction.Customer.LastName, ViewModel.PrintTransaction.Customer.FirstName)));
            this._reportViewer.LocalReport.SetParameters(new ReportParameter("CustomerAge", ViewModel.PrintTransaction.Customer.Age.ToString()));
            //if (ViewModel.Model.EditTransaction.Status < CustomerTransactionStatus.Finished &&
            //    ViewModel.Model.InvoiceType == InvoiceConstant.INVOICE && ViewModel.PrintInvoice.IsPaidItems)
            //{
            //    this._reportViewer.LocalReport.SetParameters(new ReportParameter("CustomerDiscount", "0"));
            //    this._reportViewer.LocalReport.SetParameters(new ReportParameter("CustomerDeposit", "0"));
            //}
            //else
            //{
            //    this._reportViewer.LocalReport.SetParameters(new ReportParameter("CustomerDiscount", ViewModel.PrintInvoice.CustomerDiscount.ToString()));
            //    this._reportViewer.LocalReport.SetParameters(new ReportParameter("CustomerDeposit", ViewModel.PrintInvoice.Deposit.ToString()));
            //}
            this._reportViewer.LocalReport.SetParameters(new ReportParameter("CustomerDiscount", ViewModel.PrintInvoice.CustomerDiscount.ToString()));
            this._reportViewer.LocalReport.SetParameters(new ReportParameter("CustomerDeposit", ViewModel.PrintInvoice.Deposit.ToString()));
            this._reportViewer.LocalReport.SetParameters(new ReportParameter("DepositDate", String.Format("{0:dd/MM/yyy}", ViewModel.PrintInvoice.DepositDate)));
            this._reportViewer.LocalReport.SetParameters(new ReportParameter("PaidAmount", ViewModel.PrintInvoice.PaidAmount.ToString()));

            this._reportViewer.LocalReport.SetParameters(new ReportParameter("TherapistId", ViewModel.PrintTransaction.Customer.Therapist.Id.ToString()));
            this._reportViewer.LocalReport.SetParameters(new ReportParameter("TherapistName", ViewModel.PrintTransaction.Customer.Therapist.Name));
            this._reportViewer.LocalReport.SetParameters(new ReportParameter("ConsultantId", ViewModel.PrintTransaction.Customer.TreatmentConsultant.Id.ToString()));
            this._reportViewer.LocalReport.SetParameters(new ReportParameter("ConsultantName", ViewModel.PrintTransaction.Customer.TreatmentConsultant.Name));

            this._reportViewer.LocalReport.Refresh();
            _reportViewer.RefreshReport();
        }

        private ReportDataSource GetReportDataSource()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("ItemNo", typeof(int)));
            dt.Columns.Add(new DataColumn("Product", typeof(string)));
            dt.Columns.Add(new DataColumn("UnitPrice", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Quantity", typeof(int)));
            dt.Columns.Add(new DataColumn("ProductDiscountRate", typeof(string)));
            dt.Columns.Add(new DataColumn("TotalAmount", typeof(decimal)));

            if (ViewModel.PrintInvoice.InvoiceItems != null)
            {
                foreach (InvoiceRowDto i in ViewModel.PrintInvoice.InvoiceItems)
                {
                    if (ViewModel.Model.InvoiceType != InvoiceConstant.INVOICE && i.NotPaid == false)
                    {
                        i.Amount = i.UnitPrice * i.Quantity;
                        i.ProductDiscountAmount = i.Amount * i.ProductDiscountRate;
                    }
                    if (ViewModel.Model.EditTransaction.Status == CustomerTransactionStatus.Finished || 
                        ViewModel.Model.InvoiceType != InvoiceConstant.INVOICE || i.NotPaid)
                    {
                        DataRow dr = dt.NewRow();
                        dr["ItemNo"] = i.ItemNo;
                        dr["Product"] = i.Product.Name;
                        dr["UnitPrice"] = i.UnitPrice;
                        dr["Quantity"] = i.Quantity;
                        dr["ProductDiscountRate"] = i.ProductDiscountType;
                        dr["TotalAmount"] = i.TotalAmount;
                        dt.Rows.Add(dr);
                    }
                }
            }

            ReportDataSource reportDataSource = new ReportDataSource();
            reportDataSource.Name = "InvoiceDataSet"; // Name of the DataSet we set in .rdlc
            reportDataSource.Value = dt;
            return reportDataSource;
        }
    }
}
