﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.MainWorking.Model;
using eClinicSpa.WPF.InvoiceBilling.ViewModel;
using eClinicSpa.Common.Dto.Transaction;
using eClinicSpa.Common.Dto.Product;
using eClinicSpa.Common.Constant;

namespace eClinicSpa.WPF.InvoiceBilling.View
{
    /// <summary>
    /// Interaction logic for StockOutView.xaml
    /// </summary>
    public partial class StockOutView : UserControl, IView
    {
        private readonly Lazy<StockOutViewModel> viewModel;

        public StockOutView()
        {
            InitializeComponent();
            viewModel = new Lazy<StockOutViewModel>(() => ViewHelper.GetViewModel<StockOutViewModel>(this));
        }

        private StockOutViewModel ViewModel { get { return viewModel.Value; } }
    }
}
