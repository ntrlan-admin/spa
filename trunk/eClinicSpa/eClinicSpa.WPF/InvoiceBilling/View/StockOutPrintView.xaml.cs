﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.MainWorking.Model;
using eClinicSpa.WPF.InvoiceBilling.ViewModel;
using eClinicSpa.Common.Dto.Transaction;
using eClinicSpa.Common.Dto.Product;
using eClinicSpa.Common.Constant;
using System.Data;
using Microsoft.Reporting.WinForms;
using eClinicSpa.WPF.InvoiceBilling.Model;
using System.Drawing.Printing;
using System.Drawing;

namespace eClinicSpa.WPF.InvoiceBilling.View
{
    /// <summary>
    /// Interaction logic for StockOutPrintView.xaml
    /// </summary>
    public partial class StockOutPrintView : UserControl, IView
    {
        private readonly Lazy<StockOutPrintViewModel> viewModel;

        public StockOutPrintView()
        {
            InitializeComponent();
            viewModel = new Lazy<StockOutPrintViewModel>(() => ViewHelper.GetViewModel<StockOutPrintViewModel>(this));
        }

        private StockOutPrintViewModel ViewModel { get { return viewModel.Value; } }

        public void ShowReport()
        {
            ReportDataSource reportDataSource = GetReportDataSource();

            this._reportViewer.LocalReport.DataSources.Clear();
            this._reportViewer.LocalReport.DataSources.Add(reportDataSource);

            this._reportViewer.LocalReport.ReportEmbeddedResource = "eClinicSpa.WPF.InvoiceBilling.View.StockOutPrint.rdlc";
            
            this._reportViewer.LocalReport.SetParameters(new ReportParameter("ReportTitle", ViewModel.InvoiceTitle.ToUpperInvariant()));
            this._reportViewer.LocalReport.SetParameters(new ReportParameter("InvoiceId", ViewModel.PrintInvoice.Id.ToString()));
            this._reportViewer.LocalReport.SetParameters(new ReportParameter("InvoiceDate", String.Format("{0:dd/MM/yyy HH:mm}", ViewModel.PrintInvoice.DocDate)));
            this._reportViewer.LocalReport.SetParameters(new ReportParameter("CustomerId", ViewModel.PrintTransaction.Customer.Id.ToString()));
            this._reportViewer.LocalReport.SetParameters(new ReportParameter("CustomerName", String.Format("{0} {1}", ViewModel.PrintTransaction.Customer.LastName, ViewModel.PrintTransaction.Customer.FirstName)));
            this._reportViewer.LocalReport.SetParameters(new ReportParameter("CustomerAge", ViewModel.PrintTransaction.Customer.Age.ToString()));
            this._reportViewer.LocalReport.SetParameters(new ReportParameter("CustomerDiscount", ViewModel.PrintInvoice.CustomerDiscount.ToString()));
            this._reportViewer.LocalReport.SetParameters(new ReportParameter("CustomerDeposit", ViewModel.PrintInvoice.Deposit.ToString()));

            this._reportViewer.LocalReport.SetParameters(new ReportParameter("TherapistId", ViewModel.PrintTransaction.Customer.Therapist.Id.ToString()));
            this._reportViewer.LocalReport.SetParameters(new ReportParameter("TherapistName", ViewModel.PrintTransaction.Customer.Therapist.Name));
            this._reportViewer.LocalReport.SetParameters(new ReportParameter("ConsultantId", ViewModel.PrintTransaction.Customer.TreatmentConsultant.Id.ToString()));
            this._reportViewer.LocalReport.SetParameters(new ReportParameter("ConsultantName", ViewModel.PrintTransaction.Customer.TreatmentConsultant.Name));

            this._reportViewer.PrinterSettings.DefaultPageSettings.PaperSize = new PaperSize("Receipt - Roll Feed", 295, 11693);
            this._reportViewer.PrinterSettings.Collate = true;

            //this._reportViewer.SetDisplayMode(DisplayMode.PrintLayout);
            //this._reportViewer.ZoomMode = ZoomMode.Percent;
            //this._reportViewer.ZoomPercent = 100;

            this._reportViewer.LocalReport.Refresh();
            _reportViewer.RefreshReport();
        }

        private ReportDataSource GetReportDataSource()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("ItemNo", typeof(int)));
            dt.Columns.Add(new DataColumn("Product", typeof(string)));
            dt.Columns.Add(new DataColumn("Quantity", typeof(int)));

            if (ViewModel.PrintInvoice.InvoiceItems != null)
            {
                foreach (InvoiceRowDto i in ViewModel.PrintInvoice.InvoiceItems)
                {
                    DataRow dr = dt.NewRow();
                    dr["ItemNo"] = i.ItemNo;
                    dr["Product"] = i.Product.Name;
                    dr["Quantity"] = i.Quantity;
                    dt.Rows.Add(dr);
                }
            }

            ReportDataSource reportDataSource = new ReportDataSource();
            reportDataSource.Name = "InvoiceDataSet"; // Name of the DataSet we set in .rdlc
            reportDataSource.Value = dt;
            return reportDataSource;
        }
    }
}
