﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.MainWorking.Model;
using eClinicSpa.WPF.InvoiceBilling.ViewModel;
using eClinicSpa.Common.Dto.Transaction;
using eClinicSpa.Common.Dto.Product;
using eClinicSpa.Common.Constant;

namespace eClinicSpa.WPF.InvoiceBilling.View
{
    /// <summary>
    /// Interaction logic for InvoiceBillingView.xaml
    /// </summary>
    public partial class InvoiceBillingView : UserControl, IView
    {
        private readonly Lazy<InvoiceBillingViewModel> viewModel;

        public InvoiceBillingView()
        {
            InitializeComponent();
            viewModel = new Lazy<InvoiceBillingViewModel>(() => ViewHelper.GetViewModel<InvoiceBillingViewModel>(this));
        }

        private InvoiceBillingViewModel ViewModel { get { return viewModel.Value; } }

        public void ReCalculateItemTotalAmount()
        {
            if (ViewModel.EditInvoiceItem.NotPaid)
            {
                ViewModel.EditInvoiceItem.Amount = ViewModel.EditInvoiceItem.UnitPrice * ViewModel.EditInvoiceItem.Quantity;
                ViewModel.EditInvoiceItem.ProductDiscountAmount = ViewModel.EditInvoiceItem.Amount * ViewModel.EditInvoiceItem.ProductDiscountRate;
            }
            else
            {
                ViewModel.EditInvoiceItem.Amount = 0;
                ViewModel.EditInvoiceItem.ProductDiscountAmount = 0;
            }
            ViewModel.NotifyPropertyChanged("InvoiceItemList");
            ViewModel.NotifyPropertyChanged("EditInvoice");
        }

        private void Cbo_Product_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ViewModel.EditInvoiceItem == null) return;

            var product = (sender as ComboBox).SelectedItem;
            ViewModel.EditInvoiceItem.UnitPrice = (product as ProductDto).Price;

            ReCalculateItemTotalAmount();
        }

        private void Cbo_ProductDiscount_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ViewModel.EditInvoiceItem == null) return;
            
            var productDiscount = (sender as ComboBox).SelectedItem;
            ViewModel.EditInvoiceItem.ProductDiscountRate = (productDiscount as ProductDiscount).Rate;

            ReCalculateItemTotalAmount();
        }

        private void TextBox_Quantity_LostFocus(object sender, RoutedEventArgs e)
        {
            if (ViewModel.EditInvoiceItem == null) return;

            if (ViewModel.EditInvoiceItem.Amount !=
                ViewModel.EditInvoiceItem.UnitPrice * ViewModel.EditInvoiceItem.Quantity)
            {
                ReCalculateItemTotalAmount();
            }
        }

        private void TextBox_DiscountAmount_LostFocus(object sender, RoutedEventArgs e)
        {
            if (ViewModel.EditInvoiceItem == null) return;

            if (ViewModel.EditInvoiceItem.ProductDiscountAmount !=
                ViewModel.EditInvoiceItem.Amount * ViewModel.EditInvoiceItem.ProductDiscountRate)
            {
                ViewModel.NotifyPropertyChanged("InvoiceItemList");
            }
        }

        private void TextBox_PaymentAmount_LostFocus(object sender, RoutedEventArgs e)
        {
            ViewModel.NotifyPropertyChanged("EditInvoice");
        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox_SelectAll(sender);
        }

        private void TextBox_GotMouseCapture(object sender, MouseEventArgs e)
        {
            TextBox_SelectAll(sender);
        }

        private void TextBox_SelectAll(object sender)
        {
            TextBox tb = (sender as TextBox);

            if (tb != null)
            {
                tb.SelectAll();
            }
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            ReCalculateItemTotalAmount();
        }
    }
}
