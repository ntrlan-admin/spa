﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.Common.ServiceContract;
using eClinicSpa.Common.Properties;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.MainWindow.View;
using eClinicSpa.WPF.MainWindow.Model;
using eClinicSpa.WPF.Services;
using eClinicSpa.WPF.Util;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.InvoiceBilling.View;
using eClinicSpa.WPF.MainWorking.View;
using eClinicSpa.WPF.Wokspace.ViewModel;
using eClinicSpa.WPF.ProductCategory.ViewModel;
using eClinicSpa.WPF.MainWorking.ViewModel;
using eClinicSpa.WPF.InvoiceBilling.Model;
using eClinicSpa.Common.Dto.Transaction;
using eClinicSpa.Common.Dto.Employee;
using eClinicSpa.Common.Dto.Product;
using eClinicSpa.Common.Constant;
using System.Windows;

namespace eClinicSpa.WPF.InvoiceBilling.ViewModel
{
    public class StockOutViewModel : InvoiceBaseViewModel
    {
        public StockOutViewModel(StockOutView view)
            : base(view)
        {
            base.DisplayName = eClinicSpaResources.InvoiceBilling_Stock;

            Model.InvoiceType = InvoiceConstant.STOCK;
            RaisePropertyChanged(() => Model);
        }

        #region Commands

        private RelayCommand TransferToStockOutCommandInstance;
        public RelayCommand TransferToStockOutCommand
        {
            get
            {
                if (TransferToStockOutCommandInstance != null) return TransferToStockOutCommandInstance;
                TransferToStockOutCommandInstance = new RelayCommand(a => TransferToStockOut(),
                    p => EditInvoice != null && EditInvoice.IsValid() &&
                        (EditInvoice.DocType == InvoiceConstant.STOCK) &&
                        EditInvoice.InvoiceItems != null && EditInvoice.InvoiceItems.Any() &&
                        EditInvoice.Status < PaymentStatus.PrepareStockOut);
                return TransferToStockOutCommandInstance;
            }
        }

        public void TransferToStockOut()
        {
            EditInvoice.Status = PaymentStatus.PrepareStockOut;
            SaveInvoice(false);
            MessageBox.Show(Application.Current.MainWindow, "Đã chuyển đến Múc mâm", ApplicationInfo.ProductName);
        }

        #endregion
    }
}
