﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.Common.ServiceContract;
using eClinicSpa.Common.Properties;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.MainWindow.View;
using eClinicSpa.WPF.MainWindow.Model;
using eClinicSpa.WPF.Services;
using eClinicSpa.WPF.Util;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.InvoiceBilling.View;
using eClinicSpa.WPF.MainWorking.View;
using eClinicSpa.WPF.Wokspace.ViewModel;
using eClinicSpa.WPF.ProductCategory.ViewModel;
using eClinicSpa.WPF.MainWorking.ViewModel;
using eClinicSpa.WPF.InvoiceBilling.Model;
using eClinicSpa.Common.Dto.Transaction;
using eClinicSpa.Common.Dto.Employee;
using eClinicSpa.Common.Dto.Product;
using eClinicSpa.Common.Constant;

namespace eClinicSpa.WPF.InvoiceBilling.ViewModel
{
    public class InvoicePrintBaseViewModel : WorkspaceViewModel
    {
        protected readonly ServiceAdapter<ITransactionService> TransactionServiceAdapter;

        public InvoicePrintBaseViewModel(IView view)
            : base(view)
        {
            base.DisplayName = eClinicSpaResources.InvoicePrintView_Title;

            TransactionServiceAdapter = new ServiceAdapter<ITransactionService>();
            Model = new InvoiceBillingModel();
        }


        #region Properties

        public InvoiceBillingModel Model { get; set; }

        public string InvoiceTitle
        {
            get
            {
                switch (Model.InvoiceType)
                {
                    case InvoiceConstant.STOCK:
                        return eClinicSpaResources.InvoiceBilling_Stock;
                    case InvoiceConstant.REFERENCE:
                        return eClinicSpaResources.InvoiceBilling_Reference;
                    case InvoiceConstant.INVOICE:
                        return eClinicSpaResources.InvoiceBilling_Invoice;
                    default:
                        return eClinicSpaResources.InvoiceBilling_Consultant;
                }
            }
        }

        public TransactionDto PrintTransaction
        {
            get { return Model.EditTransaction; }
            set
            {
                if (Model.EditTransaction != value)
                {
                    Model.EditTransaction = value;
                }
                ChangeTransaction();
            }
        }

        public InvoiceDto PrintInvoice
        {
            get { return Model.EditInvoice; }
            set
            {
                if (value != null && Model.EditInvoice != value)
                {
                    Model.EditInvoice = value;
                }
            }
        }

        #endregion

        #region Private helpers

        private void ChangeTransaction()
        {
            if (PrintTransaction == null)
            {
                return;
            }
            PrintInvoice = TransactionServiceAdapter.Execute(service => 
                service.GetInvoiceByTransactionAndType(PrintTransaction.Id,
                    Model.InvoiceType == InvoiceConstant.INVOICE ? InvoiceConstant.BILLING : Model.InvoiceType));
        }

        #endregion
    }
}
