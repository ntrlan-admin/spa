﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.Common.ServiceContract;
using eClinicSpa.Common.Properties;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.MainWindow.View;
using eClinicSpa.WPF.MainWindow.Model;
using eClinicSpa.WPF.Services;
using eClinicSpa.WPF.Util;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.InvoiceBilling.View;
using eClinicSpa.WPF.MainWorking.View;
using eClinicSpa.WPF.Wokspace.ViewModel;
using eClinicSpa.WPF.ProductCategory.ViewModel;
using eClinicSpa.WPF.MainWorking.ViewModel;
using eClinicSpa.WPF.InvoiceBilling.Model;
using eClinicSpa.Common.Dto.Transaction;
using eClinicSpa.Common.Dto.Employee;
using eClinicSpa.Common.Dto.Product;
using eClinicSpa.Common.Constant;

namespace eClinicSpa.WPF.InvoiceBilling.ViewModel
{
    public class StockOutPrintViewModel : InvoicePrintBaseViewModel
    {
        public StockOutPrintViewModel(StockOutPrintView view)
            : base(view)
        {
            base.DisplayName = eClinicSpaResources.InvoiceBilling_StockReceipt;
            Model.InvoiceType = InvoiceConstant.STOCK;
        }
    }
}
