﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.Common.ServiceContract;
using eClinicSpa.Common.Properties;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.MainWindow.View;
using eClinicSpa.WPF.MainWindow.Model;
using eClinicSpa.WPF.Services;
using eClinicSpa.WPF.Util;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.InvoiceBilling.View;
using eClinicSpa.WPF.MainWorking.View;
using eClinicSpa.WPF.Wokspace.ViewModel;
using eClinicSpa.WPF.ProductCategory.ViewModel;
using eClinicSpa.WPF.MainWorking.ViewModel;
using eClinicSpa.WPF.InvoiceBilling.Model;
using eClinicSpa.Common.Dto.Transaction;
using eClinicSpa.Common.Dto.Employee;
using eClinicSpa.Common.Dto.Product;
using eClinicSpa.Common.Constant;
using System.Windows;

namespace eClinicSpa.WPF.InvoiceBilling.ViewModel
{
    public class InvoiceBillingViewModel : InvoiceBaseViewModel
    {
        public InvoiceBillingViewModel(InvoiceBillingView view)
            : base(view)
        {
            base.DisplayName = eClinicSpaResources.InvoiceBilling_Title;

            Model.InvoiceType = InvoiceConstant.BILLING;
            RaisePropertyChanged(() => Model);
        }

        private RelayCommand TransferToCashierCommandInstance;
        public RelayCommand TransferToCashierCommand
        {
            get
            {
                if (TransferToCashierCommandInstance != null) return TransferToCashierCommandInstance;
                TransferToCashierCommandInstance = new RelayCommand(a => TransferToCashier());
                return TransferToCashierCommandInstance;
            }
        }

        public void TransferToCashier()
        {
            EditInvoice.Status = PaymentStatus.Paying;
            SaveInvoice(false);
            MessageBox.Show(Application.Current.MainWindow, "Đã chuyển đến Thu ngân", ApplicationInfo.ProductName);
        }
    }
}
