﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.Common.ServiceContract;
using eClinicSpa.Common.Properties;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.MainWindow.View;
using eClinicSpa.WPF.MainWindow.Model;
using eClinicSpa.WPF.Services;
using eClinicSpa.WPF.Util;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.InvoiceBilling.View;
using eClinicSpa.WPF.MainWorking.View;
using eClinicSpa.WPF.Wokspace.ViewModel;
using eClinicSpa.WPF.ProductCategory.ViewModel;
using eClinicSpa.WPF.MainWorking.ViewModel;
using eClinicSpa.WPF.InvoiceBilling.Model;
using eClinicSpa.Common.Dto.Transaction;
using eClinicSpa.Common.Dto.Employee;
using eClinicSpa.Common.Dto.Product;
using eClinicSpa.Common.Constant;

namespace eClinicSpa.WPF.InvoiceBilling.ViewModel
{
    public class InvoiceBaseViewModel : WorkspaceViewModel
    {
        protected readonly ServiceAdapter<IEmployeeService> EmployeeServiceAdapter;
        protected readonly ServiceAdapter<ICustomerService> CustomerServiceAdapter;
        protected readonly ServiceAdapter<IProductCategoryService> ProductCategoryServiceAdapter;
        protected readonly ServiceAdapter<ITransactionService> TransactionServiceAdapter;

        protected static IList<ProductDto> productList;

        public InvoiceBaseViewModel(IView view)
            : base(view)
        {
            EmployeeServiceAdapter = new ServiceAdapter<IEmployeeService>();
            CustomerServiceAdapter = new ServiceAdapter<ICustomerService>();
            ProductCategoryServiceAdapter = new ServiceAdapter<IProductCategoryService>();
            TransactionServiceAdapter = new ServiceAdapter<ITransactionService>();

            UpdateProductList();
            Model = new InvoiceBillingModel
            {
                ProductList = productList,
                IsEnabled = true
            };
        }

        #region Properties

        public InvoiceBillingModel Model { get; set; }

        public string InvoiceTitle
        {
            get
            {
                switch (Model.InvoiceType)
                {
                    case InvoiceConstant.STOCK:
                        return eClinicSpaResources.InvoiceBilling_Stock;
                    case InvoiceConstant.REFERENCE:
                        return eClinicSpaResources.InvoiceBilling_Reference;
                    case InvoiceConstant.INVOICE:
                        return eClinicSpaResources.InvoiceBilling_Invoice;
                    default:
                        return eClinicSpaResources.InvoiceBilling_Consultant;
                }
            }
        }

        public bool IsEnabled 
        {
            get { return Model.IsEnabled; }
            set
            {
                if (Model.IsEnabled != value)
                {
                    Model.IsEnabled = value;
                    RaisePropertyChanged("IsEnabled");
                }
            }
        }

        public TransactionDto EditTransaction
        {
            get { return Model.EditTransaction; }
            set
            {
                if (Model.EditTransaction != value)
                {
                    Model.EditTransaction = value;
                    RaisePropertyChanged("EditTransaction");
                }
                ChangeTransaction();
            }
        }

        public InvoiceDto EditInvoice
        {
            get { return Model.EditInvoice; }
            set
            {
                if (value != null && Model.EditInvoice != value)
                {
                    Model.EditInvoice = value;
                    RaisePropertyChanged("EditInvoice");
                    RaisePropertyChanged("InvoiceItemList");
                    RaisePropertyChanged("InvoiceTitle");
                    RaisePropertyChanged(() => Model);
                }
            }
        }

        public IList<InvoiceDto> InvoiceList
        {
            get { return Model.InvoiceList; }
            set
            {
                if (value != null && Model.InvoiceList != value)
                {
                    Model.InvoiceList = value;
                    RaisePropertyChanged("InvoiceList");
                }
            }
        }

        public InvoiceRowDto EditInvoiceItem { get; set; }

        public List<InvoiceRowDto> InvoiceItemList
        {
            get
            {
                return EditInvoice == null ? null : EditInvoice.InvoiceItems; 
            }
            set
            {
                if (EditInvoice.InvoiceItems != value)
                {
                    EditInvoice.InvoiceItems = value;
                    RaisePropertyChanged("InvoiceItemList");
                }
            }
        }

        public IList<ProductDto> ProductList
        {
            get { return Model.ProductList; }
            set
            {
                if (Model.ProductList != value)
                {
                    Model.ProductList = value;
                    RaisePropertyChanged("ProductList");
                }
            }
        }

        public void NotifyPropertyChanged(string propertyName)
        {
            RaisePropertyChanged(propertyName);
        }

        #endregion

        #region Commands

        private RelayCommand ConvertToInvoiceCommandInstance;
        public RelayCommand ConvertToInvoiceCommand
        {
            get { return ConvertToInvoiceCommandInstance; }
            set
            {
                if (ConvertToInvoiceCommandInstance != value)
                {
                    ConvertToInvoiceCommandInstance = value;
                    RaisePropertyChanged("ConvertToInvoiceCommand");
                }
            }
        }

        public void ConvertToInvoice()
        {
            Model.IsEnabled = false;
            RaisePropertyChanged(() => Model);

            SaveInvoice(false);

            InvoiceDto newInvoice = TransactionServiceAdapter.Execute(service =>
                service.GetInvoiceByTransactionAndType(EditTransaction.Id, InvoiceConstant.BILLING));
            newInvoice.Creator = EditInvoice.Creator;
            newInvoice.CustomerDiscount = EditInvoice.CustomerDiscount;
            newInvoice.Deposit = EditInvoice.Deposit;
            newInvoice.DepositDate = EditInvoice.DepositDate;
            newInvoice.PaymentType = PaymentType.Cash;
            newInvoice.Status = PaymentStatus.NotPaid;

            if (newInvoice.Id == 0)
            {
                newInvoice.Transaction = EditTransaction;
                newInvoice.DocType = InvoiceConstant.BILLING;
                newInvoice = TransactionServiceAdapter.Execute(s => s.CreateNewInvoice(newInvoice));
            }
            else
            {
                foreach (InvoiceRowDto item in newInvoice.InvoiceItems)
                {
                    TransactionServiceAdapter.Execute(s => s.DeleteInvoiceRow(item.Id));
                }
                newInvoice = TransactionServiceAdapter.Execute(s => s.UpdateInvoice(newInvoice));
            }
            foreach (InvoiceRowDto item in EditInvoice.InvoiceItems)
            {
                InvoiceRowDto newItem = new InvoiceRowDto
                {
                    InvoiceId = newInvoice.Id,
                    ItemNo = item.ItemNo,
                    Product = item.Product,
                    UnitPrice = item.UnitPrice,
                    Quantity = item.Quantity,
                    ProductDiscountRate = item.ProductDiscountRate,
                    ProductDiscountAmount = item.ProductDiscountAmount,
                    NotPaid = true
                };
                TransactionServiceAdapter.Execute(s => s.CreateNewInvoiceRow(newItem));
            }
            Model.IsEnabled = true;
            RaisePropertyChanged(() => Model);
        }

        private RelayCommand InvoicePreviewCommandInstance;
        public RelayCommand InvoicePreviewCommand
        {
            get { return InvoicePreviewCommandInstance; }
            set
            {
                if (InvoicePreviewCommandInstance != value)
                {
                    InvoicePreviewCommandInstance = value;
                    RaisePropertyChanged("InvoicePreviewCommand");
                }
            }
        }

        private RelayCommand SaveInvoiceCommandInstance;
        public RelayCommand SaveInvoiceCommand
        {
            get
            {
                if (SaveInvoiceCommandInstance != null) return SaveInvoiceCommandInstance;
                SaveInvoiceCommandInstance = new RelayCommand(a => SaveInvoice(true), p => EditInvoice != null && EditInvoice.IsValid());
                return SaveInvoiceCommandInstance;
            }
        }

        public void SaveInvoice(bool showMessage)
        {
            for (int i = 0; i < EditInvoice.InvoiceItems.Count; i++)
            {
                InvoiceRowDto item = EditInvoice.InvoiceItems.ElementAt(i);
                try
                {
                    if (item.Id == 0)
                    {
                        item = TransactionServiceAdapter.Execute(s => s.CreateNewInvoiceRow(item));
                    }
                    else
                    {
                        item = TransactionServiceAdapter.Execute(s => s.UpdateInvoiceRow(item));
                    }
                    EditInvoice.InvoiceItems[i] = item;
                }
                catch (Exception e)
                {
                    MessageBox.Show(Application.Current.MainWindow,
                                    String.Format("Lỗi xảy ra khi lưu các mục trong phiếu: dòng {0}, sản phẩm/dịch vụ: {1}.\n\n{2}",
                                                item.ItemNo, item.Product.Name,
                                                e.StackTrace),
                                    ApplicationInfo.ProductName);
                    return;
                }
            }
            try
            {
                if (EditInvoice.Deposit == 0)
                {
                    EditInvoice.DepositDate = null;
                }
                //else
                //{
                //    EditInvoice.DepositDate = DateTime.Now;
                //}
                if (EditInvoice.PaymentType == 0)
                {
                    EditInvoice.PaymentType = PaymentType.Cash;
                }
                EditInvoice = TransactionServiceAdapter.Execute(s => s.UpdateInvoice(EditInvoice));
                RaisePropertyChanged("InvoiceItemList");
                RaisePropertyChanged(() => Model);
                if (showMessage)
                {
                    MessageBox.Show(Application.Current.MainWindow, "Đã lưu dữ liệu.",
                                    ApplicationInfo.ProductName);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Application.Current.MainWindow,
                                "Có lỗi xảy ra, không lưu được dữ liệu.\n\n" + e.StackTrace,
                                ApplicationInfo.ProductName);
            }
        }

        public void ReCalculateItemTotalAmount()
        {
            foreach (InvoiceRowDto item in EditInvoice.InvoiceItems)
            {
                if (item.NotPaid == false)
                    continue;
                item.UnitPrice = item.Product == null ? 0 : item.Product.Price;
                item.Amount = item.UnitPrice * item.Quantity;
                item.ProductDiscountAmount = item.Amount * item.ProductDiscountRate;
            }
            RaisePropertyChanged("InvoiceItemList");
            RaisePropertyChanged("EditInvoice");
        }

        private RelayCommand AddInvoiceItemCommandInstance;
        public RelayCommand AddInvoiceItemCommand
        {
            get
            {
                if (AddInvoiceItemCommandInstance != null) return AddInvoiceItemCommandInstance;
                AddInvoiceItemCommandInstance = new RelayCommand(a => AddInvoiceItem(), p => EditInvoice != null && EditInvoice.IsValid());
                return AddInvoiceItemCommandInstance;
            }
        }

        private void AddInvoiceItem()
        {
            IsEnabled = false;
            AddNewInvoiceItem();
            RaisePropertyChanged("InvoiceItemList");
            RaisePropertyChanged("EditInvoice");
            IsEnabled = true;
        }

        private RelayCommand RemoveInvoiceItemCommandInstance;
        public RelayCommand RemoveInvoiceItemCommand
        {
            get
            {
                if (RemoveInvoiceItemCommandInstance != null) return RemoveInvoiceItemCommandInstance;
                RemoveInvoiceItemCommandInstance = new RelayCommand(a => 
                {
                    MessageBoxResult result = MessageBox.Show(Application.Current.MainWindow, eClinicSpaResources.DeleteConfirmation,
                                ApplicationInfo.ProductName,
                                MessageBoxButton.YesNo, MessageBoxImage.Question);

                    if (result == MessageBoxResult.Yes)
                    {
                        RemoveInvoiceItem();
                    }
                }, p => EditInvoice != null && EditInvoice.IsValid());
                return RemoveInvoiceItemCommandInstance;
            }
        }

        private void RemoveInvoiceItem()
        {
            IsEnabled = false;
            IList<InvoiceRowDto> deleteItems = new List<InvoiceRowDto>();
            foreach (InvoiceRowDto item in EditInvoice.InvoiceItems)
            {
                if (item.IsSelected && item.Id != 0)
                {
                    TransactionServiceAdapter.Execute(s => s.DeleteInvoiceRow(item.Id));
                }
            }
            EditInvoice.InvoiceItems.RemoveAll(i => i.IsSelected);
            // re-index
            for (int i = 0; i < EditInvoice.InvoiceItems.Count; i++)
            {
                EditInvoice.InvoiceItems.ElementAt(i).ItemNo = i + 1;
            }
            RaisePropertyChanged("InvoiceItemList");
            RaisePropertyChanged("EditInvoice");
            IsEnabled = true;
        }

        #endregion

        #region Private helpers

        private void UpdateProductList()
        {
            var products = ProductCategoryServiceAdapter.Execute(service => service.FindAllActiveProduct());
            productList = products.Products;
        }

        private void ChangeTransaction()
        {
            if (EditTransaction == null)
            {
                return;
            }
            IsEnabled = false;
            RaisePropertyChanged(() => Model);
            EditInvoice = TransactionServiceAdapter.Execute(service => service.GetInvoiceByTransactionAndType(EditTransaction.Id, Model.InvoiceType));
            if (EditInvoice.Status == PaymentStatus.Paid || EditInvoice.Status == PaymentStatus.StockOut)
            {
                EditInvoice = new InvoiceDto();
            }
            if (EditInvoice.Id == 0)
            {
                EditInvoice.Transaction = EditTransaction;
                EditInvoice.DocType = Model.InvoiceType;
                EditInvoice = TransactionServiceAdapter.Execute(s => s.CreateNewInvoice(EditInvoice));
                AddNewInvoiceItem();
            }
            else if (EditInvoice.InvoiceItems == null || EditInvoice.InvoiceItems.Count == 0)
            {
                EditInvoice.InvoiceItems = new List<InvoiceRowDto>();
                AddNewInvoiceItem();
            }
            RaisePropertyChanged("InvoiceItemList");
            RaisePropertyChanged("InvoiceTitle");
            UpdateInvoiceList();
            //UpdateProductList();
           
            IsEnabled = true;
            RaisePropertyChanged(() => Model);
        }

        private void UpdateInvoiceList()
        {
            var invoices = TransactionServiceAdapter.Execute(service => service.GetInvoiceListByTransactionAndType(EditTransaction.Id, Model.InvoiceType));
            InvoiceList = invoices.Invoices;
        }

        private void AddNewInvoiceItem()
        {
            InvoiceRowDto invoiceRow = new InvoiceRowDto { InvoiceId = EditInvoice.Id, Quantity = 1, NotPaid = true };
            EditInvoice.InvoiceItems.Add(invoiceRow);
            invoiceRow.ItemNo = EditInvoice.InvoiceItems.Count;
        }

        #endregion
    }
}
