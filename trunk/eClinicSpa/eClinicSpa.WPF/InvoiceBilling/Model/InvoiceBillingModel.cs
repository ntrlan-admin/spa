﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eClinicSpa.Common.Constant;
using eClinicSpa.Common.Dto.Employee;
using eClinicSpa.Common.Dto.Product;
using eClinicSpa.Common.Dto.Transaction;

namespace eClinicSpa.WPF.InvoiceBilling.Model
{
    public class InvoiceBillingModel
    {
        public InvoiceBillingModel()
        {
        }

        public int InvoiceType { get; set; }
        public TransactionDto EditTransaction { get; set; }
        public IList<InvoiceDto> InvoiceList { get; set; }
        public InvoiceDto EditInvoice { get; set; }
        public IList<InvoiceRowDto> InvoiceItemList { get; set; }
        public IList<ProductDto> ProductList { get; set; }
        public IList<ProductDiscount> ProductDiscountList { get { return ProductDiscountTypes.ProductDiscountList; } }

        public bool IsEnabled { get; set; }

        public bool IsEnabledTransferToCashier 
        {
            get
            {
                return EditInvoice != null &&
                      (EditInvoice.DocType == InvoiceConstant.BILLING || EditInvoice.DocType == InvoiceConstant.INVOICE) &&
                      EditInvoice.InvoiceItems != null && EditInvoice.InvoiceItems.Any() &&
                      EditInvoice.Status < PaymentStatus.Paying;
            }
        }
        public bool IsEnabledToChangeInvoice { get { return EditInvoice != null && EditInvoice.Status != PaymentStatus.Paid; } }

        public bool IsEnabledInvoicePrintView { get { return EditInvoice != null && EditInvoice.Id != 0 && EditInvoice.InvoiceItems != null && EditInvoice.InvoiceItems.Any() && EditInvoice.InvoiceItems[0].Product != null; } }
        public bool IsEnabledPaidInvoiceView { get { return IsEnabledInvoicePrintView && EditTransaction != null && EditTransaction.Status > CustomerTransactionStatus.Incoming; } }
        public bool IsEnabledInvoiceView { get { return IsEnabledPaidInvoiceView && EditTransaction.Status < CustomerTransactionStatus.Finished; } }
        public bool IsEnabledConvertToInvoice { get { return IsEnabledInvoicePrintView && EditInvoice.DocType != InvoiceConstant.BILLING; } }
        public bool IsShowInvoicePrintView { get { return IsEnabledInvoicePrintView && EditInvoice.DocType == InvoiceConstant.BILLING; } }
    }
}
