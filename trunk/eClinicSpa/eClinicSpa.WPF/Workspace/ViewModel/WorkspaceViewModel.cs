﻿using System;
using System.Windows.Input;

using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.Util;

namespace eClinicSpa.WPF.Wokspace.ViewModel
{
    /// <summary>
    /// This BaseViewModel subclass requests to be removed 
    /// from the UI when its CloseCommand executes.
    /// This class is abstract.
    /// </summary>
    public abstract class WorkspaceViewModel : ViewModelBase
    {
        #region Fields

        RelayCommand CloseCommandInstance;

        #endregion // Fields

        #region Constructor
        protected WorkspaceViewModel(IView view)
            : base(view)
        {
        }
        #endregion // Constructor

        #region CloseCommand

        /// <summary>
        /// Returns the command that, when invoked, attempts
        /// to remove this workspace from the user interface.
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (CloseCommandInstance == null)
                    CloseCommandInstance = new RelayCommand(a => OnRequestClose());

                return CloseCommandInstance;
            }
        }

        #endregion // CloseCommand

        #region RequestClose [event]

        /// <summary>
        /// Raised when this workspace should be removed from the UI.
        /// </summary>
        public event EventHandler RequestClose;

        void OnRequestClose()
        {
            EventHandler handler = this.RequestClose;
            if (handler != null)
                handler(this, EventArgs.Empty);
        }

        #endregion // RequestClose [event]
    }
}