﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.MainWindow.ViewModel;
using eClinicSpa.Common.Converters;
using eClinicSpa.WPF.Properties;
using System.Windows.Threading;
using eClinicSpa.Common.Properties;
//using eClinicSpa.Common.Properties;

namespace eClinicSpa.WPF.MainWindow.View
{
    /// <summary>
    /// Interaction logic for MainWindowView.xaml
    /// </summary>
    public partial class MainWindowView : Window, IView
    {
        private readonly Lazy<MainWindowViewModel> viewModel;
        private static DispatcherTimer timerRefreshEmployeeStatusList = new DispatcherTimer();

        public MainWindowView()
        {
            InitializeComponent();
            RestoreWindowSizeLocation();
            viewModel = new Lazy<MainWindowViewModel>(() => ViewHelper.GetViewModel<MainWindowViewModel>(this));

            // Hook up the Elapsed event for the timer.
            timerRefreshEmployeeStatusList.Tick += new EventHandler(RefreshEmployeeStatusListOnTimedEvent);

            // Set the Interval to 10 seconds (10000 milliseconds).
            timerRefreshEmployeeStatusList.Interval = new TimeSpan(0, 0, 10);
            timerRefreshEmployeeStatusList.Start();
        }

        private MainWindowViewModel ViewModel { get { return viewModel.Value; } }

        public bool IsMaximized
        {
            get { return WindowState == WindowState.Maximized; }
            set
            {
                if (value)
                {
                    WindowState = WindowState.Maximized;
                }
                else if (WindowState == WindowState.Maximized)
                {
                    WindowState = WindowState.Normal;
                }
            }
        }

        // RefreshEmployeeStatusList when the Elapsed event is raised.
        private void RefreshEmployeeStatusListOnTimedEvent(object source, EventArgs e)
        {
            ViewModel.RefreshEmployeeStatusList();
        }

        public void RestoreWindowSizeLocation()
        {
            // Restore the window size when the values are valid.
            if (Settings.Default.Left >= 0 && Settings.Default.Top >= 0 && Settings.Default.Width > 0 && Settings.Default.Height > 0
                && Settings.Default.Left + Settings.Default.Width <= SystemParameters.VirtualScreenWidth
                && Settings.Default.Top + Settings.Default.Height <= SystemParameters.VirtualScreenHeight)
            {
                this.Left = Settings.Default.Left;
                this.Top = Settings.Default.Top;
                this.Height = Settings.Default.Height;
                this.Width = Settings.Default.Width;
            }
            else
            {
                this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            }
            this.IsMaximized = Settings.Default.IsMaximized;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show(Application.Current.MainWindow, eClinicSpaResources.ExitConfirmation,
                        ApplicationInfo.ProductName,
                        MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (result == MessageBoxResult.No)
            {
                e.Cancel = true;
                return;
            }

            MainWindowView mainWindow = sender as MainWindowView;

            Settings.Default.LastLoggedInUserName = ViewModel.Model.LoggedInUser.UserName;

            Settings.Default.Left = mainWindow.Left;
            Settings.Default.Top = mainWindow.Top;
            Settings.Default.Height = mainWindow.Height;
            Settings.Default.Width = mainWindow.Width;
            Settings.Default.IsMaximized = mainWindow.IsMaximized;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (ViewModel.NewLanguage != null)
            {
                Settings.Default.UICulture = ViewModel.NewLanguage.Name;
            }
            try
            {
                Settings.Default.Save();
            }
            catch (Exception)
            {
                // When more application instances are closed at the same time then an exception occurs.
            }
        }

        private bool isShowEmployeeList = true;
        private void ShowHideEmployeeStatusView()
        {
            isShowEmployeeList = !isShowEmployeeList;
            employeeStatusList.Visibility = isShowEmployeeList ? Visibility.Visible : Visibility.Collapsed;
            if (isShowEmployeeList)
            {
                ShowHideEmployeeStatusList.Content = FindResource("collapse");
                ShowHideEmployeeStatusList.ToolTip = "Ẩn danh sách trạng thái nhân viên";
            }
            else
            {
                ShowHideEmployeeStatusList.Content = FindResource("expand");
                ShowHideEmployeeStatusList.ToolTip = "Hiện danh sách trạng thái nhân viên";
            }
        }

        private void ShowHideEmployeeStatusList_Click(object sender, RoutedEventArgs e)
        {
            ShowHideEmployeeStatusView();
        }

        public void HideEmployeeStatusView()
        {
            if (isShowEmployeeList)
            {
                ShowHideEmployeeStatusView();
            }
        }
    }
}
