﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows;
using System.Collections.Specialized;

using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.Common.ServiceContract;
using eClinicSpa.Common.Properties;
using eClinicSpa.Common.Constant;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.Services;
using eClinicSpa.WPF.Util;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Wokspace.ViewModel;
using eClinicSpa.WPF.MainWindow.View;
using eClinicSpa.WPF.MainWindow.Model;
using eClinicSpa.WPF.ProductCategory.View;
using eClinicSpa.WPF.ProductCategory.ViewModel;
using eClinicSpa.WPF.MainWorking.View;
using eClinicSpa.WPF.MainWorking.ViewModel;
using eClinicSpa.WPF.Product.View;
using eClinicSpa.WPF.Product.ViewModel;
using eClinicSpa.WPF.InvoiceBilling.View;
using eClinicSpa.WPF.InvoiceBilling.ViewModel;
using eClinicSpa.WPF.InvoiceBilling.Model;
using eClinicSpa.WPF.Cashier.View;
using eClinicSpa.WPF.Cashier.ViewModel;
using eClinicSpa.WPF.Report.View;
using eClinicSpa.WPF.Report.ViewModel;
using eClinicSpa.WPF.OperateStockOut.View;
using eClinicSpa.WPF.OperateStockOut.ViewModel;
using eClinicSpa.WPF.Employee.View;
using eClinicSpa.WPF.Employee.ViewModel;

namespace eClinicSpa.WPF.MainWindow.ViewModel
{
    /// <remarks>
    /// version 0.06 Chapter VI:   MVVM Pattern
    /// version 0.08 Chapter VIII: RelayCommand
    /// version 0.09 Chapter IX:   Notify Property Changed
    /// version 0.13 Chapter XIII: Async Service Commands
    /// </remarks>
    public class MainWindowViewModel
        : WorkspaceViewModel
    {
        private readonly ServiceAdapter<IEmployeeService> EmployeeServiceAdapter;
        private readonly ServiceAdapter<ICustomerService> CustomerServiceAdapter;

        private readonly ObservableCollection<IView> workspaceViews;
        private IView activeWorkspaceView;

        ObservableCollection<WorkspaceViewModel> _workspaces;

        MainWorkingScreen receptionView;
        MainWorkingScreen consultantView;
        MainWorkingScreenViewModel receptionViewModel;
        MainWorkingScreenViewModel consultantViewModel;
        CashierViewModel cashierViewModel;
        InvoiceBillingViewModel invoiceBillingViewModel;
        StockOutViewModel stockOutViewModel;
        StockReceiptViewModel stockReceiptViewModel;
        InvoicePrintViewModel invoicePrintViewModel;
        StockOutPrintViewModel stockOutPrintViewModel;

        private RelayCommand closeActiveWorkspaceCommand;

        private RelayCommand viewReceptionScreenCommand;
        private RelayCommand viewConsultantScreenCommand;
        private RelayCommand viewCashierCommand;
        private RelayCommand viewInvoiceBillingCommand;
        private RelayCommand viewStockOutCommand;
        private RelayCommand convertStockOutToInvoiceCommand;
        private RelayCommand convertReferenceToInvoiceCommand;
        private RelayCommand invoicePreviewInConsultantCommand;
        private RelayCommand invoicePreviewInBillingCommand;
        private RelayCommand invoicePrintPreviewInCashierCommand;
        private RelayCommand stockOutPrintViewCommand;
        private RelayCommand productCategoryListCommand;

        private RelayCommand exitCommand;
        private readonly RelayCommand aboutCommand;

        private readonly RelayCommand vietnameseCommand;
        private readonly RelayCommand englishCommand;
        private CultureInfo newLanguage;

        public MainWindowViewModel(MainWindowView view)
            : base(view)
        {
            EmployeeServiceAdapter = new ServiceAdapter<IEmployeeService>();
            CustomerServiceAdapter = new ServiceAdapter<ICustomerService>();

            workspaceViews = new ObservableCollection<IView>();
            _workspaces = new ObservableCollection<WorkspaceViewModel>();
            _workspaces.CollectionChanged += this.OnWorkspacesChanged;

            this.vietnameseCommand = new RelayCommand(a => SelectLanguage(new CultureInfo("vi-VN")));
            this.englishCommand = new RelayCommand(a => SelectLanguage(new CultureInfo("en-US")));

            this.aboutCommand = new RelayCommand(a => ShowAboutMessage());

            GetEmployeeStatusList();
        }

        #region Properties

        public MainWindowModel Model { get; set; }

        public CultureInfo NewLanguage { get { return newLanguage; } }
        public RelayCommand VietnameseCommand { get { return vietnameseCommand; } }
        public RelayCommand EnglishCommand { get { return englishCommand; } }

        #endregion Properties

        #region Commands

        private RelayCommand StockReceiptCommandInstance;
        public RelayCommand StockReceiptCommand
        {
            get
            {
                if (StockReceiptCommandInstance != null) return StockReceiptCommandInstance;
                StockReceiptCommandInstance = new RelayCommand(a => ShowStockReceiptView());
                return StockReceiptCommandInstance;
            }
        }

        private RelayCommand LogInHistoryCommandInstance;
        public RelayCommand LogInHistoryCommand
        {
            get
            {
                if (LogInHistoryCommandInstance != null) return LogInHistoryCommandInstance;
                LogInHistoryCommandInstance = new RelayCommand(a => ShowLogInHistoryView());
                return LogInHistoryCommandInstance;
            }
        }

        private RelayCommand CashierReportCommandInstance;
        public RelayCommand CashierReportCommand
        {
            get
            {
                if (CashierReportCommandInstance != null) return CashierReportCommandInstance;
                CashierReportCommandInstance = new RelayCommand(a => ShowCashierReportView());
                return CashierReportCommandInstance;
            }
        }

        private RelayCommand CustomerReportCommandInstance;
        public RelayCommand CustomerReportCommand
        {
            get
            {
                if (CustomerReportCommandInstance != null) return CustomerReportCommandInstance;
                CustomerReportCommandInstance = new RelayCommand(a => ShowCustomerReportView());
                return CustomerReportCommandInstance;
            }
        }

        private RelayCommand StockOutReportCommandInstance;
        public RelayCommand StockOutReportCommand
        {
            get
            {
                if (StockOutReportCommandInstance != null) return StockOutReportCommandInstance;
                StockOutReportCommandInstance = new RelayCommand(a => ShowStockOutReportView());
                return StockOutReportCommandInstance;
            }
        }

        public RelayCommand CashierViewCommand
        {
            get
            {
                if (viewCashierCommand == null)
                    viewCashierCommand = new RelayCommand(a => ShowCashierView());

                return viewCashierCommand;
            }
        }

        public RelayCommand InvoiceBillingCommand
        {
            get
            {
                if (viewInvoiceBillingCommand == null)
                    viewInvoiceBillingCommand = new RelayCommand(a => ShowInvoiceBillingView(int.Parse(a.ToString())));

                return viewInvoiceBillingCommand;
            }
        }

        public RelayCommand StockOutCommand
        {
            get
            {
                if (viewStockOutCommand == null)
                    viewStockOutCommand = new RelayCommand(a => ShowStockOutView());

                return viewStockOutCommand;
            }
        }

        public RelayCommand ConvertStockOutToInvoiceCommand
        {
            get
            {
                if (convertStockOutToInvoiceCommand == null)
                    convertStockOutToInvoiceCommand = new RelayCommand(a => ConvertStockOutToInvoice());

                return convertStockOutToInvoiceCommand;
            }
        }

        public RelayCommand ConvertReferenceToInvoiceCommand
        {
            get
            {
                if (convertReferenceToInvoiceCommand == null)
                    convertReferenceToInvoiceCommand = new RelayCommand(a => ConvertReferenceToInvoice());

                return convertReferenceToInvoiceCommand;
            }
        }

        public RelayCommand InvoicePreviewInConsultantCommand
        {
            get
            {
                if (invoicePreviewInConsultantCommand == null)
                    invoicePreviewInConsultantCommand = new RelayCommand(a => ShowInvoicePreviewInConsultant(int.Parse(a.ToString())));

                return invoicePreviewInConsultantCommand;
            }
        }

        public RelayCommand InvoicePreviewInBillingCommand
        {
            get
            {
                if (invoicePreviewInBillingCommand == null)
                    invoicePreviewInBillingCommand = new RelayCommand(a => ShowInvoicePreviewInBilling(int.Parse(a.ToString())));

                return invoicePreviewInBillingCommand;
            }
        }
        public RelayCommand InvoicePrintPreviewCommand
        {
            get
            {
                if (invoicePrintPreviewInCashierCommand == null)
                    invoicePrintPreviewInCashierCommand = new RelayCommand(a => ShowInvoicePrintPreview(int.Parse(a.ToString())));

                return invoicePrintPreviewInCashierCommand;
            }
        }
        public RelayCommand StockOutPrintViewCommand
        {
            get
            {
                if (stockOutPrintViewCommand == null)
                    stockOutPrintViewCommand = new RelayCommand(a => ShowStockOutPrintView());

                return stockOutPrintViewCommand;
            }
        }

        public RelayCommand ReceptionViewCommand
        {
            get
            {
                if (viewReceptionScreenCommand == null)
                    viewReceptionScreenCommand = new RelayCommand(a => ShowReceptionScreenView());

                return viewReceptionScreenCommand;
            }
        }
        public RelayCommand ConsultantViewCommand
        {
            get
            {
                if (viewConsultantScreenCommand == null)
                    viewConsultantScreenCommand = new RelayCommand(a => ShowConsultantScreenView());

                return viewConsultantScreenCommand;
            }
        }

        public RelayCommand ProductCategoryListCommand
        {
            get
            {
                if (productCategoryListCommand == null)
                    productCategoryListCommand = new RelayCommand(a => ShowProductCategoriesView());

                return productCategoryListCommand;
            }
        }

        private RelayCommand ProductListCommandInstance;
        public RelayCommand ProductListCommand
        {
            get
            {
                if (ProductListCommandInstance == null)
                    ProductListCommandInstance = new RelayCommand(a => ShowProductManageView());

                return ProductListCommandInstance;
            }
        }

        private RelayCommand EmployeeManageCommandInstance;
        public RelayCommand EmployeeManageCommand
        {
            get
            {
                if (EmployeeManageCommandInstance == null)
                    EmployeeManageCommandInstance = new RelayCommand(a => ShowEmployeeManageView());

                return EmployeeManageCommandInstance;
            }
        }

        public RelayCommand ExitCommand
        {
            get { return exitCommand; }
            set
            {
                if (exitCommand != value)
                {
                    exitCommand = value;
                    RaisePropertyChanged("ExitCommand");
                }
            }
        }

        public RelayCommand AboutCommand { get { return aboutCommand; } }

        public RelayCommand CloseActiveWorkspaceCommand
        {
            get
            {
                if (closeActiveWorkspaceCommand == null)
                    closeActiveWorkspaceCommand = new RelayCommand(a => CloseActiveWorkspace());

                return closeActiveWorkspaceCommand;
            }
        }

        #endregion

        #region Workspaces

        /// <summary>
        /// Returns the collection of available workspace views to display.
        /// A 'workspace view' has a ViewModel that can request to be closed.
        /// </summary>
        public ObservableCollection<IView> WorkspaceViews { get { return workspaceViews; } }

        public IView ActiveWorkspaceView
        {
            get { return activeWorkspaceView; }
            set
            {
                if (activeWorkspaceView != value)
                {
                    activeWorkspaceView = value;
                    RaisePropertyChanged("ActiveWorkspaceView");

                    if (activeWorkspaceView != null)
                    {
                        ViewHelper.GetViewModel(activeWorkspaceView).Focus();
                    }
                }
            }
        }

        void OnWorkspacesChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null && e.NewItems.Count != 0)
                foreach (WorkspaceViewModel workspace in e.NewItems)
                    workspace.RequestClose += this.OnWorkspaceRequestClose;

            if (e.OldItems != null && e.OldItems.Count != 0)
                foreach (WorkspaceViewModel workspace in e.OldItems)
                    workspace.RequestClose -= this.OnWorkspaceRequestClose;
        }

        void OnWorkspaceRequestClose(object sender, EventArgs e)
        {
            WorkspaceViewModel workspace = sender as WorkspaceViewModel;
            workspace.Dispose();
            _workspaces.Remove(workspace);
            workspaceViews.Remove(workspace.View);
        }

        #endregion // Workspaces

        #region Private Helpers

        private void GetEmployeeStatusList()
        {
            Model = new MainWindowModel()
            {
                IsEnabled = true
            };
            GetConsultantStatusView();
            GetTherapistStatusView();
            RaisePropertyChanged(() => Model);
        }

        void GetConsultantStatusView()
        {
            var consultantList = EmployeeServiceAdapter.Execute(service => service.GetConsultantStatusView());
            Model.ConsultantStatusList = consultantList.ConsultantStatusViews;
            RaisePropertyChanged(() => Model);
        }

        void GetTherapistStatusView()
        {
            var therapistList = EmployeeServiceAdapter.Execute(service => service.GetTherapistStatusView());
            Model.TherapistStatusList = therapistList.TherapistStatusViews;
            RaisePropertyChanged(() => Model);
        }

        public void RefreshEmployeeStatusList()
        {
            GetConsultantStatusView();
            GetTherapistStatusView();
        }

        void ShowReceptionScreenView()
        {
            if (receptionView == null || !workspaceViews.Contains(receptionView))
            {
                receptionView = (MainWorkingScreen)ViewHelper.CreateView<MainWorkingScreenViewModel>();
                receptionViewModel = ViewHelper.GetViewModel(receptionView) as MainWorkingScreenViewModel;
                receptionViewModel.Model.IsReceptionView = true;
                receptionViewModel.Model.LoggedInUser = this.Model.LoggedInUser;
                receptionViewModel.ChangeDisplayName();
                _workspaces.Add(receptionViewModel);
                workspaceViews.Add(receptionView);
            }
            receptionViewModel.MainWindowViewModel = this;

            this.SetActiveWorkspaceView(receptionView);
        }

        void ShowConsultantScreenView()
        {
            if (consultantView == null || !workspaceViews.Contains(consultantView))
            {
                consultantView = (MainWorkingScreen)ViewHelper.CreateView<MainWorkingScreenViewModel>();
                consultantViewModel = ViewHelper.GetViewModel(consultantView) as MainWorkingScreenViewModel;
                consultantViewModel.Model.IsReceptionView = false;
                consultantViewModel.Model.LoggedInUser = this.Model.LoggedInUser;
                consultantViewModel.ChangeDisplayName();
                _workspaces.Add(consultantViewModel);
                workspaceViews.Add(consultantView);
            }
            consultantViewModel.MainWindowViewModel = this;
            consultantViewModel.InvoiceBillingCommand = this.InvoiceBillingCommand;
            consultantViewModel.StockOutCommand = this.StockOutCommand;
            
            consultantViewModel.InvoicePreviewCommand = this.InvoicePreviewInConsultantCommand;
            //consultantViewModel.StockOutPrintViewCommand = this.StockOutPrintViewCommand;

            this.SetActiveWorkspaceView(consultantView);
        }

        void ShowCashierView()
        {
            CashierView workspaceView = workspaceViews.FirstOrDefault(vm => vm is CashierView) as CashierView;

            if (workspaceView == null)
            {
                workspaceView = (CashierView)ViewHelper.CreateView<CashierViewModel>();
                cashierViewModel = ViewHelper.GetViewModel(workspaceView) as CashierViewModel;
                cashierViewModel.Model.LoggedInUser = this.Model.LoggedInUser;
                _workspaces.Add(cashierViewModel);
                workspaceViews.Add(workspaceView);
            }
            cashierViewModel.InvoicePreviewCommand = this.InvoicePrintPreviewCommand;
            (this.View as MainWindowView).HideEmployeeStatusView();
            this.SetActiveWorkspaceView(workspaceView);
        }
        
        void ShowInvoiceBillingView(int billType)
        {
            InvoiceBillingView workspaceView = workspaceViews.FirstOrDefault(vm => vm is InvoiceBillingView) as InvoiceBillingView;

            if (workspaceView == null)
            {
                workspaceView = (InvoiceBillingView)ViewHelper.CreateView<InvoiceBillingViewModel>();
                invoiceBillingViewModel = ViewHelper.GetViewModel(workspaceView) as InvoiceBillingViewModel;
                _workspaces.Add(invoiceBillingViewModel);
                workspaceViews.Add(workspaceView);
            }
            invoiceBillingViewModel.InvoicePreviewCommand = this.InvoicePreviewInBillingCommand;
            invoiceBillingViewModel.ConvertToInvoiceCommand = this.ConvertReferenceToInvoiceCommand;
            invoiceBillingViewModel.Model.InvoiceType = billType;
            invoiceBillingViewModel.EditTransaction = consultantViewModel.EditingTransaction;
            invoiceBillingViewModel.ReCalculateItemTotalAmount();

            this.SetActiveWorkspaceView(workspaceView);
        }

        void ShowStockOutView()
        {
            StockOutView workspaceView = workspaceViews.FirstOrDefault(vm => vm is StockOutView) as StockOutView;

            if (workspaceView == null)
            {
                workspaceView = (StockOutView)ViewHelper.CreateView<StockOutViewModel>();
                stockOutViewModel = ViewHelper.GetViewModel(workspaceView) as StockOutViewModel;
                _workspaces.Add(stockOutViewModel);
                workspaceViews.Add(workspaceView);
            }
            stockOutViewModel.ConvertToInvoiceCommand = this.ConvertStockOutToInvoiceCommand;
            stockOutViewModel.EditTransaction = consultantViewModel.EditingTransaction;

            this.SetActiveWorkspaceView(workspaceView);
        }

        void ConvertStockOutToInvoice()
        {
            stockOutViewModel.ConvertToInvoice();
            consultantViewModel.EditingTransaction = stockOutViewModel.EditTransaction;
            ShowInvoiceBillingView(InvoiceConstant.BILLING);
        }

        void ConvertReferenceToInvoice()
        {
            invoiceBillingViewModel.ConvertToInvoice();
            consultantViewModel.EditingTransaction = invoiceBillingViewModel.EditTransaction;
            ShowInvoiceBillingView(InvoiceConstant.BILLING);
        }

        void ShowInvoicePreviewInConsultant(int billType)
        {
            InvoicePrintPreview workspaceView = workspaceViews.FirstOrDefault(vm => vm is InvoicePrintPreview) as InvoicePrintPreview;

            if (workspaceView == null)
            {
                workspaceView = (InvoicePrintPreview)ViewHelper.CreateView<InvoicePrintViewModel>();
                invoicePrintViewModel = ViewHelper.GetViewModel(workspaceView) as InvoicePrintViewModel;
                _workspaces.Add(invoicePrintViewModel);
                workspaceViews.Add(workspaceView);
            }
            invoicePrintViewModel.Model.InvoiceType = billType;
            invoicePrintViewModel.PrintTransaction = consultantViewModel.EditingTransaction;
            workspaceView.ShowReport(ReportPrintPaperSize.A4);

            this.SetActiveWorkspaceView(workspaceView);
        }

        void ShowInvoicePreviewInBilling(int billType)
        {
            InvoicePrintPreview workspaceView = workspaceViews.FirstOrDefault(vm => vm is InvoicePrintPreview) as InvoicePrintPreview;

            if (workspaceView == null)
            {
                workspaceView = (InvoicePrintPreview)ViewHelper.CreateView<InvoicePrintViewModel>();
                invoicePrintViewModel = ViewHelper.GetViewModel(workspaceView) as InvoicePrintViewModel;
                _workspaces.Add(invoicePrintViewModel);
                workspaceViews.Add(workspaceView);
            }
            invoiceBillingViewModel.SaveInvoice(false);
            invoicePrintViewModel.Model.InvoiceType = billType;
            invoicePrintViewModel.PrintTransaction = invoiceBillingViewModel.EditTransaction;
            invoicePrintViewModel.PrintInvoice = invoiceBillingViewModel.EditInvoice;
            workspaceView.ShowReport(ReportPrintPaperSize.A4);

            this.SetActiveWorkspaceView(workspaceView);
        }

        void ShowInvoicePrintPreview(int billType)
        {
            InvoicePrintPreview workspaceView = workspaceViews.FirstOrDefault(vm => vm is InvoicePrintPreview) as InvoicePrintPreview;

            if (workspaceView == null)
            {
                workspaceView = (InvoicePrintPreview)ViewHelper.CreateView<InvoicePrintViewModel>();
                invoicePrintViewModel = ViewHelper.GetViewModel(workspaceView) as InvoicePrintViewModel;
                _workspaces.Add(invoicePrintViewModel);
                workspaceViews.Add(workspaceView);
            }
            invoicePrintViewModel.Model.InvoiceType = billType;
            invoicePrintViewModel.PrintTransaction = cashierViewModel.ProcessInvoice.Transaction;
            workspaceView.ShowReport(ReportPrintPaperSize.A6);

            this.SetActiveWorkspaceView(workspaceView);
        }

        void ShowProductCategoriesView()
        {
            ProductCategoriesView workspaceView = workspaceViews.FirstOrDefault(vm => vm is ProductCategoriesView) as ProductCategoriesView;

            if (workspaceView == null)
            {
                workspaceView = (ProductCategoriesView)ViewHelper.CreateView<ProductCategoriesViewModel>();
                ProductCategoriesViewModel workspace = ViewHelper.GetViewModel(workspaceView) as ProductCategoriesViewModel;
                _workspaces.Add(workspace);
                workspaceViews.Add(workspaceView);
            }

            this.SetActiveWorkspaceView(workspaceView);
        }

        void ShowProductManageView()
        {
            ProductManageView workspaceView = workspaceViews.FirstOrDefault(vm => vm is ProductManageView) as ProductManageView;

            if (workspaceView == null)
            {
                workspaceView = (ProductManageView)ViewHelper.CreateView<ProductManageViewModel>();
                ProductManageViewModel workspace = ViewHelper.GetViewModel(workspaceView) as ProductManageViewModel;
                _workspaces.Add(workspace);
                workspaceViews.Add(workspaceView);
            }

            (this.View as MainWindowView).HideEmployeeStatusView();
            this.SetActiveWorkspaceView(workspaceView);
        }

        void ShowEmployeeManageView()
        {
            EmployeeManageView workspaceView = workspaceViews.FirstOrDefault(vm => vm is EmployeeManageView) as EmployeeManageView;

            if (workspaceView == null)
            {
                workspaceView = (EmployeeManageView)ViewHelper.CreateView<EmployeeManageViewModel>();
                EmployeeManageViewModel workspace = ViewHelper.GetViewModel(workspaceView) as EmployeeManageViewModel;
                _workspaces.Add(workspace);
                workspaceViews.Add(workspaceView);
            }

            (this.View as MainWindowView).HideEmployeeStatusView();
            this.SetActiveWorkspaceView(workspaceView);
        }

        void ShowLogInHistoryView()
        {
            LogInHistoryView workspaceView = workspaceViews.FirstOrDefault(vm => vm is LogInHistoryView) as LogInHistoryView;

            if (workspaceView == null)
            {
                workspaceView = (LogInHistoryView)ViewHelper.CreateView<LogInHistoryViewModel>();
                LogInHistoryViewModel workspace = ViewHelper.GetViewModel(workspaceView) as LogInHistoryViewModel;
                _workspaces.Add(workspace);
                workspaceViews.Add(workspaceView);
            }

            this.SetActiveWorkspaceView(workspaceView);
        }

        void ShowCashierReportView()
        {
            CashierReportView workspaceView = workspaceViews.FirstOrDefault(vm => vm is CashierReportView) as CashierReportView;

            if (workspaceView == null)
            {
                workspaceView = (CashierReportView)ViewHelper.CreateView<CashierReportViewModel>();
                CashierReportViewModel workspace = ViewHelper.GetViewModel(workspaceView) as CashierReportViewModel;
                workspace.Model.LoggedInUser = this.Model.LoggedInUser;
                workspace.SetReportByAdmin();
                _workspaces.Add(workspace);
                workspaceViews.Add(workspaceView);
            }

            this.SetActiveWorkspaceView(workspaceView);
        }

        void ShowCustomerReportView()
        {
            CustomerReportView workspaceView = workspaceViews.FirstOrDefault(vm => vm is CustomerReportView) as CustomerReportView;

            if (workspaceView == null)
            {
                workspaceView = (CustomerReportView)ViewHelper.CreateView<CustomerReportViewModel>();
                CustomerReportViewModel workspace = ViewHelper.GetViewModel(workspaceView) as CustomerReportViewModel;
                _workspaces.Add(workspace);
                workspaceViews.Add(workspaceView);
            }

            //workspaceView.ShowReport();
            this.SetActiveWorkspaceView(workspaceView);
        }

        void ShowStockOutReportView()
        {
            StockOutReportView workspaceView = workspaceViews.FirstOrDefault(vm => vm is StockOutReportView) as StockOutReportView;

            if (workspaceView == null)
            {
                workspaceView = (StockOutReportView)ViewHelper.CreateView<StockOutReportViewModel>();
                StockOutReportViewModel workspace = ViewHelper.GetViewModel(workspaceView) as StockOutReportViewModel;
                _workspaces.Add(workspace);
                workspaceViews.Add(workspaceView);
            }

            this.SetActiveWorkspaceView(workspaceView);
        }

        void ShowStockReceiptView()
        {
            StockReceiptView workspaceView = workspaceViews.FirstOrDefault(vm => vm is StockReceiptView) as StockReceiptView;

            if (workspaceView == null)
            {
                workspaceView = (StockReceiptView)ViewHelper.CreateView<StockReceiptViewModel>();
                stockReceiptViewModel = ViewHelper.GetViewModel(workspaceView) as StockReceiptViewModel;
                stockReceiptViewModel.InvoicePreviewCommand = this.StockOutPrintViewCommand;
                _workspaces.Add(stockReceiptViewModel);
                workspaceViews.Add(workspaceView);
            }
            (this.View as MainWindowView).HideEmployeeStatusView();
            this.SetActiveWorkspaceView(workspaceView);
        }

        void ShowStockOutPrintView()
        {
            StockOutPrintView workspaceView = workspaceViews.FirstOrDefault(vm => vm is StockOutPrintView) as StockOutPrintView;

            if (workspaceView == null)
            {
                workspaceView = (StockOutPrintView)ViewHelper.CreateView<StockOutPrintViewModel>();
                stockOutPrintViewModel = ViewHelper.GetViewModel(workspaceView) as StockOutPrintViewModel;
                _workspaces.Add(stockOutPrintViewModel);
                workspaceViews.Add(workspaceView);
            }
            stockOutPrintViewModel.PrintTransaction = stockReceiptViewModel.ProcessStockReceipt.Transaction;
            workspaceView.ShowReport();

            this.SetActiveWorkspaceView(workspaceView);
        }

        void CloseActiveWorkspace()
        {
            WorkspaceViewModel workspace = ViewHelper.GetViewModel(ActiveWorkspaceView as IView) as WorkspaceViewModel;
            workspace.Dispose();
            _workspaces.Remove(workspace);
            workspaceViews.Remove(ActiveWorkspaceView);
        }

        void SetActiveWorkspaceView(IView workspaceView)
        {
            Debug.Assert(workspaceViews.Contains(workspaceView));

            ICollectionView collectionView = CollectionViewSource.GetDefaultView(workspaceViews);
            if (collectionView != null)
                collectionView.MoveCurrentTo(workspaceView);
            this.ActiveWorkspaceView = workspaceView;
        }

        private void SelectLanguage(CultureInfo uiCulture)
        {
            if (!uiCulture.Equals(CultureInfo.CurrentUICulture))
            {
                MessageBox.Show(eClinicSpaResources.RestartApplication + "\n\n" +
                                eClinicSpaResources.ResourceManager.GetString("RestartApplication", uiCulture),
                                ApplicationInfo.ProductName);
            }
            newLanguage = uiCulture;
        }

        private void ShowAboutMessage()
        {
            MessageBox.Show(Application.Current.MainWindow,
                string.Format(CultureInfo.CurrentCulture, eClinicSpaResources.AboutText,
                ApplicationInfo.ProductName, ApplicationInfo.Version, ApplicationInfo.Copyright),
                ApplicationInfo.ProductName);
        }

        #endregion // Private Helpers
    }
}
