﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.Common.Dto.Employee;

namespace eClinicSpa.WPF.MainWindow.Model
{
    /// <remarks>
    /// version 0.06 Chapter VI:   MVVM Pattern
    /// version 0.13 Chapter XIII: Async service methods
    /// </remarks> 
    public class MainWindowModel
    {
        public IList<ConsultantStatusViewDto> ConsultantStatusList { get; set; }
        public IList<TherapistStatusViewDto> TherapistStatusList { get; set; }

        public bool IsEnabled { get; set; }
        public EmployeeDto LoggedInUser { get; set; }
    }
}
