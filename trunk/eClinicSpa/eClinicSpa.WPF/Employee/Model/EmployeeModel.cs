﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eClinicSpa.Common.Constant;
using eClinicSpa.Common.Dto.Employee;
using eClinicSpa.Common.Dto.Transaction;

namespace eClinicSpa.WPF.Employee.Model
{
    public class EmployeeModel
    {
        public string SearchFilter { get; set; }
        public IList<EmployeeDto> EmployeeList { get; set; }
        public EmployeeDto SelectedEmployee { get; set; }
        public EmployeeDto EditingEmployee { get; set; }

        public bool IsEnabled { get; set; }
    }
}
