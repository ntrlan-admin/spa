﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Spring.Util;

using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.Employee.ViewModel;

namespace eClinicSpa.WPF.Employee.View
{
    /// <summary>
    /// Interaction logic for EmployeeSearchView.xaml
    /// </summary>
    public partial class EmployeeSearchView : UserControl, IView
    {
        private readonly Lazy<EmployeeManageViewModel> viewModel;

        public EmployeeSearchView()
        {
            InitializeComponent();
            viewModel = new Lazy<EmployeeManageViewModel>(() => ViewHelper.GetViewModel<EmployeeManageViewModel>(this));
        }

        private EmployeeManageViewModel ViewModel { get { return viewModel.Value; } }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox_SelectAll(sender);
        }

        private void SearchByName_GotMouseCapture(object sender, MouseEventArgs e)
        {
            TextBox_SelectAll(sender);
        }

        private void TextBox_SelectAll(object sender)
        {
            TextBox tb = (sender as TextBox);

            if (tb != null)
            {
                tb.SelectAll();
            }
        }
    }
}
