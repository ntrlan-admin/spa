﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.Common.ServiceContract;
using eClinicSpa.Common.Properties;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.Services;
using eClinicSpa.WPF.Util;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Wokspace.ViewModel;
using eClinicSpa.Common.Dto.Transaction;
using eClinicSpa.Common.Dto.Employee;
using eClinicSpa.Common.Constant;
using eClinicSpa.WPF.Employee.View;
using eClinicSpa.WPF.Employee.Model;
using System.Windows;

namespace eClinicSpa.WPF.Employee.ViewModel
{
    public class EmployeeManageViewModel : WorkspaceViewModel
    {
        protected readonly ServiceAdapter<IEmployeeService> EmployeeServiceAdapter;

        public EmployeeManageViewModel(EmployeeManageView view)
            : base(view)
        {
            base.DisplayName = eClinicSpaResources.Employee;
            
            EmployeeServiceAdapter = new ServiceAdapter<IEmployeeService>();

            Model = new EmployeeModel
            {
                EditingEmployee = new EmployeeDto(),
                IsEnabled = true
            };
            EmployeeRoleList = EmployeeRole.EmployeeRoleList;
            SearchEmployee();
        }

        #region Properties

        public EmployeeModel Model { get; set; }

        public IList<EmployeeDto> EmployeeList 
        {
            get
            {
                return Model.EmployeeList;
            }
            set
            {
                if (Model.EmployeeList != value)
                {
                    Model.EmployeeList = value;
                    RaisePropertyChanged("EmployeeList");
                }
            }
        }

        public IList<EmployeeStatus> EmployeeStatusList { get { return AllEmployeeStatus.EmployeeStatusList; } }

        public IList<EmployeeRole> EmployeeRoleList { get; private set; }

        public EmployeeDto SelectedEmployee
        {
            get { return Model.SelectedEmployee; }
            set
            {
                if (Model.SelectedEmployee != value)
                {
                    Model.SelectedEmployee = value;
                    ChangeEditingEmployee();
                }
            }
        }

        public EmployeeDto EditingEmployee
        {
            get { return Model.EditingEmployee; }
            set
            {
                if (Model.EditingEmployee != value)
                {
                    Model.EditingEmployee = value;
                    RaisePropertyChanged("EditingEmployee");
                }
            }
        }

        #endregion

        #region Commands

        private RelayCommand CreateEmployeeCmdInstance;
        public RelayCommand CreateEmployeeCommand
        {
            get
            {
                if (CreateEmployeeCmdInstance != null) return CreateEmployeeCmdInstance;
                CreateEmployeeCmdInstance = new RelayCommand(a => CreateNewEmployee());
                return CreateEmployeeCmdInstance;
            }
        }

        private RelayCommand SaveEmployeeCmdInstance;
        public RelayCommand SaveEmployeeCommand
        {
            get
            {
                if (SaveEmployeeCmdInstance != null) return SaveEmployeeCmdInstance;
                SaveEmployeeCmdInstance = new RelayCommand(a => SaveEmployee(),
                                                          p => Model.EditingEmployee != null && Model.EditingEmployee.IsValid());

                return SaveEmployeeCmdInstance;
            }
        }

        private RelayCommand SearchEmployeeCommandInstance;
        public RelayCommand SearchEmployeeCommand
        {
            get
            {
                if (SearchEmployeeCommandInstance != null) return SearchEmployeeCommandInstance;
                SearchEmployeeCommandInstance = new RelayCommand(a => SearchEmployee());
                return SearchEmployeeCommandInstance;
            }
        }

        #endregion

        #region Private helpers

        private void SaveEmployee()
        {
            try
            {
                if (Model.EditingEmployee.Id == 0)
                {
                    Model.EditingEmployee = EmployeeServiceAdapter.Execute(s => s.CreateNewEmployee(Model.EditingEmployee));
                }
                else
                {
                    Model.EditingEmployee = EmployeeServiceAdapter.Execute(s => s.UpdateEmployee(Model.EditingEmployee));
                }
                RaisePropertyChanged("EditingEmployee");
                MessageBox.Show(Application.Current.MainWindow, "Đã lưu dữ liệu.",
                                ApplicationInfo.ProductName);
                SearchEmployee();
            }
            catch (Exception e)
            {
                MessageBox.Show(Application.Current.MainWindow,
                                "Có lỗi xảy ra, không lưu được dữ liệu.\n\n" + e.StackTrace,
                                ApplicationInfo.ProductName);
            }
        }

        private void CreateNewEmployee()
        {
            EditingEmployee = new EmployeeDto { Status = EmployeeStatus.Active };
            RaisePropertyChanged(() => Model);
        }

        private void SearchEmployee()
        {
            Model.IsEnabled = false;
            RaisePropertyChanged(() => Model);

            RefreshEmployeeList();

            Model.IsEnabled = true;
            RaisePropertyChanged(() => Model);
        }

        public void RefreshEmployeeList()
        {
            if (string.IsNullOrEmpty(Model.SearchFilter))
            {
                var result = EmployeeServiceAdapter.Execute(service => service.FindAll());
                EmployeeList = result.Employees;
            }
            else
            {
                var result = EmployeeServiceAdapter.Execute(service => service.FindByFilter(Model.SearchFilter.Trim()));
                EmployeeList = result.Employees;
            }
        }

        private void ChangeEditingEmployee()
        {
            if (Model.SelectedEmployee == null) return;
            EditingEmployee = EmployeeServiceAdapter.Execute(service => service.GetById(Model.SelectedEmployee.Id));
        }

        #endregion
    }
}
