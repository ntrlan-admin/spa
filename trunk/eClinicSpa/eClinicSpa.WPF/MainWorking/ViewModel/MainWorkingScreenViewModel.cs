﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows;

using Microsoft.Practices.Unity;

using Spring.Util;

using eClinicSpa.Common.Properties;
using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.Common.Dto.ProductCategory;
using eClinicSpa.Common.ServiceContract;
using eClinicSpa.Common.Filter;

using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.Services;
using eClinicSpa.WPF.Util;
using eClinicSpa.WPF.MainWorking.View;
using eClinicSpa.WPF.MainWorking.Model;
using eClinicSpa.WPF.Wokspace.ViewModel;
using eClinicSpa.Common.Dto.Transaction;
using eClinicSpa.Common.Constant;
using eClinicSpa.WPF.MainWindow.ViewModel;


namespace eClinicSpa.WPF.MainWorking.ViewModel
{
    public class MainWorkingScreenViewModel : WorkspaceViewModel
    {
        private readonly ServiceAdapter<ICustomerService> CustomerServiceAdapter;
        private readonly ServiceAdapter<IEmployeeService> EmployeeServiceAdapter;
        private readonly ServiceAdapter<ITransactionService> TransactionServiceAdapter;
        private readonly ServiceAdapter<IProductCategoryService> ProductCategoryServiceAdapter;

        public MainWorkingScreenViewModel(MainWorkingScreen view)
            : base(view)
        {
            base.DisplayName = eClinicSpaResources.MainWorkingScreen_Consulting_Title;
            CustomerServiceAdapter = new ServiceAdapter<ICustomerService>();
            EmployeeServiceAdapter = new ServiceAdapter<IEmployeeService>();
            TransactionServiceAdapter = new ServiceAdapter<ITransactionService>();
            ProductCategoryServiceAdapter = new ServiceAdapter<IProductCategoryService>();
            RefreshAll();
        }

        #region Properties

        public void ChangeDisplayName()
        {
            base.DisplayName = Model.IsReceptionView ? eClinicSpaResources.MainWorkingScreen_Reception_Title :
                eClinicSpaResources.MainWorkingScreen_Consulting_Title;
            RaisePropertyChanged("DisplayName");
            RaisePropertyChanged(() => Model);
        }

        public CustomerModel Model { get; set; }
        public MainWindowViewModel MainWindowViewModel { get; set; }

        public CustomerDto EditingCustomer
        {
            get { return Model.EditCustomerOperation; }
            set
            {
                if (Model.EditCustomerOperation != value)
                {
                    Model.EditCustomerOperation = value;
                    RaisePropertyChanged("EditingCustomer");
                    RaisePropertyChanged("EditingCustomerBirthYear");
                    RaisePropertyChanged("EditingCustomerReferral");
                }
            }
        }

        public DateTime EditingCustomerBirthYear
        {
            get { return EditingCustomer.DayOfBirth; }
            set
            {
                if (EditingCustomer.DayOfBirth != value)
                {
                    EditingCustomer.DayOfBirth = value;
                    RaisePropertyChanged("EditingCustomerBirthYear");
                }
            }
        }

        public CustomerDto EditingCustomerReferral
        {
            get { return EditingCustomer.ReferralCustomer; }
            set
            {
                if (EditingCustomer.ReferralCustomer != value)
                {
                    EditingCustomer.ReferralCustomer = value;
                    RaisePropertyChanged("EditingCustomerReferral");
                }
            }
        }

        public CustomerViewDto SelectedCustomer
        {
            get { return Model.SelectedCustomer; }
            set
            {
                if (Model.SelectedCustomer != value)
                {
                    Model.SelectedCustomer = value;
                    ChangeEditingCustomer();
                    RaisePropertyChanged("SelectedCustomer");
                }
            }
        }

        public DateTime TransactionSearchDate { get; set; }

        public CustomerTransactionViewDto SelectedTransaction
        {
            get { return Model.SelectedTransaction; }
            set
            {
                if (value!= null && Model.SelectedTransaction != value)
                {
                    Model.SelectedTransaction = value;
                    ChangeEditingCustomerWithSelectedTransaction();
                    RaisePropertyChanged("SelectedTransaction");
                }
            }
        }

        public TransactionDto EditingTransaction
        {
            get { return Model.EditTransaction; }
            set
            {
                if (value != null && Model.EditTransaction != value)
                {
                    Model.EditTransaction = value;
                    RaisePropertyChanged("EditingTransaction");
                }
            }
        }

        #endregion Properties

        #region Commands

        private RelayCommand InvoiceBillingCommandInstance;
        public RelayCommand InvoiceBillingCommand 
        {
            get { return InvoiceBillingCommandInstance; }
            set
            {
                if (InvoiceBillingCommandInstance != value)
                {
                    InvoiceBillingCommandInstance = value;
                    RaisePropertyChanged("InvoiceBillingCommand");
                }
            }
        }

        private RelayCommand StockOutCommandInstance;
        public RelayCommand StockOutCommand
        {
            get { return StockOutCommandInstance; }
            set
            {
                if (StockOutCommandInstance != value)
                {
                    StockOutCommandInstance = value;
                    RaisePropertyChanged("StockOutCommand");
                }
            }
        }

        private RelayCommand InvoicePreviewCommandInstance;
        public RelayCommand InvoicePreviewCommand
        {
            get { return InvoicePreviewCommandInstance; }
            set
            {
                if (InvoicePreviewCommandInstance != value)
                {
                    InvoicePreviewCommandInstance = value;
                    RaisePropertyChanged("InvoicePreviewCommand");
                }
            }
        }

        private RelayCommand StockOutPrintViewCommandInstance;
        public RelayCommand StockOutPrintViewCommand
        {
            get { return StockOutPrintViewCommandInstance; }
            set
            {
                if (StockOutPrintViewCommandInstance != value)
                {
                    StockOutPrintViewCommandInstance = value;
                    RaisePropertyChanged("StockOutPrintViewCommand");
                }
            }
        }
        
        private RelayCommand SearchCustomerCommandInstance;
        public RelayCommand SearchCustomerCommand
        {
            get
            {
                if (SearchCustomerCommandInstance != null) return SearchCustomerCommandInstance;
                SearchCustomerCommandInstance = new RelayCommand(a => SearchCustomer());
                return SearchCustomerCommandInstance;
            }
        }

        private RelayCommand SearchReferralCustomerCommandInstance;
        public RelayCommand SearchReferralCustomerCommand
        {
            get
            {
                if (SearchReferralCustomerCommandInstance != null) return SearchReferralCustomerCommandInstance;
                SearchReferralCustomerCommandInstance = new RelayCommand(a => SearchReferralCustomer());
                return SearchReferralCustomerCommandInstance;
            }
        }

        private RelayCommand ChangeReferralCustomerCommandInstance;
        public RelayCommand ChangeReferralCustomerCommand
        {
            get
            {
                if (ChangeReferralCustomerCommandInstance != null) return ChangeReferralCustomerCommandInstance;
                ChangeReferralCustomerCommandInstance = new RelayCommand(a => ChangeReferralCustomer());
                return ChangeReferralCustomerCommandInstance;
            }
        }

        private RelayCommand SearchTransactionCommandInstance;
        public RelayCommand SearchTransactionCommand
        {
            get
            {
                if (SearchTransactionCommandInstance != null) return SearchTransactionCommandInstance;
                SearchTransactionCommandInstance = new RelayCommand(a => SearchTransaction(bool.Parse(a.ToString())));
                return SearchTransactionCommandInstance;
            }
        }

        private RelayCommand CreateCustomerCmdInstance;
        public RelayCommand CreateCustomerCommand
        {
            get
            {
                if (CreateCustomerCmdInstance != null) return CreateCustomerCmdInstance;
                CreateCustomerCmdInstance = new RelayCommand(a => NewCustomer());
                return CreateCustomerCmdInstance;
            }
        }

        private RelayCommand SaveCustomerCmdInstance;
        public RelayCommand SaveCustomerCommand
        {
            get
            {
                if (SaveCustomerCmdInstance != null) return SaveCustomerCmdInstance;
                SaveCustomerCmdInstance = new RelayCommand(a => SaveCustomer(),
                                                           p => Model.EditCustomerOperation.IsValid());

                return SaveCustomerCmdInstance;
            }
        }

        private RelayCommand SetAppointmentCommandInstance;
        public RelayCommand SetAppointmentCommand
        {
            get
            {
                if (SetAppointmentCommandInstance != null) return SetAppointmentCommandInstance;
                SetAppointmentCommandInstance = new RelayCommand(a => SetTransactionStatus(CustomerTransactionStatus.Appointment),
                                                                 p => Model.EditCustomerOperation.IsValid());
                return SetAppointmentCommandInstance;
            }
        }

        private RelayCommand SetCancellationCommandInstance;
        public RelayCommand SetCancellationCommand
        {
            get
            {
                if (SetCancellationCommandInstance != null) return SetCancellationCommandInstance;
                SetCancellationCommandInstance = new RelayCommand(a => DeleteTransaction(EditingTransaction.Id),
                                                                  p => Model.EditCustomerOperation.IsValid());
                return SetCancellationCommandInstance;
            }
        }

        private RelayCommand SetIncomingCommandInstance;
        public RelayCommand SetIncomingCommand
        {
            get
            {
                if (SetIncomingCommandInstance != null) return SetIncomingCommandInstance;
                SetIncomingCommandInstance = new RelayCommand(a => SetTransactionStatus(CustomerTransactionStatus.Incoming),
                                                              p => Model.EditCustomerOperation.IsValid());
                return SetIncomingCommandInstance;
            }
        }

        private RelayCommand SetConsultingCommandInstance;
        public RelayCommand SetConsultingCommand
        {
            get
            {
                if (SetConsultingCommandInstance != null) return SetConsultingCommandInstance;
                SetConsultingCommandInstance = new RelayCommand(a => SetTransactionStatus(CustomerTransactionStatus.Consulting),
                                                                p => Model.EditCustomerOperation.IsValid());
                return SetConsultingCommandInstance;
            }
        }

        private RelayCommand SetTreatingCommandInstance;
        public RelayCommand SetTreatingCommand
        {
            get
            {
                if (SetTreatingCommandInstance != null) return SetTreatingCommandInstance;
                SetTreatingCommandInstance = new RelayCommand(a => SetTransactionStatus(CustomerTransactionStatus.Treating),
                                                              p => Model.EditCustomerOperation.IsValid());
                return SetTreatingCommandInstance;
            }
        }

        private RelayCommand SetFinishingCommandInstance;
        public RelayCommand SetFinishingCommand
        {
            get
            {
                if (SetFinishingCommandInstance != null) return SetFinishingCommandInstance;
                SetFinishingCommandInstance = new RelayCommand(a => SetTransactionStatus(CustomerTransactionStatus.Finished),
                                                               p => Model.EditCustomerOperation.IsValid());
                return SetFinishingCommandInstance;
            }
        }

        public void UpdateCustomerReferral()
        {
            if (EditingCustomerReferral != null || CollectionUtils.IsEmpty(Model.ReferralCustomerList))
                return;

            CustomerViewDto selected = (CustomerViewDto)Model.ReferralCustomerList.First();
            if (selected != null)
            {
                EditingCustomerReferral = CustomerServiceAdapter.Execute(service => service.GetById(selected.Id));
            }
        }

        #endregion

        #region Private helpers

        private void SaveCustomer()
        {
            try
            {
                if (Model.EditCustomerOperation.Id == 0)
                {
                    if (Model.EditCustomerOperation.DayOfBirth.Year < 1900)
                    {
                        MessageBox.Show(Application.Current.MainWindow, "Năm sinh không hợp lệ.",
                                        ApplicationInfo.ProductName);
                        return;
                    }
                    Model.EditCustomerOperation.CreatedOn = DateTime.Now;
                    Model.EditCustomerOperation.ModifiedOn = Model.EditCustomerOperation.CreatedOn;
                    Model.EditCustomerOperation = CustomerServiceAdapter.Execute(s => s.CreateNewCustomer(Model.EditCustomerOperation));
                }
                else
                {
                    Model.EditCustomerOperation.ModifiedOn = DateTime.Now;
                    Model.EditCustomerOperation = CustomerServiceAdapter.Execute(s => s.UpdateCustomer(Model.EditCustomerOperation));
                }
                if (EditingTransaction.Status != CustomerTransactionStatus.Unknown)
                {
                    SaveTransaction(EditingTransaction.Status);
                }

                RaisePropertyChanged("EditingCustomer");
                MessageBox.Show(Application.Current.MainWindow, "Đã lưu dữ liệu.",
                                ApplicationInfo.ProductName);
            }
            catch (Exception e)
            {
                MessageBox.Show(Application.Current.MainWindow,
                                "Có lỗi xảy ra, không lưu được dữ liệu.\n\n" + e.StackTrace,
                                ApplicationInfo.ProductName);
            }
        }

        private void SetTransactionStatus(int status)
        {
            Model.IsEnabled = false;
            RaisePropertyChanged(() => Model);
            SaveCustomer();
            SaveTransaction(status);
            RefreshTransactionList(false);
        }

        private void SaveTransaction(int status)
        {
            EditingTransaction.Status = status;
            switch (status)
            {
                case CustomerTransactionStatus.Appointment:
                    if (EditingTransaction.TransactionDate == null || EditingTransaction.TransactionDate < DateTime.Now)
                    {
                        EditingTransaction.TransactionDate = DateTime.Now;
                    }
                    break;
                case CustomerTransactionStatus.Treating:
                    if (EditingTransaction.TreatmentDate == null)
                    {
                        EditingTransaction.TreatmentDate = DateTime.Now;
                    }
                    break;
                case CustomerTransactionStatus.Finished:
                    if (EditingTransaction.OutGoingDate == null)
                    {
                        EditingTransaction.OutGoingDate = DateTime.Now;
                    }
                    break;
            }
            if (EditingTransaction.Id == 0)
            {
                EditingTransaction.Customer = EditingCustomer;
                Model.EditTransaction = TransactionServiceAdapter.Execute(s => s.CreateNewTransaction(EditingTransaction));
                EditingTransaction = TransactionServiceAdapter.Execute(s => s.GetById(Model.EditTransaction.Id));
            }
            else
            {
                EditingTransaction = TransactionServiceAdapter.Execute(s => s.UpdateTransaction(EditingTransaction));
            }
            SetInConsultant();

            if (EditingTransaction.Status >= CustomerTransactionStatus.Consulting)
            {
                MainWindowViewModel.RefreshEmployeeStatusList();
            }
        }

        private void DeleteTransaction(long transactionId)
        {
            MessageBoxResult result = MessageBox.Show("Chắn chắn xóa giao dịch này?", "Xác nhận xóa giao dịch", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                SaveCustomer();
                TransactionServiceAdapter.Execute(s => s.DeleteTransaction(transactionId));
                NewCustomer();
                RefreshTransactionList(true);
            }
        }

        private void SearchCustomer()
        {
            if (Model.SearchFilter.IsEmpty)
            {
                return;
            }
            Model.IsEnabled = false;
            RaisePropertyChanged(() => Model);

            var result = CustomerServiceAdapter.Execute(service => service.FindViewByFilter(Model.SearchFilter));
            Model.CustomerList = result.Customers;
            
            Model.IsEnabled = true;
            RaisePropertyChanged(() => Model);
        }

        private void SearchReferralCustomer()
        {
            if (Model.ReferralFilter.IsEmpty)
            {
                return;
            }
            Model.IsEnabled = false;
            RaisePropertyChanged(() => Model);

            var result = CustomerServiceAdapter.Execute(service => service.FindViewByFilter(Model.ReferralFilter));
            Model.ReferralCustomerList = result.Customers;
            
            Model.IsEnabled = true;
            RaisePropertyChanged(() => Model);
        }

        private void ChangeReferralCustomer()
        {
            if (Model.SelectedReferralCustomer != null)
            {
                EditingCustomerReferral = CustomerServiceAdapter.Execute(service => service.GetById(Model.SelectedReferralCustomer.Id));
            }
        }
        
        private void SearchTransaction(bool scrollToTop)
        {
            Model.IsEnabled = false;
            RaisePropertyChanged(() => Model);
            RefreshTransactionList(scrollToTop);
        }
        
        private void NewCustomer()
        {
            EditingCustomer = new CustomerDto();
            EditingTransaction = new TransactionDto();
            RaisePropertyChanged(() => Model);
        }

        private void RefreshAll()
        {
            //var result = CustomerServiceAdapter.Execute(service => service.FindByFilter(new CustomerSearchFilter { FirstName = "", LastName = "", Phone = ""}));
            var consultantList = EmployeeServiceAdapter.Execute(service => service.FindAllConsultant());
            var therapistList = EmployeeServiceAdapter.Execute(service => service.FindAllTherapist());
            Model = new CustomerModel()
            {
                EditCustomerOperation = new CustomerDto(),
                EditTransaction = new TransactionDto(),
                TransactionSearchFilter = new CustomerTransactionSearchFilter(),
                SearchFilter = new CustomerSearchFilter(),
                ReferralFilter = new CustomerSearchFilter(),
                ConsultantList = consultantList.Employees,
                TherapistList = therapistList.Employees,
                IsEnabled = true
            };
            TransactionSearchDate = DateTime.Now;

            RaisePropertyChanged(() => Model);
        }

        private void RefreshTransactionList(bool scrollToTop)
        {
            var result = TransactionServiceAdapter.Execute(service => service.FindCustomerTransactionByDate(TransactionSearchDate, Model.TransactionSearchFilter));
            Model.ByDateTransactionList = result.Transactions;
            SelectedTransaction = null;
            if (EditingTransaction.Id != 0)
            {
                Model.SelectedTransaction = Model.ByDateTransactionList.FirstOrDefault(t => t.Id == EditingTransaction.Id);
                RaisePropertyChanged("SelectedTransaction");
            }
            //else
            //{
            //    SelectedTransaction = Model.ByDateTransactionList.Count > 0 ? Model.ByDateTransactionList.First() : null;
            //}

            //if (scrollToTop && Model.ByDateTransactionList.Count > 0)
            //{
            //    MainWorkingScreen thisView = (MainWorkingScreen)View;
            //    if (thisView.CustomerSearchView != null)
            //    {
            //        thisView.CustomerSearchView.ByDateCustomerGrid.SelectedIndex = 0;
            //    }
            //}

            Model.IsEnabled = true;
            RaisePropertyChanged(() => Model);
        }

        private void ChangeEditingCustomer()
        {
            if (SelectedCustomer == null)
            {
                return;
            }
            //Model.IsEnabled = false;
            //RaisePropertyChanged(() => Model);

            var result = CustomerServiceAdapter.Execute(service => service.GetById(SelectedCustomer.Id));
            EditingCustomer = result;
            EditingTransaction = new TransactionDto();
            //if (SelectedTransaction != null)
            //{
            //    RefreshTransactionList(false);
            //}
            Model.IsEnabled = true;
            RaisePropertyChanged(() => Model);
        }

        private void ChangeEditingCustomerWithSelectedTransaction()
        {
            if (SelectedTransaction == null)
            {
                return;
            }
            //Model.IsEnabled = false;
            //RaisePropertyChanged(() => Model);

            EditingTransaction = TransactionServiceAdapter.Execute(service => service.GetById(SelectedTransaction.Id));
            EditingCustomer = CustomerServiceAdapter.Execute(service => service.GetById(EditingTransaction.Customer.Id));

            Model.IsEnabled = true;
            RaisePropertyChanged(() => Model);

            SetInConsultant();
        }

        private void SetInConsultant()
        {
            if (Model.IsReceptionView)
            {
                return;
            }
            if ((EditingTransaction.Status == CustomerTransactionStatus.Incoming ||
                 EditingTransaction.Status == CustomerTransactionStatus.Consulting) &&
                 EditingTransaction.InConsultant == null)
            {
                if (Model.LoggedInUser.EmployeeType == EmployeeRole.TreatmentConsultant ||
                    Model.LoggedInUser.EmployeeType == EmployeeRole.TreatmentConsultantAndTherapist)
                {
                    EditingTransaction.InConsultant = Model.LoggedInUser;
                }
                else
                {
                    EditingTransaction.InConsultant = EditingCustomer.TreatmentConsultant;
                }
                RaisePropertyChanged("EditingTransaction");
            }
        }
        
        #endregion

        public override void Focus()
        {
            //if (SelectedCategory == null)
            //    (View as ProductCategoryList).FocusFirstCell();
            //else
            //    (View as ProductCategoryList).FocusSelectedCell();
        }
    }
}
