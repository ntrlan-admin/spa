﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

using eClinicSpa.Common.Dto.Employee;
using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.Common.Dto.Transaction;
using eClinicSpa.Common.Constant;
using eClinicSpa.Common.Filter;

namespace eClinicSpa.WPF.MainWorking.Model
{
    /// <remarks>
    /// version 0.06 Chapter VI:   MVVM Pattern
    /// version 0.13 Chapter XIII: Async service methods
    /// </remarks> 
    public class CustomerModel
    {
        public CustomerModel()
        {
            UserDistrictList = UserDistrict.UserDistrictList;
        }

        public CustomerDto EditCustomerOperation { get; set; }

        public IList<CustomerTransactionViewDto> ByDateTransactionList { get; set; }
        public CustomerTransactionViewDto SelectedTransaction { get; set; }
        public CustomerTransactionSearchFilter TransactionSearchFilter { get; set; }
        public TransactionDto EditTransaction { get; set; }

        public CustomerSearchFilter SearchFilter { get; set; }
        public IList<CustomerViewDto> CustomerList { get; set; }
        public CustomerViewDto SelectedCustomer { get; set; }

        public IList<UserDistrict> UserDistrictList { get; set; }
        public IList<CustomerTransactionStatus> TransactionStatusList { get { return CustomerTransactionStatus.TransactionStatusList; } }

        public CustomerSearchFilter ReferralFilter { get; set; }
        public IList<CustomerViewDto> ReferralCustomerList { get; set; }
        public CustomerViewDto SelectedReferralCustomer { get; set; }

        public IList<EmployeeDto> ConsultantList { get; set; }
        public IList<EmployeeDto> TherapistList { get; set; }

        public bool IsReceptionView { get; set; }
        public bool IsConsultantView { get { return !IsReceptionView; } }

        public bool IsEnabledInvoiceView { get { return EditTransaction != null && EditTransaction.Id != 0 && EditTransaction.Status > CustomerTransactionStatus.Incoming/* && EditTransaction.Status < CustomerTransactionStatus.Finished*/; } }
        public bool IsFinished { get { return EditTransaction.Status == CustomerTransactionStatus.Finished; } }

        public bool IsEnabled { get; set; }
        public EmployeeDto LoggedInUser { get; set; }
        public bool IsAdministrator { get { return LoggedInUser != null && LoggedInUser.EmployeeType == EmployeeRole.Manager; } }
        public bool IsAllowEditData { get { return IsAdministrator || (EditCustomerOperation != null && EditCustomerOperation.Id == 0); } }
        public bool IsMaskedData { get { return !IsAdministrator && !IsAllowEditData; } }
        public bool IsStaff { get { return LoggedInUser != null && LoggedInUser.EmployeeType == EmployeeRole.Staff; } }
        public bool IsNotStaff { get { return LoggedInUser != null && LoggedInUser.EmployeeType != EmployeeRole.Staff; } }

        public bool CanEditPhone
        {
            get
            {
                return (this.EditCustomerOperation.Id == 0);
            }
        }
        public bool CannotEditPhone
        {
            get
            {
                return (!this.CanEditPhone);
            }
        }
    }
}
