﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Spring.Util;

using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.MainWorking.Model;
using eClinicSpa.WPF.MainWorking.ViewModel;

namespace eClinicSpa.WPF.MainWorking.View
{
    /// <summary>
    /// Interaction logic for CustomerSearchByDateView.xaml
    /// </summary>
    public partial class CustomerSearchByDateView : UserControl, IView
    {
        private readonly Lazy<MainWorkingScreenViewModel> viewModel;

        public CustomerSearchByDateView()
        {
            InitializeComponent();
            viewModel = new Lazy<MainWorkingScreenViewModel>(() => ViewHelper.GetViewModel<MainWorkingScreenViewModel>(this));
        }

        private MainWorkingScreenViewModel ViewModel { get { return viewModel.Value; } }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (ViewModel.Model.IsAdministrator)
            {
                ByDateCustomerGrid.Columns[6].Visibility = Visibility.Visible;
                ByDateCustomerGrid.Columns[7].Visibility = Visibility.Hidden;
            }
            else
            {
                ByDateCustomerGrid.Columns[6].Visibility = Visibility.Hidden;
                ByDateCustomerGrid.Columns[7].Visibility = Visibility.Visible;
            }
        }

        private void ByDateCustomerGrid_TargetUpdated(object sender, DataTransferEventArgs e)
        {
            ByDateCustomerGrid.Columns[0].Width = 
            ByDateCustomerGrid.Columns[1].Width = 
            ByDateCustomerGrid.Columns[2].Width = 
            ByDateCustomerGrid.Columns[3].Width = 
            ByDateCustomerGrid.Columns[4].Width = 
            ByDateCustomerGrid.Columns[5].Width = 
            ByDateCustomerGrid.Columns[6].Width = 
            ByDateCustomerGrid.Columns[7].Width = 0;
            //ByDateCustomerGrid.UpdateLayout();
            ByDateCustomerGrid.Columns[0].Width = 
            ByDateCustomerGrid.Columns[1].Width = 
            ByDateCustomerGrid.Columns[2].Width = 
            ByDateCustomerGrid.Columns[3].Width = 
            ByDateCustomerGrid.Columns[4].Width = 
            ByDateCustomerGrid.Columns[5].Width = 
            ByDateCustomerGrid.Columns[6].Width = 
            ByDateCustomerGrid.Columns[7].Width = DataGridLength.Auto;
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            if (ByDateCustomerGrid.Items.Count > 0)
            {
                var first = ByDateCustomerGrid.Items[0];
                ByDateCustomerGrid.ScrollIntoView(first);
                ByDateCustomerGrid.UpdateLayout();
                //ByDateCustomerGrid.ScrollIntoView(ByDateCustomerGrid.Items[ByDateCustomerGrid.Items.Count - 1]);
            }
        }

        //private void ByDateCustomerGrid_LoadingRow(object sender, DataGridRowEventArgs e)
        //{
        //    if (ByDateCustomerGrid.Columns[1].Width == 30)
        //    {
        //        ByDateCustomerGrid.Columns[1].Width = 10;
        //        ByDateCustomerGrid.Columns[1].Width = DataGridLength.Auto;
        //    }
        //}
    }
}
