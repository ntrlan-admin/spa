﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.MainWorking.ViewModel;

namespace eClinicSpa.WPF.MainWorking.View
{
    /// <summary>
    /// Interaction logic for CustomerSearchView.xaml
    /// </summary>
    public partial class CustomerSearchView : UserControl, IView
    {
        private readonly Lazy<MainWorkingScreenViewModel> viewModel;

        public CustomerSearchView()
        {
            InitializeComponent();
            viewModel = new Lazy<MainWorkingScreenViewModel>(() => ViewHelper.GetViewModel<MainWorkingScreenViewModel>(this));
        }

        private MainWorkingScreenViewModel ViewModel { get { return viewModel.Value; } }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox_SelectAll(sender);
        }

        private void TextBox_GotMouseCapture(object sender, MouseEventArgs e)
        {
            TextBox_SelectAll(sender);
        }

        private void TextBox_SelectAll(object sender)
        {
            TextBox tb = (sender as TextBox);

            if (tb != null)
            {
                tb.SelectAll();
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (ViewModel.Model.IsAdministrator)
            {
                CustomerListGrid.Columns[3].Visibility = Visibility.Visible;
                CustomerListGrid.Columns[4].Visibility = Visibility.Hidden;
            }
            else
            {
                CustomerListGrid.Columns[3].Visibility = Visibility.Hidden;
                CustomerListGrid.Columns[4].Visibility = Visibility.Visible;
            }
            //SearchFirstName.AddHandler(FrameworkElement.MouseLeftButtonUpEvent, new MouseButtonEventHandler(TextBox_MouseLeftButtonUp), true);
            //SearchLastName.AddHandler(FrameworkElement.MouseLeftButtonUpEvent, new MouseButtonEventHandler(TextBox_MouseLeftButtonUp), true);
            //SearchPhone.AddHandler(FrameworkElement.MouseLeftButtonUpEvent, new MouseButtonEventHandler(TextBox_MouseLeftButtonUp), true);
        }

        private void CustomerListGrid_TargetUpdated(object sender, DataTransferEventArgs e)
        {
            CustomerListGrid.Columns[0].Width =
            CustomerListGrid.Columns[1].Width =
            CustomerListGrid.Columns[2].Width =
            CustomerListGrid.Columns[3].Width =
            CustomerListGrid.Columns[4].Width = 0;
            CustomerListGrid.UpdateLayout();
            CustomerListGrid.Columns[0].Width =
            CustomerListGrid.Columns[1].Width =
            CustomerListGrid.Columns[2].Width =
            CustomerListGrid.Columns[3].Width =
            CustomerListGrid.Columns[4].Width = DataGridLength.Auto;
        }
    }
}
