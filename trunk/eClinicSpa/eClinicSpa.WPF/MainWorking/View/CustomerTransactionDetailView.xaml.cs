﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.MainWorking.Model;
using eClinicSpa.WPF.MainWorking.ViewModel;

using Xceed.Wpf.Toolkit;

namespace eClinicSpa.WPF.MainWorking.View
{
    /// <summary>
    /// Interaction logic for CustomerTransactionDetailView.xaml
    /// </summary>
    public partial class CustomerTransactionDetailView : UserControl, IView
    {
        private readonly Lazy<MainWorkingScreenViewModel> viewModel;

        public CustomerTransactionDetailView()
        {
            InitializeComponent();
            viewModel = new Lazy<MainWorkingScreenViewModel>(() => ViewHelper.GetViewModel<MainWorkingScreenViewModel>(this));
        }

        private MainWorkingScreenViewModel ViewModel { get { return viewModel.Value; } }

        private void chkSourceIntroduced_Checked(object sender, RoutedEventArgs e)
        {
            ViewModel.UpdateCustomerReferral();
        }

        private void chkSourceIntroduced_Unchecked(object sender, RoutedEventArgs e)
        {
            ViewModel.EditingCustomerReferral = null;
        }

        private void chkSourceOther_Unchecked(object sender, RoutedEventArgs e)
        {
            ViewModel.EditingCustomer.KnownByOther = null;
        }

        private void ShowPopupReferral_Click(object sender, RoutedEventArgs e)
        {
            PopupReferral.IsOpen = true;
        }

        private void ClosePopupReferral_Click(object sender, RoutedEventArgs e)
        {
            PopupReferral.IsOpen = false;
        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox_SelectAll(sender);
        }

        private void TextBox_GotMouseCapture(object sender, MouseEventArgs e)
        {
            TextBox_SelectAll(sender);
        }

        private void TextBox_SelectAll(object sender)
        {
            TextBox tb = (sender as TextBox);

            if (tb != null)
            {
                tb.SelectAll();
            }
        }
    }
}
