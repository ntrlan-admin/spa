﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

using eClinicSpa.Common.Properties;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.MainWorking.ViewModel;

namespace eClinicSpa.WPF.MainWorking.View
{
    /// <summary>
    /// Interaction logic for MainWorkingScreen.xaml
    /// </summary>
    public partial class MainWorkingScreen : UserControl, IView
    {
        private readonly Lazy<MainWorkingScreenViewModel> viewModel;

        public MainWorkingScreen()
        {
            InitializeComponent();
            viewModel = new Lazy<MainWorkingScreenViewModel>(() => ViewHelper.GetViewModel<MainWorkingScreenViewModel>(this));
        }

        private MainWorkingScreenViewModel ViewModel { get { return viewModel.Value; } }
    }
}
