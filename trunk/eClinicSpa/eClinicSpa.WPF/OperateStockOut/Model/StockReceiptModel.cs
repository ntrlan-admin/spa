﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eClinicSpa.Common.Constant;
using eClinicSpa.Common.Dto.Employee;
using eClinicSpa.Common.Dto.Product;
using eClinicSpa.Common.Dto.Transaction;

namespace eClinicSpa.WPF.OperateStockOut.Model
{
    public class StockReceiptModel
    {
        public StockReceiptModel()
        {
        }

        public DateTime StockReceiptSearchDate { get; set; }
        public IList<InvoiceDto> StockReceiptList { get; set; }
        public InvoiceDto SelectedStockReceipt { get; set; }
        public InvoiceDto ProcessStockReceipt { get; set; }

        public bool IsEnabled { get; set; }
    }
}
