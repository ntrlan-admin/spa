﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.Common.ServiceContract;
using eClinicSpa.Common.Properties;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.OperateStockOut.Model;
using eClinicSpa.WPF.OperateStockOut.View;
using eClinicSpa.WPF.Services;
using eClinicSpa.WPF.Util;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.InvoiceBilling.View;
using eClinicSpa.WPF.Wokspace.ViewModel;
using eClinicSpa.WPF.ProductCategory.ViewModel;
using eClinicSpa.Common.Dto.Transaction;
using eClinicSpa.Common.Dto.Employee;
using eClinicSpa.Common.Dto.Product;
using eClinicSpa.Common.Constant;

namespace eClinicSpa.WPF.OperateStockOut.ViewModel
{
    public class StockReceiptViewModel : WorkspaceViewModel
    {
        protected readonly ServiceAdapter<IEmployeeService> EmployeeServiceAdapter;
        protected readonly ServiceAdapter<ICustomerService> CustomerServiceAdapter;
        protected readonly ServiceAdapter<IProductCategoryService> ProductCategoryServiceAdapter;
        protected readonly ServiceAdapter<ITransactionService> TransactionServiceAdapter;

        public StockReceiptViewModel(StockReceiptView view)
            : base(view)
        {
            base.DisplayName = eClinicSpaResources.InvoiceBilling_StockOut_Title;
            
            EmployeeServiceAdapter = new ServiceAdapter<IEmployeeService>();
            CustomerServiceAdapter = new ServiceAdapter<ICustomerService>();
            ProductCategoryServiceAdapter = new ServiceAdapter<IProductCategoryService>();
            TransactionServiceAdapter = new ServiceAdapter<ITransactionService>();

            Model = new StockReceiptModel
            {
                StockReceiptSearchDate = DateTime.Now,
                IsEnabled = true
            };
        }

        #region Properties

        public StockReceiptModel Model { get; set; }
        public String InvoiceTitle { get { return base.DisplayName; } }

        public IList<InvoiceDto> StockReceiptList 
        {
            get
            {
                return Model.StockReceiptList;
            }
            set
            {
                if (Model.StockReceiptList != value)
                {
                    Model.StockReceiptList = value;
                    RaisePropertyChanged("StockReceiptList");
                }
            }
        }

        public InvoiceDto SelectedStockReceipt
        {
            get { return Model.SelectedStockReceipt; }
            set
            {
                if (Model.SelectedStockReceipt != value)
                {
                    Model.SelectedStockReceipt = value;
                    ChangeProcessStockReceipt();
                }
            }
        }

        public InvoiceDto ProcessStockReceipt
        {
            get { return Model.ProcessStockReceipt; }
            set
            {
                if (Model.ProcessStockReceipt != value)
                {
                    Model.ProcessStockReceipt = value;
                    RaisePropertyChanged("ProcessStockReceipt");
                }
            }
        }

        public void NotifyPropertyChanged(string propertyName)
        {
            RaisePropertyChanged(propertyName);
        }

        #endregion

        #region Commands

        private RelayCommand ReturnToBillingCommandInstance;
        public RelayCommand ReturnToBillingCommand
        {
            get
            {
                if (ReturnToBillingCommandInstance != null) return ReturnToBillingCommandInstance;
                ReturnToBillingCommandInstance = new RelayCommand(a => ReturnToBilling(),
                    p => ProcessStockReceipt != null && ProcessStockReceipt.Status == PaymentStatus.PrepareStockOut);
                return ReturnToBillingCommandInstance;
            }
        }

        public void ReturnToBilling()
        {
            ProcessStockReceipt.Status = PaymentStatus.NotPaid;
            TransactionServiceAdapter.Execute(s => s.UpdateInvoice(ProcessStockReceipt));
            MessageBox.Show(Application.Current.MainWindow, "Đã trả về Tư vấn", ApplicationInfo.ProductName);
            SearchStockReceipt();
        }

        private RelayCommand SearchStockReceiptCommandInstance;
        public RelayCommand SearchStockReceiptCommand
        {
            get
            {
                if (SearchStockReceiptCommandInstance != null) return SearchStockReceiptCommandInstance;
                SearchStockReceiptCommandInstance = new RelayCommand(a => SearchStockReceipt());
                return SearchStockReceiptCommandInstance;
            }
        }
        private void SearchStockReceipt()
        {
            Model.IsEnabled = false;
            RaisePropertyChanged(() => Model);

            RefreshStockReceiptList();
            SelectedStockReceipt = null;
            ProcessStockReceipt = null;

            Model.IsEnabled = true;
            RaisePropertyChanged(() => Model);
        }

        private RelayCommand InvoicePreviewCommandInstance;
        public RelayCommand InvoicePreviewCommand
        {
            get { return InvoicePreviewCommandInstance; }
            set
            {
                if (InvoicePreviewCommandInstance != value)
                {
                    InvoicePreviewCommandInstance = value;
                    RaisePropertyChanged("InvoicePreviewCommand");
                }
            }
        }

        private RelayCommand FinishStockReceiptCommandInstance;
        public RelayCommand FinishStockReceiptCommand
        {
            get
            {
                if (FinishStockReceiptCommandInstance != null) return FinishStockReceiptCommandInstance;
                FinishStockReceiptCommandInstance = new RelayCommand(a => FinishStockReceipt(), p => ProcessStockReceipt != null && ProcessStockReceipt.IsValid());
                return FinishStockReceiptCommandInstance;
            }
        }

        public void FinishStockReceipt()
        {
            ProcessStockReceipt.Status = PaymentStatus.StockOut;
            ProcessStockReceipt = TransactionServiceAdapter.Execute(s => s.UpdateInvoice(ProcessStockReceipt));
            SearchStockReceipt();
        }

        #endregion

        #region Private helpers

        public void RefreshStockReceiptList()
        {
            var result = TransactionServiceAdapter.Execute(service => service.FindInvoiceByDateAndStatus(Model.StockReceiptSearchDate, PaymentStatus.PrepareStockOut));
            StockReceiptList = result.Invoices;
        }

        private void ChangeProcessStockReceipt()
        {
            if (Model.SelectedStockReceipt == null) return;
            ProcessStockReceipt = TransactionServiceAdapter.Execute(service => service.GetInvoiceById(Model.SelectedStockReceipt.Id));
        }

        #endregion
    }
}
