﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.MainWorking.Model;
using eClinicSpa.WPF.OperateStockOut.ViewModel;
using eClinicSpa.Common.Dto.Transaction;
using eClinicSpa.Common.Dto.Product;
using eClinicSpa.Common.Constant;

namespace eClinicSpa.WPF.OperateStockOut.View
{
    /// <summary>
    /// Interaction logic for StockReceiptProcessView.xaml
    /// </summary>
    public partial class StockReceiptProcessView : UserControl, IView
    {
        private static DispatcherTimer timerRefreshStockReceiptList = new DispatcherTimer();

        private readonly Lazy<StockReceiptViewModel> viewModel;

        public StockReceiptProcessView()
        {
            InitializeComponent();
            viewModel = new Lazy<StockReceiptViewModel>(() => ViewHelper.GetViewModel<StockReceiptViewModel>(this));

            // Hook up the Elapsed event for the timer.
            timerRefreshStockReceiptList.Tick += new EventHandler(RefreshStockReceiptListOnTimedEvent);

            // Set the Interval to 10 seconds (10000 milliseconds).
            timerRefreshStockReceiptList.Interval = new TimeSpan(0, 0, 10);
            timerRefreshStockReceiptList.Start();
        }

        private StockReceiptViewModel ViewModel { get { return viewModel.Value; } }

        // RefreshStockReceiptList when the Elapsed event is raised.
        private void RefreshStockReceiptListOnTimedEvent(object source, EventArgs e)
        {
            ViewModel.RefreshStockReceiptList();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            timerRefreshStockReceiptList.Start();
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            timerRefreshStockReceiptList.Stop();
        }
    }
}
