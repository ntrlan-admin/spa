﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Spring.Util;

using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.OperateStockOut.Model;
using eClinicSpa.WPF.OperateStockOut.ViewModel;

namespace eClinicSpa.WPF.OperateStockOut.View
{
    /// <summary>
    /// Interaction logic for StockReceiptSearchByDateView.xaml
    /// </summary>
    public partial class StockReceiptSearchByDateView : UserControl, IView
    {
        private readonly Lazy<StockReceiptViewModel> viewModel;

        public StockReceiptSearchByDateView()
        {
            InitializeComponent();
            viewModel = new Lazy<StockReceiptViewModel>(() => ViewHelper.GetViewModel<StockReceiptViewModel>(this));
        }

        private StockReceiptViewModel ViewModel { get { return viewModel.Value; } }

        private void ByDateInvoiceGrid_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = (e.Row.GetIndex() + 1).ToString();
        }
    }
}
