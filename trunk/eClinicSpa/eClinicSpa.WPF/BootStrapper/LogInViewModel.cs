﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using eClinicSpa.Common.Dto.Employee;
using eClinicSpa.Common.ServiceContract;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.Services;
using eClinicSpa.WPF.Util;

namespace eClinicSpa.WPF.BootStrapper
{
    public class LogInViewModel : INotifyPropertyChanged
    {
        private LogInForm View;
        private readonly ServiceAdapter<IEmployeeService> EmployeeServiceAdapter;

        public LogInViewModel()
        {
            View = new LogInForm{DataContext = this};
            EmployeeServiceAdapter = new ServiceAdapter<IEmployeeService>();
            IsEnabled = true;
        }

        public String UserName { get; set; }
        private String Password { get { return View.humm.Password; } }

        private RelayCommand ExitCommandInstance;
        public RelayCommand ExitCommand
        {
            get
            {
                if (ExitCommandInstance != null) return ExitCommandInstance;
                ExitCommandInstance = new RelayCommand(a => View.Close());
                return ExitCommandInstance;
            }
        }

        private RelayCommand LogInCommandInstance;
        public RelayCommand LogInCommand
        {
            get
            {
                if (LogInCommandInstance != null) return LogInCommandInstance;
                LogInCommandInstance = new RelayCommand(a => CheckAuthentication(),
                                                        p => !String.IsNullOrEmpty(UserName) && !String.IsNullOrEmpty(Password));
                return LogInCommandInstance;
            }
        }

        public bool IsEnabled { get; set; }
        public EmployeeDto LoggedInUser { get; set; }

        private void CheckAuthentication()
        {
            IsEnabled = false;
            NotifyPropertyChanged("IsEnabled");
            try
            {
                var employees = EmployeeServiceAdapter.Execute(service => service.GetByCredential(UserName, Password));
                if (employees.Employees.Count() > 0)
                {
                    IsAuthenticated = true;
                    LoggedInUser = employees.Employees.First();
                    LogInHistoryDto logInRecord = new LogInHistoryDto { LogInDate = DateTime.Now, LogInUser = LoggedInUser };
                    EmployeeServiceAdapter.Execute(service => service.CreateNewLogInHistory(logInRecord));
                    View.Close();
                }
                else
                {
                    MessageBox.Show(Application.Current.MainWindow,
                                    "Tên hoặc mật khẩu không đúng.",
                                    ApplicationInfo.ProductName);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Application.Current.MainWindow,
                                "Có lỗi xảy ra khi đăng nhập.\n\n" + e.StackTrace,
                                ApplicationInfo.ProductName);
            }
            IsEnabled = true;
            NotifyPropertyChanged("IsEnabled");
        }

        public bool IsAuthenticated { get; set; }
        public bool GetAuthentication(string lastUserName)
        {
            UserName = lastUserName;
            View.ShowDialog();
            return IsAuthenticated;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
