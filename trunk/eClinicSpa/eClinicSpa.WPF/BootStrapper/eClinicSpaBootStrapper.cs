﻿using System;
using System.Configuration;
using System.Globalization;
using System.Threading;
using System.Windows;
using System.Windows.Markup;
using eClinicSpa.Common.DependencyInjection;
using eClinicSpa.WPF.MainWindow.View;
using eClinicSpa.WPF.MainWindow.ViewModel;
using eClinicSpa.WPF.Properties;
using Spring.Context.Support;

namespace eClinicSpa.WPF.BootStrapper
{
    /// <remarks>
    /// version 0.08 Chapter VIII: Relay Command
    /// version 0.10 Chapter X:    Dependency Injection
    /// version 0.13 Chapter XIII: Business Domain Extension
    /// </remarks>
    class eClinicSpaBootStrapper
    {
        public void Run()
        {
            // Ensure the current culture passed into bindings is the OS culture.
            // By default, WPF uses en-US as the culture, regardless of the system settings.
            //
            FrameworkElement.LanguageProperty.OverrideMetadata(typeof(FrameworkElement), new FrameworkPropertyMetadata(
                XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));

            InitialiseDependencies();
            InitializeCultures();

            LogInViewModel logInForm = new LogInViewModel();
            var result = logInForm.GetAuthentication(Settings.Default.LastLoggedInUserName);
            if (!result) return;

            var mainWindowView = new MainWindowView();
            var mainViewModel = new MainWindowViewModel(mainWindowView);
            mainViewModel.Model.LoggedInUser = logInForm.LoggedInUser;

            EventHandler handlerMainView = null;
            handlerMainView = delegate
            {
                mainViewModel.RequestClose -= handlerMainView;
                mainWindowView.Close();
            };
            mainViewModel.RequestClose += handlerMainView;

            mainWindowView.DataContext = mainViewModel;
            mainWindowView.ShowDialog();
        }

        private void InitialiseDependencies()
        {
            var spring = ConfigurationManager.AppSettings.Get("SpringConfigFile");
            DiContext.AppContext = new XmlApplicationContext(spring);
        }

        private static void InitializeCultures()
        {
            if (!String.IsNullOrEmpty(Settings.Default.Culture))
            {
                CultureInfo ci = CultureInfo.CreateSpecificCulture(Settings.Default.Culture);
                ci.DateTimeFormat.ShortDatePattern = Settings.Default.ShortDateFormat;// "dd/MM/yyyy";
                Thread.CurrentThread.CurrentCulture = ci;
            }
            if (!String.IsNullOrEmpty(Settings.Default.UICulture))
            {
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Settings.Default.UICulture);
            }
        }
    }
}
