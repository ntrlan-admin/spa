﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace eClinicSpa.WPF.Services.UI
{
    public interface IDialogService
    {
        int Width { get; set; }
        int Height { get; set; }
        void Show(string title, string message, Action<DialogResult> onClosedCallback);
    }
}
