﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using eClinicSpa.WPF.Controls;

namespace eClinicSpa.WPF.Services.UI
{
    public class DialogService : IDialogService
    {
        public int Width { get; set; }
        public int Height { get; set; }

        public void Show(string title, string message, Action<DialogResult> onClosedCallback)
        {
            // Instantiate a custom ChildWindow that contains a message body that we 
            // can set in the constructor
            ConfirmationDialog dialog = new ConfirmationDialog(message);

            // Set the title of the dialog
            dialog.Title = title;

            // When the dialog is closed we call the callback (if it exists) with the 
            // result.
            dialog.Closed += (s, e) =>
            {
                if (onClosedCallback != null)
                {
                    DialogResult result = new DialogResult();
                    //result.Result = dialog.DialogResult;
                    onClosedCallback(result);
                }
            };

            dialog.Width = Width;
            dialog.Height = Height;
            dialog.Show();

        }
    }
}
