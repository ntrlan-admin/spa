﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using eClinicSpa.Common.Message;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.ExceptionNotifier.ViewModel;

namespace eClinicSpa.WPF.Services
{
    class BusinessExceptionManager
        :IBusinessExceptionManager
    {
        public void HandleBusinessException(BusinessExceptionDto exceptionDto)
        {
            new ExceptionNotifierViewModel(exceptionDto);
            throw new SuspendProcessException();
        }
    }
}
