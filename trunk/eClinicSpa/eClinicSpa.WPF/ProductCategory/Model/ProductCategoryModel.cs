﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

using eClinicSpa.Common.Dto.ProductCategory;

namespace eClinicSpa.WPF.ProductCategory.Model
{
    /// <remarks>
    /// version 0.06 Chapter VI:   MVVM Pattern
    /// version 0.13 Chapter XIII: Async service methods
    /// </remarks> 
    public class ProductCategoryModel
    {
        public ProductCategoryDto NewProductCategoryOperation { get; set; }
        public ObservableCollection<ProductCategoryDto> ProductCategoryList { get; set; }

        public bool IsEnabled { get; set; }
    }
}
