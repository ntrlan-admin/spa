﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows;

using Microsoft.Practices.Unity;

using eClinicSpa.Common.Properties;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.Util;
using eClinicSpa.WPF.ProductCategory.View;
using eClinicSpa.WPF.Wokspace.ViewModel;

namespace eClinicSpa.WPF.ProductCategory.ViewModel
{
    public class ProductCategoryEditViewModel : WorkspaceViewModel
    {
        //private ICategoryServices categoryServices;

        //private Category category;

        private ICommand backToListCommand;
        private ICommand addNewCommand;
        private ICommand saveCommand;
        private ICommand deleteCommand;

        public ProductCategoryEditViewModel(ProductCategoryEdit view)
            : base(view)
        {
            //category = new Category();
        }

        //public ICategoryServices CategoryServices
        //{
        //    get { return categoryServices; }
        //    set
        //    {
        //        if (categoryServices != value)
        //        {
        //            categoryServices = value;
        //            OnPropertyChanged("CategoryServices");
        //        }
        //    }
        //}


        //public string CategoryCode
        //{
        //    get { return category.CategoryCode; }
        //    set
        //    {
        //        if (category.CategoryCode != value)
        //        {
        //            category.CategoryCode = value;
        //            base.OnPropertyChanged("CategoryCode");
        //        }
        //    }
        //}

        //public string CategoryName
        //{
        //    get { return category.CategoryName; }
        //    set
        //    {
        //        if (category.CategoryName != value)
        //        {
        //            category.CategoryName = value;
        //            base.OnPropertyChanged("CategoryName");
        //        }
        //    }
        //}

        //public Category ParentCategory
        //{
        //    get { return category.SuperCategory; }
        //    set
        //    {
        //        if (category.SuperCategory != value)
        //        {
        //            category.SuperCategory = value;
        //            base.OnPropertyChanged("ParentCategory");
        //        }
        //    }
        //}

        //public Category ProductCategory
        //{
        //    get { return category; }
        //    set
        //    {
        //        category = value;
        //        base.OnPropertyChanged("ProductCategory");
        //        base.OnPropertyChanged("CategoryCode");
        //        base.OnPropertyChanged("CategoryName");
        //        base.OnPropertyChanged("ParentCategory");
        //    }
        //}

        //public ObservableCollection<Category> ParentCategories { get { return new ObservableCollection<Category>(CategoryServices.GetAll()); } }

        public ICommand BackToListCommand
        {
            get { return backToListCommand; }
            set
            {
                if (backToListCommand != value)
                {
                    backToListCommand = value;
                    RaisePropertyChanged("BackToListCommand");
                }
            }
        }

        public ICommand AddNewCommand
        {
            get { return addNewCommand; }
            set
            {
                if (addNewCommand != value)
                {
                    addNewCommand = value;
                    RaisePropertyChanged("AddNewCommand");
                }
            }
        }

        public ICommand SaveCommand
        {
            get { return saveCommand; }
            set
            {
                if (saveCommand != value)
                {
                    saveCommand = value;
                    RaisePropertyChanged("SaveCommand");
                }
            }
        }

        public ICommand DeleteCommand
        {
            get { return deleteCommand; }
            set
            {
                if (deleteCommand != value)
                {
                    deleteCommand = value;
                    RaisePropertyChanged("DeleteCommand");
                }
            }
        }

        public void RefreshParentCategories()
        {
            RaisePropertyChanged("ParentCategories");
        }

        public override void Focus()
        {
            (View as ProductCategoryEdit).FocusContent();
        }
    }
}
