﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows;

using Microsoft.Practices.Unity;

using eClinicSpa.Common.Properties;
using eClinicSpa.Common.Dto.ProductCategory;
using eClinicSpa.Common.ServiceContract;

using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.Util;
using eClinicSpa.WPF.ProductCategory.View;
using eClinicSpa.WPF.Wokspace.ViewModel;
using eClinicSpa.WPF.Services;

namespace eClinicSpa.WPF.ProductCategory.ViewModel
{
    public class ProductCategoriesViewModel : WorkspaceViewModel
    {
        //public ICategoryServices categoryServices;

        private ProductCategoryList categoryListView;
        private ProductCategoryListViewModel categoryListViewModel;
        private ProductCategoryEdit categoryEditView;
        //private ProductCategoryEditViewModel categoryEditViewModel;

        private ICommand backToListCommand;
        private ICommand addNewCommand;
        private ICommand editCommand;
        private ICommand saveCommand;
        private ICommand deleteCommand;


        public ProductCategoriesViewModel(ProductCategoriesView view)
            : base(view)
        {
            base.DisplayName = eClinicSpaResources.ProductCategories_Title;

            CreateProductCategoryListView();
            (View as ProductCategoriesView).ContentViewState = ContentViewState.View_ListViewVisible;
        }

        public ProductCategoryList CategoryListView
        {
            get
            {
                return categoryListView;
            }
            set
            {
                if (categoryListView != value)
                {
                    categoryListView = value;
                    RaisePropertyChanged("CategoryListView");
                }
            }
        }

        public ProductCategoryEdit CategoryEditView
        {
            get
            {
                return categoryEditView;
            }
            set
            {
                if (categoryEditView != value)
                {
                    categoryEditView = value;
                    RaisePropertyChanged("CategoryEditView");
                }
            }
        }

        private void CreateProductCategoryListView()
        {
            categoryListView = (ProductCategoryList)ViewHelper.CreateView<ProductCategoryListViewModel>();
            categoryListViewModel = ViewHelper.GetViewModel(categoryListView as IView) as ProductCategoryListViewModel;
        }

        //private void CreateProductCategoryEditView()
        //{
        //    categoryEditView = ViewHelper.CreateProductCategoryEditView();
        //    categoryEditViewModel = ViewHelper.GetViewModel(categoryEditView as IView) as ProductCategoryEditViewModel;

        //    categoryEditViewModel.CategoryServices = this.categoryServices;

        //    backToListCommand = new RelayCommand(a => this.BackToListCategory());
        //    saveCommand = new RelayCommand(a => this.SaveCategory());
        //    deleteCommand = new RelayCommand(a => this.DeleteCategory());

        //    categoryEditViewModel.BackToListCommand = backToListCommand;
        //    categoryEditViewModel.AddNewCommand = addNewCommand;
        //    categoryEditViewModel.SaveCommand = saveCommand;
        //    categoryEditViewModel.DeleteCommand = deleteCommand;

        //    OnPropertyChanged("CategoryEditView");
        //}

        //private void DeleteCategory()
        //{
        //    if (categoryEditView == null)
        //        CreateProductCategoryEditView();

        //    if (categoryServices.IsNewCategory(categoryEditViewModel.ProductCategory))
        //        return;

        //    // Use the CategoryCollectionView, which represents the sorted/filtered state of the categories, to determine the next category to select.
        //    IEnumerable<Category> categoriesToExclude = categoryListViewModel.SelectedCategories.Except(new[] { categoryListViewModel.SelectedCategory });
        //    Category nextCategory = CollectionHelper.GetNextElementOrDefault(categoryListViewModel.CategoryCollectionView.Except(categoriesToExclude), 
        //        categoryListViewModel.SelectedCategory);

        //    categoryServices.DeleteCategory(categoryEditViewModel.ProductCategory);
        //    categoryListViewModel.ProductCategories.Remove(categoryEditViewModel.ProductCategory);

        //    categoryListViewModel.RefreshCategoryList();
        //    categoryListViewModel.SelectedCategory = nextCategory ?? categoryListViewModel.CategoryCollectionView.LastOrDefault();

        //    (View as ProductCategoriesView).ContentViewState = ContentViewState.View_ListViewVisible;
        //    categoryListViewModel.Focus();
        //}

        //private void AddNewCategory()
        //{
        //    if (categoryEditView == null)
        //        CreateProductCategoryEditView();

        //    categoryEditViewModel.RefreshParentCategories();
        //    categoryEditViewModel.ProductCategory = new Category();

        //    (View as ProductCategoriesView).ContentViewState = ContentViewState.View_EditViewVisible;
        //}

        //private void EditCategory()
        //{
        //    if (categoryListViewModel.SelectedCategory == null) return;

        //    if (categoryEditView == null)
        //        CreateProductCategoryEditView();

        //    categoryEditViewModel.RefreshParentCategories();
        //    categoryEditViewModel.ProductCategory = categoryListViewModel.SelectedCategory;

        //    (View as ProductCategoriesView).ContentViewState = ContentViewState.View_EditViewVisible;
        //}

        ///// <summary>
        ///// Saves the category to the repository.  This method is invoked by the SaveCommand.
        ///// </summary>
        //private void SaveCategory()
        //{
        //    Category category = categoryEditViewModel.ProductCategory;

        //    if (category == null)
        //        throw new InvalidOperationException(eClinicSpaResources.ProductCategories_Exception_CannotSave);

        //    DateTime now = DateTime.Now;
        //    category.ModifiedDate = now;
        //    category.ModifiedByUID = 1;

        //    if (categoryServices.IsNewCategory(category))
        //    {
        //        category.CreatedDate = now;
        //        category.CreatedByUID = 1;

        //        categoryServices.CreateCategory(category);

        //        categoryListViewModel.ProductCategories.Add(category);
        //        categoryEditViewModel.ParentCategories.Add(category);

        //        categoryListViewModel.SelectedCategory = categoryListViewModel.ProductCategories.Single(c => c == category);
        //    }
        //    else
        //    {
        //        categoryServices.UpdateCategory(category);
        //        categoryListViewModel.RefreshCategoryList();
        //    }
        //}

        private void BackToListCategory()
        {
            (View as ProductCategoriesView).ContentViewState = ContentViewState.View_ListViewVisible;
            //categoryListViewModel.Focus();
        }

        public override void Focus()
        {
            if ((View as ProductCategoriesView).ContentViewState == ContentViewState.View_ListViewVisible)
            {
                //categoryListViewModel.Focus();
            }
            else
            {
                //categoryEditViewModel.Focus();
            }
        }

        protected override void OnDispose()
        {
            //if (categoryListViewModel != null)
            //    categoryListViewModel.Dispose();
            //if (categoryEditViewModel != null)
            //    categoryEditViewModel.Dispose();

            base.OnDispose();
        }
    }
}
