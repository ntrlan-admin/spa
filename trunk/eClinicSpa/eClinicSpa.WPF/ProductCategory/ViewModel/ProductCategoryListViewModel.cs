﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows;

using Microsoft.Practices.Unity;

using eClinicSpa.Common.Properties;
using eClinicSpa.Common.Dto.ProductCategory;
using eClinicSpa.Common.ServiceContract;

using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.Util;
using eClinicSpa.WPF.ProductCategory.Model;
using eClinicSpa.WPF.ProductCategory.View;
using eClinicSpa.WPF.Wokspace.ViewModel;
using eClinicSpa.WPF.Services;

namespace eClinicSpa.WPF.ProductCategory.ViewModel
{
    public class ProductCategoryListViewModel : WorkspaceViewModel
    {
        private readonly ServiceAdapter<IProductCategoryService> ProductCategoryServiceAdapter;

        private readonly ObservableCollection<ProductCategoryDto> selectedCategories;
        private ProductCategoryDto selectedCategory;

        private string filterText = "";

        private ICommand addNewCommand;
        private ICommand editCommand;
        private ICommand removeCommand;

        public ProductCategoryListViewModel(ProductCategoryList view)
            : base(view)
        {
            base.DisplayName = eClinicSpaResources.ProductCategories_Title;
            ProductCategoryServiceAdapter = new ServiceAdapter<IProductCategoryService>();
            Refresh();
            //removeCommand = new RelayCommand(this.RemoveCategories);
        }

        #region Properties

        public ProductCategoryModel Model { get; set; }

        public ObservableCollection<ProductCategoryDto> SelectedCategories { get { return selectedCategories; } }

        public IEnumerable<ProductCategoryDto> CategoryCollectionView { get; set; }

        public ProductCategoryDto SelectedCategory
        {
            get { return selectedCategory; }
            set
            {
                if (selectedCategory != value)
                {
                    selectedCategory = value;
                    RaisePropertyChanged("SelectedCategory");
                }
            }
        }

        public string FilterText
        {
            get { return filterText; }
            set
            {
                if (filterText != value)
                {
                    filterText = value;
                    RaisePropertyChanged("FilterText");
                }
            }
        }
        #endregion Properties

        #region Commands

        private RelayCommand RefreshCommandInstance;

        public RelayCommand RefreshCommand
        {
            get
            {
                if (RefreshCommandInstance != null) return RefreshCommandInstance;
                RefreshCommandInstance = new RelayCommand(a => Refresh());
                return RefreshCommandInstance;
            }
        }

        private void Refresh()
        {
            var result = ProductCategoryServiceAdapter.Execute(service => service.FindAllProductCategory());
            Model = new ProductCategoryModel()
            {
                NewProductCategoryOperation = new ProductCategoryDto(),
                ProductCategoryList = new ObservableCollection<ProductCategoryDto>(result.ProductCategories),
                IsEnabled = true
            };

            RaisePropertyChanged(() => Model);
            Focus();
        }

        public ICommand AddNewCommand
        {
            get { return addNewCommand; }
            set
            {
                if (addNewCommand != value)
                {
                    addNewCommand = value;
                    RaisePropertyChanged("AddNewCommand");
                }
            }
        }

        public ICommand EditCommand
        {
            get { return editCommand; }
            set
            {
                if (editCommand != value)
                {
                    editCommand = value;
                    RaisePropertyChanged("EditCommand");
                }
            }
        }

        public ICommand RemoveCommand { get { return removeCommand; } }

        #endregion
        //public bool Filter(Category category)
        //{
        //    return (category.CategoryCode == null || category.CategoryCode.IndexOf(FilterText, StringComparison.CurrentCultureIgnoreCase) >= 0)
        //        || (category.CategoryName == null || category.CategoryName.IndexOf(FilterText, StringComparison.CurrentCultureIgnoreCase) >= 0);
        //}

        //private void RemoveCategories()
        //{
        //    // Use the CategoryCollectionView, which represents the sorted/filtered state of the categories, to determine the next category to select.
        //    IEnumerable<Category> categoriesToExclude = SelectedCategories.Except(new[] { SelectedCategory });
        //    Category nextCategory = CollectionHelper.GetNextElementOrDefault(CategoryCollectionView.Except(categoriesToExclude), SelectedCategory);

        //    CategoryServices.DeleteCategories(SelectedCategories);

        //    foreach (Category category in SelectedCategories.ToArray())
        //    {
        //        ProductCategories.Remove(category);
        //    }

        //    RefreshCategoryList();
        //    SelectedCategory = nextCategory ?? CategoryCollectionView.LastOrDefault();
        //    Focus();
        //}

        public override void Focus()
        {
            //if (SelectedCategory == null)
            //    (View as ProductCategoryList).FocusFirstCell();
            //else
            //    (View as ProductCategoryList).FocusSelectedCell();
        }
    }
}
