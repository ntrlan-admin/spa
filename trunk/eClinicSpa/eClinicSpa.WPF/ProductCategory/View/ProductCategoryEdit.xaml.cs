﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.ProductCategory.ViewModel;

namespace eClinicSpa.WPF.ProductCategory.View
{
    /// <summary>
    /// Interaction logic for ProductCategoryEdit.xaml
    /// </summary>
    public partial class ProductCategoryEdit : UserControl, IView
    {
        private readonly Lazy<ProductCategoryEditViewModel> viewModel;

        public ProductCategoryEdit()
        {
            InitializeComponent();

            viewModel = new Lazy<ProductCategoryEditViewModel>(() => ViewHelper.GetViewModel<ProductCategoryEditViewModel>(this));
        }

        private ProductCategoryEditViewModel ViewModel { get { return viewModel.Value; } }


        public void FocusContent()
        {
            DispatcherHelper.DoEvents();

            categoryCodeTxt.Focus();
        }

        public void ReloadParentCategories()
        {
            //parentCategoryCmb.ItemsSource = ViewModel.ParentCategories;
        }

        private void categoryCodeTxt_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (categoryCodeTxt.IsVisible)
            {
                categoryCodeTxt.Focus();
            }
        }
    }
}
