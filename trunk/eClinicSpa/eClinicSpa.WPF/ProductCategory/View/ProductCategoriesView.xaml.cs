﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Core;

namespace eClinicSpa.WPF.ProductCategory.View
{
    /// <summary>
    /// Interaction logic for ProductCategoriesView.xaml
    /// </summary>
    public partial class ProductCategoriesView : UserControl, IView
    {
        private ContentViewState contentViewState;

        public ProductCategoriesView()
        {
            InitializeComponent();
            VisualStateManager.GoToElementState(rootContainer, ContentViewState.ToString(), false);
        }

        public ContentViewState ContentViewState
        {
            get { return contentViewState; }
            set
            {
                if (contentViewState != value)
                {
                    contentViewState = value;
                    VisualStateManager.GoToElementState(rootContainer, value.ToString(), true);
                }
            }
        }
    }
}
