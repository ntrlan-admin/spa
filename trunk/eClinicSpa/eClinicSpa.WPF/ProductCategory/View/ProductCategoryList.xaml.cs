﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.ProductCategory.ViewModel;

namespace eClinicSpa.WPF.ProductCategory.View
{
    /// <summary>
    /// Interaction logic for ProductCategoryList.xaml
    /// </summary>
    public partial class ProductCategoryList : UserControl, IView
    {
        private readonly Lazy<ProductCategoryListViewModel> viewModel;
        private ICollectionView categoryCollectionView;

        public ProductCategoryList()
        {
            InitializeComponent();

            viewModel = new Lazy<ProductCategoryListViewModel>(() => ViewHelper.GetViewModel<ProductCategoryListViewModel>(this));

            Loaded += FirstTimeLoadedHandler;
            //productCategoryList.MouseDoubleClick += (s, ev) => ViewModel.EditCommand.Execute(null);
        }

        private ProductCategoryListViewModel ViewModel { get { return viewModel.Value; } }


        public void RefreshProductCategories()
        {
            productCategoryList.Items.Refresh();
        }

        public void FocusFirstCell()
        {
            //DispatcherHelper.DoEvents();

            productCategoryList.Focus();
            //productCategoryList.SelectedItem = ViewModel.ProductCategories.FirstOrDefault();
            productCategoryList.CurrentCell = new DataGridCellInfo(productCategoryList.SelectedItem, productCategoryList.Columns[0]);
        }

        public void FocusSelectedCell()
        {
            //DispatcherHelper.DoEvents();

            productCategoryList.Focus();
            productCategoryList.CurrentCell = new DataGridCellInfo(productCategoryList.SelectedItem, productCategoryList.Columns[0]);

            //productCategoryList.UpdateLayout();
            //productCategoryList.Focus();
            //productCategoryList.ScrollIntoView(productCategoryList.SelectedItem);
            //DataGridRow dgrow = (DataGridRow)productCategoryList.ItemContainerGenerator.ContainerFromIndex(productCategoryList.SelectedIndex);
            //if (dgrow != null) dgrow.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
        }

        //private bool Filter(object obj)
        //{
        //    return ViewModel.Filter((Category)obj);
        //}

        private void FirstTimeLoadedHandler(object sender, RoutedEventArgs e)
        {
            // Ensure that this handler is called only once.
            Loaded -= FirstTimeLoadedHandler;

            //categoryCollectionView = CollectionViewSource.GetDefaultView(ViewModel.ProductCategories);
            //categoryCollectionView.Filter = Filter;
            //ViewModel.CategoryCollectionView = categoryCollectionView.Cast<Category>();

            //ViewModel.SelectedCategory = ViewModel.CategoryCollectionView.LastOrDefault();
            //ViewModel.SelectedCategory = ViewModel.ProductCategories.FirstOrDefault();
            productCategoryList.Focus();
            productCategoryList.CurrentCell = new DataGridCellInfo(productCategoryList.SelectedItem, productCategoryList.Columns[0]);
        }

        private void FilterBoxTextChanged(object sender, TextChangedEventArgs e)
        {
            // Refresh must not be called as long the DataGrid is in edit mode.
            if (productCategoryList.CommitEdit(DataGridEditingUnit.Row, true))
            {
                categoryCollectionView.Refresh();
            }
        }

        private void DataGridSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //foreach (Category category in e.RemovedItems)
            //{
            //    ViewModel.SelectedCategories.Remove(category);
            //}
            //foreach (Category category in e.AddedItems)
            //{
            //    ViewModel.SelectedCategories.Add(category);
            //}
        }

        private void HandlerRowMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ViewModel.EditCommand.Execute(null);
        }

        private void CategoryListLoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Tag = sender;
            e.Row.MouseDoubleClick += HandlerRowMouseDoubleClick;
        }

        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((sender as Control).IsVisible)
            {
                if (productCategoryList.SelectedItem == null)
                {
                    FocusFirstCell();
                }
                else
                {
                    FocusSelectedCell();
                }
            }
        }
    }
}
