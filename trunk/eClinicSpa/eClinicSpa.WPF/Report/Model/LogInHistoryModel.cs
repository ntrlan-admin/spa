﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using eClinicSpa.Common.Dto.Employee;

namespace eClinicSpa.WPF.Report.Model
{
    public class LogInHistoryModel
    {
        public bool IsEnabled { get; set; }
        
        public DateTime HistorySearchFromDate { get; set; }
        public DateTime HistorySearchToDate { get; set; }

        public bool IsAdminOnly { get; set; }
        public IList<LogInHistoryDto> LogInRecords { get; set; }
    }
}
