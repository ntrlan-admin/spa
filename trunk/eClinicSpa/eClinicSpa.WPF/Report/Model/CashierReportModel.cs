﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eClinicSpa.Common.Constant;
using eClinicSpa.Common.Dto.Employee;
using eClinicSpa.Common.Dto.Product;

namespace eClinicSpa.WPF.Report.Model
{
    public class CashierReportModel
    {
        public bool IsEnabled { get; set; }

        public ReportTimeFrame ReportTimeOption { get; set; }

        public bool ByProductOnly { get; set; }
        public bool ReportByAdmin { get; set; }

        public DateTime SelectReportDate { get; set; }
        public DateTime SelectReportMonth { get; set; }

        public IList<EmployeeDto> CashierList { get; set; }
        public EmployeeDto SelectCashier { get; set; }

        public IList<EmployeeDto> TherapistList { get; set; }
        public EmployeeDto SelectTherapist { get; set; }

        public IList<ProductDto> ProductList { get; set; }
        public ProductDto SelectProduct { get; set; }

        public EmployeeDto LoggedInUser { get; set; }
        public IList<LogInHistoryDto> LogInRecords { get; set; }
    }
}
