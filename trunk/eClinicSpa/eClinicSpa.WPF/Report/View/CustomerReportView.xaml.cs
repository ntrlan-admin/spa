﻿using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;

using Microsoft.Reporting.WinForms;

using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.Report.ViewModel;


namespace eClinicSpa.WPF.Report.View
{
    /// <summary>
    /// Interaction logic for CustomerReportView.xaml
    /// </summary>
    public partial class CustomerReportView : UserControl, IView
    {
        private readonly Lazy<CustomerReportViewModel> viewModel;

        public CustomerReportView()
        {
            InitializeComponent();
            viewModel = new Lazy<CustomerReportViewModel>(() => ViewHelper.GetViewModel<CustomerReportViewModel>(this));
        }

        private CustomerReportViewModel ViewModel { get { return viewModel.Value; } }

        private void ViewReport_Click(object sender, RoutedEventArgs e)
        {
            ShowReport();
        }

        public void ShowReport()
        {
            if (_reportViewer.LocalReport.DataSources.Count > 0)
            {
                this._reportViewer.LocalReport.DataSources.RemoveAt(0);
            }
            ReportDataSource reportDataSource = GetReportDataSource();

            this._reportViewer.LocalReport.DataSources.Add(reportDataSource);
            this._reportViewer.LocalReport.ReportEmbeddedResource = "eClinicSpa.WPF.Report.View.CustomerReport.rdlc";
            ReportParameter[] p = {
                new ReportParameter("ReportTitle", ViewModel.DisplayName.ToUpperInvariant()),
            };

            this._reportViewer.LocalReport.SetParameters(p);

            this._reportViewer.LocalReport.Refresh();
            _reportViewer.RefreshReport();
        }

        private ReportDataSource GetReportDataSource()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Id", typeof(string)));
            dt.Columns.Add(new DataColumn("FirstName", typeof(string)));
            dt.Columns.Add(new DataColumn("LastName", typeof(string)));
            dt.Columns.Add(new DataColumn("Phone", typeof(string)));
            dt.Columns.Add(new DataColumn("Gender", typeof(int)));
            dt.Columns.Add(new DataColumn("DayOfBirth", typeof(DateTime)));
            dt.Columns.Add(new DataColumn("CurrentAddress", typeof(string)));
            dt.Columns.Add(new DataColumn("DistrictName", typeof(string)));
            dt.Columns.Add(new DataColumn("Vip", typeof(bool)));
            dt.Columns.Add(new DataColumn("VipCardNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("CardNumber", typeof(string)));
            //dt.Columns.Add(new DataColumn("TreatmentConsultantId", typeof(long)));
            dt.Columns.Add(new DataColumn("TreatmentConsultantName", typeof(string)));
            //dt.Columns.Add(new DataColumn("TherapistId", typeof(long)));
            dt.Columns.Add(new DataColumn("TherapistName", typeof(string)));

            var result = ViewModel.GetCustomerReportData();
            foreach (CustomerViewDto cus in result)
            {
                DataRow dr = dt.NewRow();
                dr["Id"] = cus.Id.ToString();
                dr["FirstName"] = cus.FirstName;
                dr["LastName"] = cus.LastName;
                dr["Phone"] = cus.Phone;
                dr["Gender"] = cus.Gender;
                dr["DayOfBirth"] = cus.DayOfBirth;
                dr["CurrentAddress"] = cus.CurrentAddress;
                dr["DistrictName"] = cus.DistrictName;
                dr["Vip"] = cus.Vip;
                dr["VipCardNumber"] = cus.VipCardNumber;
                dr["CardNumber"] = cus.CardNumber;
                //dr["TreatmentConsultantId"] = cus.TreatmentConsultantName;
                dr["TreatmentConsultantName"] = cus.TreatmentConsultantName;
                //dr["TherapistId"] = i.Transaction.Customer.Therapist.Id;
                dr["TherapistName"] = cus.TherapistName;
                dt.Rows.Add(dr);
            }

            ReportDataSource reportDataSource = new ReportDataSource();
            reportDataSource.Name = "CustomerDataSet"; // Name of the DataSet we set in .rdlc
            reportDataSource.Value = dt;
            return reportDataSource;
        }
    }
}
