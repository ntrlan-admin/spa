﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using eClinicSpa.Common.Constant;
using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.Common.Dto.Product;
using eClinicSpa.Common.Dto.Transaction;
using eClinicSpa.Common.Properties;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Cashier.ViewModel;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.MainWorking.Model;
using eClinicSpa.WPF.Report.ViewModel;
using eClinicSpa.WPF.Windows;
using Microsoft.Reporting.WinForms;
using Excel = Microsoft.Office.Interop.Excel;

namespace eClinicSpa.WPF.Report.View
{
    /// <summary>
    /// Interaction logic for CashierReportView.xaml
    /// </summary>
    public partial class CashierReportView : UserControl, IView
    {
        const string DEFAULT_EXT = ".xlsx";
        const string DEFAULT_EXCEL_FILTER = "Excel (*.xlsx)|*.xlsx";

        private readonly Lazy<CashierReportViewModel> viewModel;

        public CashierReportView()
        {
            InitializeComponent();
            viewModel = new Lazy<CashierReportViewModel>(() => ViewHelper.GetViewModel<CashierReportViewModel>(this));
        }

        private CashierReportViewModel ViewModel { get { return viewModel.Value; } }

        private void ExportToExcel_Click(object sender, RoutedEventArgs e)
        {
            ShowReport();

            string DefaultFilename = string.Format("Báo cáo doanh thu - {0:dd.MM.yy}.xlsx", ViewModel.Model.SelectReportDate);

            string StatusText = eClinicSpaResources.Report_ExportingToExcel;
            ProgressDialogResult result = ProgressDialog.Execute(Application.Current.MainWindow, StatusText, () =>
            {
                this.Dispatcher.Invoke((Action)(() =>
                {
                    var dlg = new Microsoft.Win32.SaveFileDialog();
                    dlg.FileName = DefaultFilename;    // Default file name
                    dlg.DefaultExt = DEFAULT_EXT;      // Default file extension
                    dlg.Filter = DEFAULT_EXCEL_FILTER; // Filter files by extension

                    Nullable<bool> dlgResult = dlg.ShowDialog();

                    if (dlgResult == true)
                    {
                        ExportToExcel(dlg.FileName);
                    }
                }));
            }, ProgressDialogSettings.WithLabelOnly);

            if (result.OperationFailed)
                MessageBox.Show(Application.Current.MainWindow, "Export to Excel failed.", eClinicSpaResources.Report_ExportToExcel);
            else
                MessageBox.Show(Application.Current.MainWindow, eClinicSpaResources.Report_ExportToExcelSuccess, eClinicSpaResources.Report_ExportToExcel);
            
            //Thread t = new Thread(new ThreadStart(ExportToExcel_STAThread));
            // Make sure to set the apartment state BEFORE starting the thread. 
            //t.SetApartmentState(ApartmentState.STA); 
            //t.Start();
        }

        [STAThread]
        private void ExportToExcel_STAThread()
        {
            //App.Current.Dispatcher.Invoke((Action)delegate
            //{
            //});
        }

        public void ExportToExcel(string filename)
        {
            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string extension;

            byte[] bytes = _reportViewer.LocalReport.Render("EXCELOPENXML", null, out mimeType, out encoding, out extension, out streamids, out warnings);

            FileStream fs = new FileStream(filename, FileMode.Create);
            fs.Write(bytes, 0, bytes.Length);
            fs.Close();

            Excel.Application excelApp = null;
            Excel._Workbook workBook = null;
            try
            {
                excelApp = new Excel.Application();
                workBook = (Excel.Workbook)excelApp.Workbooks.Open(filename);
                Excel._Worksheet workSheet = (Excel.Worksheet)workBook.ActiveSheet;
                Excel.Range rng = workSheet.Range["F4"];
                rng.Formula = "=SUM($O$12:INDEX($O:$O,MATCH(9.99999999999999E+307,$O:$O)))+SUM($Q$12:INDEX($Q:$Q,MATCH(9.99999999999999E+307,$Q:$Q)))+SUM($R$12:INDEX($R:$R,MATCH(9.99999999999999E+307,$R:$R)))";
                string formatNumber = "#,##0_);[Red](#,##0)";
                string formatAccounting = "_(* #,##0_);_(* (#,##0);_(* \"-\"_);_(@_)";
                Excel.Range rngSummary = workSheet.Range["$F$4:$F$9"];
                Excel.Range rngPaymentType = workSheet.Range["$H$4:$H$9"];
                Excel.Range rngNumberFormat = workSheet.Range["$J:$P"];
                Excel.Range rngAccountingFormat = workSheet.Range["$Q:$R"];
                rngSummary.NumberFormat = formatNumber;
                rngPaymentType.NumberFormat = formatAccounting;
                rngNumberFormat.NumberFormat = formatNumber;
                rngAccountingFormat.NumberFormat = formatAccounting;
                workBook.Save();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (workBook != null)
                {
                    workBook.Close(false, filename, null);
                    Marshal.ReleaseComObject(workBook);
                    workBook = null;
                }
                if (excelApp != null)
                {
                    excelApp.Quit();
                    excelApp = null;
                }
            }
        }

        private void ViewReport_Click(object sender, RoutedEventArgs e)
        {
            ShowReport();
        }

        public void ShowReport()
        {
            if (_reportViewer.LocalReport.DataSources.Count > 0)
            {
                this._reportViewer.LocalReport.DataSources.RemoveAt(0);
            }
            ReportDataSource reportDataSource = GetReportDataSource();

            this._reportViewer.LocalReport.DataSources.Add(reportDataSource);
            this._reportViewer.LocalReport.ReportEmbeddedResource = "eClinicSpa.WPF.Report.View.CashierReport.rdlc";
            ReportParameter[] p = {
                new ReportParameter("ReportTitle", ViewModel.DisplayName.ToUpperInvariant()),
                new ReportParameter("ReportByDate", ViewModel.Model.ReportTimeOption == ReportTimeFrame.ByDate ? bool.TrueString : bool.FalseString),
                new ReportParameter("ReportByProduct", ViewModel.ByProductOnly.ToString()),
                new ReportParameter("ReportDate", String.Format("{0:dd/MM/yyy}", ViewModel.Model.SelectReportDate)),
                new ReportParameter("ReportMonth", String.Format("{0:MM/yyy}", ViewModel.Model.SelectReportMonth)),
                new ReportParameter("SelectCashierName", String.Concat("Thu ngân: ", ViewModel.Model.SelectCashier.Name == null ? "Tất cả" : ViewModel.Model.SelectCashier.Name)),

                new ReportParameter("AmountPaymentByCash", AmountPaymentByCash.ToString()),
                new ReportParameter("AmountPaymentByCard", AmountPaymentByCard.ToString()),
                new ReportParameter("AmountPaymentByFund", AmountPaymentByFund.ToString()),

                new ReportParameter("TotalPaidAmount", TotalPaidAmount.ToString()),
                new ReportParameter("TotalAmount", TotalAmount.ToString()),
                new ReportParameter("CustomerDiscount", CustomerDiscount.ToString()),
                new ReportParameter("CustomerDeposit", Deposit.ToString()),
                new ReportParameter("ProductDiscountAmount", ProductDiscountAmount.ToString()),
                new ReportParameter("CustomerDebit", DebitAmount.ToString())
            };

            this._reportViewer.LocalReport.SetParameters(p);

            this._reportViewer.LocalReport.Refresh();
            this._reportViewer.RefreshReport();
        }

        private Decimal TotalAmount = 0;
        private Decimal CustomerDiscount = 0; // BDH
        private Decimal Deposit = 0;
        private Decimal ProductDiscountAmount = 0;
        private Decimal TotalPaidAmount = 0;
        private Decimal DebitAmount = 0;

        private Decimal AmountPaymentByCash = 0;
        private Decimal AmountPaymentByCard = 0;
        private Decimal AmountPaymentByFund = 0;

        private ReportDataSource GetReportDataSource()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("DocDate", typeof(DateTime)));
            dt.Columns.Add(new DataColumn("CustomerId", typeof(long)));
            dt.Columns.Add(new DataColumn("CustomerName", typeof(string)));
            dt.Columns.Add(new DataColumn("CustomerDiscount", typeof(decimal)));
            dt.Columns.Add(new DataColumn("PaymentType", typeof(string)));
            dt.Columns.Add(new DataColumn("Deposit", typeof(decimal)));
            dt.Columns.Add(new DataColumn("DepositDate", typeof(string)));
            dt.Columns.Add(new DataColumn("PaidAmount", typeof(decimal)));
            dt.Columns.Add(new DataColumn("ProductId", typeof(long)));
            dt.Columns.Add(new DataColumn("ProductName", typeof(string)));
            dt.Columns.Add(new DataColumn("Quantity", typeof(int)));
            dt.Columns.Add(new DataColumn("UnitPrice", typeof(decimal)));
            dt.Columns.Add(new DataColumn("ProductDiscountRate", typeof(decimal)));
            dt.Columns.Add(new DataColumn("ProductDiscountAmount", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Amount", typeof(decimal)));
            dt.Columns.Add(new DataColumn("TotalAmount", typeof(decimal)));
            dt.Columns.Add(new DataColumn("TreatmentConsultantId", typeof(long)));
            dt.Columns.Add(new DataColumn("TreatmentConsultantName", typeof(string)));
            dt.Columns.Add(new DataColumn("TherapistId", typeof(long)));
            dt.Columns.Add(new DataColumn("TherapistName", typeof(string)));

            IList<InvoiceDto> PaidInvoices = ViewModel.GetInvoices();
            TotalAmount = 0;
            CustomerDiscount = 0; // BDH
            Deposit = 0;
            ProductDiscountAmount = 0;
            TotalPaidAmount = 0;
            DebitAmount = 0;

            if (PaidInvoices != null)
            {
                AmountPaymentByCash = 0;
                AmountPaymentByCard = 0;
                AmountPaymentByFund = 0;
                foreach (InvoiceDto i in PaidInvoices)
                {
                    TotalAmount += i.TotalAmount;
                    CustomerDiscount += i.CustomerDiscount;
                    Deposit += i.Deposit;
                    DebitAmount += i.DebitAmount;
                    TotalPaidAmount += i.PaidAmount;

                    GetAmountOfPayment(i);

                    foreach (InvoiceRowDto ir in i.InvoiceItems)
                    {
                        if (ViewModel.Model.SelectProduct.Id != 0L && ViewModel.Model.SelectProduct.Id != ir.Product.Id)
                        {
                            continue;
                        }
                        DataRow dr = dt.NewRow();
                        dr["DocDate"] = i.DocDate;
                        dr["CustomerId"] = i.Transaction.Customer.Id;
                        dr["CustomerName"] = String.Concat(i.Transaction.Customer.LastName, " ", i.Transaction.Customer.FirstName);
                        dr["CustomerDiscount"] = i.CustomerDiscount;
                        dr["PaymentType"] = PaymentType.GetPaymentTypeDesc(i.PaymentType);
                        dr["Deposit"] = i.Deposit;
                        dr["DepositDate"] = i.DepositDate == null ? "" : String.Format("{0:dd/MM/yyy HH:mm}", i.DepositDate);
                        dr["PaidAmount"] = i.PaidAmount;
                        dr["ProductId"] = ir.Product.Id;
                        dr["ProductName"] = ir.Product.Name;
                        dr["Quantity"] = ir.Quantity;
                        dr["UnitPrice"] = ir.UnitPrice;
                        dr["ProductDiscountRate"] = ir.ProductDiscountRate;
                        dr["ProductDiscountAmount"] = ir.ProductDiscountAmount;
                        ProductDiscountAmount += ir.ProductDiscountAmount;
                        dr["Amount"] = ir.Amount;
                        dr["TotalAmount"] = ir.TotalAmount;
                        dr["TreatmentConsultantId"] = i.Transaction.Customer.TreatmentConsultant.Id;
                        dr["TreatmentConsultantName"] = i.Transaction.Customer.TreatmentConsultant.Name;
                        dr["TherapistId"] = i.Transaction.Customer.Therapist.Id;
                        dr["TherapistName"] = i.Transaction.Customer.Therapist.Name;
                        dt.Rows.Add(dr);
                    }
                }
                //TotalPaidAmount = TotalAmount - CustomerDiscount - Deposit - DebitAmount;
            }

            ReportDataSource reportDataSource = new ReportDataSource();
            reportDataSource.Name = "IncomeDataSet"; // Name of the DataSet we set in .rdlc
            reportDataSource.Value = dt;
            return reportDataSource;
        }

        private void GetAmountOfPayment(InvoiceDto i)
        {
            Decimal paidAmount = i.PaidAmount;
            switch (i.PaymentType)
            {
                case PaymentType.Cash:
                    AmountPaymentByCash += paidAmount;
                    break;
                case PaymentType.Card:
                    AmountPaymentByCard += paidAmount;
                    break;
                case PaymentType.Fund:
                    AmountPaymentByFund += paidAmount;
                    break;
            }
        }
    }
}
