﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.MainWorking.Model;
using eClinicSpa.WPF.Cashier.ViewModel;
using eClinicSpa.Common.Dto.Transaction;
using eClinicSpa.Common.Dto.Product;
using eClinicSpa.Common.Constant;
using System.IO;

namespace eClinicSpa.WPF.Report.View
{
    /// <summary>
    /// Interaction logic for LogInHistoryView.xaml
    /// </summary>
    public partial class LogInHistoryView : UserControl, IView
    {
        private readonly Lazy<CashierViewModel> viewModel;

        public LogInHistoryView()
        {
            InitializeComponent();
            viewModel = new Lazy<CashierViewModel>(() => ViewHelper.GetViewModel<CashierViewModel>(this));
        }

        private CashierViewModel ViewModel { get { return viewModel.Value; } }

        void ExportToExcel()
        {
            PaymentDetailGrid.SelectAllCells();
            PaymentDetailGrid.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
            ApplicationCommands.Copy.Execute(null, PaymentDetailGrid);
            PaymentDetailGrid.UnselectAllCells();
            String result = (string)Clipboard.GetData(DataFormats.CommaSeparatedValue);
            try
            {
                StreamWriter sw = new StreamWriter("export.xls");
                sw.WriteLine(result);
                sw.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ExportToExcelBtn_Click(object sender, RoutedEventArgs e)
        {
            ExportToExcel();
        }
    }
}
