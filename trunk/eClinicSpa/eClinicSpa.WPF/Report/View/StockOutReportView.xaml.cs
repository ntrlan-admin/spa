﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

using Microsoft.Reporting.WinForms;

using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.MainWorking.Model;
using eClinicSpa.WPF.Cashier.ViewModel;
using eClinicSpa.WPF.Report.ViewModel;

using eClinicSpa.Common.Dto.Transaction;
using eClinicSpa.Common.Dto.Product;
using eClinicSpa.Common.Constant;

namespace eClinicSpa.WPF.Report.View
{
    /// <summary>
    /// Interaction logic for StockOutReportView.xaml
    /// </summary>
    public partial class StockOutReportView : UserControl, IView
    {
        private readonly Lazy<StockOutReportViewModel> viewModel;

        public StockOutReportView()
        {
            InitializeComponent();
            viewModel = new Lazy<StockOutReportViewModel>(() => ViewHelper.GetViewModel<StockOutReportViewModel>(this));
        }

        private StockOutReportViewModel ViewModel { get { return viewModel.Value; } }

        private void ViewReport_Click(object sender, RoutedEventArgs e)
        {
            ShowReport();
        }

        public void ShowReport()
        {
            if (_reportViewer.LocalReport.DataSources.Count > 0)
            {
                this._reportViewer.LocalReport.DataSources.RemoveAt(0);
            }
            ReportDataSource reportDataSource = GetReportDataSource();

            this._reportViewer.LocalReport.DataSources.Add(reportDataSource);
            this._reportViewer.LocalReport.ReportEmbeddedResource = "eClinicSpa.WPF.Report.View.StockOutReport.rdlc";
            ReportParameter[] p = {
                new ReportParameter("ReportTitle", ViewModel.DisplayName.ToUpperInvariant()),
                new ReportParameter("ReportByDate", ViewModel.Model.ReportTimeOption == ReportTimeFrame.ByDate ? bool.TrueString : bool.FalseString),
                new ReportParameter("ReportByProduct", ViewModel.ByProductOnly.ToString()),
                new ReportParameter("ReportDate", String.Format("{0:dd/MM/yyy}", ViewModel.Model.SelectReportDate)),
                new ReportParameter("ReportMonth", String.Format("{0:MM/yyy}", ViewModel.Model.SelectReportMonth)),
            };

            this._reportViewer.LocalReport.SetParameters(p);

            this._reportViewer.LocalReport.Refresh();
            _reportViewer.RefreshReport();
        }

        private ReportDataSource GetReportDataSource()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("DocDate", typeof(DateTime)));
            dt.Columns.Add(new DataColumn("CustomerId", typeof(long)));
            dt.Columns.Add(new DataColumn("CustomerName", typeof(string)));
            dt.Columns.Add(new DataColumn("ProductId", typeof(long)));
            dt.Columns.Add(new DataColumn("ProductName", typeof(string)));
            dt.Columns.Add(new DataColumn("Quantity", typeof(int)));
            dt.Columns.Add(new DataColumn("TreatmentConsultantId", typeof(long)));
            dt.Columns.Add(new DataColumn("TreatmentConsultantName", typeof(string)));
            dt.Columns.Add(new DataColumn("TherapistId", typeof(long)));
            dt.Columns.Add(new DataColumn("TherapistName", typeof(string)));

            IList<InvoiceDto> PaidInvoices = ViewModel.GetInvoices();

            if (PaidInvoices != null)
            {
                foreach (InvoiceDto i in PaidInvoices)
                {
                    foreach (InvoiceRowDto ir in i.InvoiceItems)
                    {
                        if (ViewModel.Model.SelectProduct.Id != 0L && ViewModel.Model.SelectProduct.Id != ir.Product.Id)
                        {
                            continue;
                        }
                        DataRow dr = dt.NewRow();
                        dr["DocDate"] = i.DocDate;
                        dr["CustomerId"] = i.Transaction.Customer.Id;
                        dr["CustomerName"] = i.Transaction.Customer.LastName + " " + i.Transaction.Customer.FirstName;
                        dr["ProductId"] = ir.Product.Id;
                        dr["ProductName"] = ir.Product.Name;
                        dr["Quantity"] = ir.Quantity;
                        dr["TreatmentConsultantId"] = i.Transaction.Customer.TreatmentConsultant.Id;
                        dr["TreatmentConsultantName"] = i.Transaction.Customer.TreatmentConsultant.Name;
                        dr["TherapistId"] = i.Transaction.Customer.Therapist.Id;
                        dr["TherapistName"] = i.Transaction.Customer.Therapist.Name;
                        dt.Rows.Add(dr);
                    }
                }
            }

            ReportDataSource reportDataSource = new ReportDataSource();
            reportDataSource.Name = "IncomeDataSet"; // Name of the DataSet we set in .rdlc
            reportDataSource.Value = dt;
            return reportDataSource;
        }
    }
}
