﻿using System;
using System.Collections.Generic;
using System.Linq;

using eClinicSpa.Common.Constant;
using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.Common.Dto.Employee;
using eClinicSpa.Common.Properties;
using eClinicSpa.Common.ServiceContract;
using eClinicSpa.WPF.Report.Model;
using eClinicSpa.WPF.Report.View;
using eClinicSpa.WPF.Services;
using eClinicSpa.WPF.Wokspace.ViewModel;

namespace eClinicSpa.WPF.Report.ViewModel
{
    public class CustomerReportViewModel : WorkspaceViewModel
    {
        private readonly ServiceAdapter<ICustomerService> CustomerServiceAdapter;
        private readonly ServiceAdapter<IEmployeeService> EmployeeServiceAdapter;

        public CustomerReportViewModel(CustomerReportView view)
            : base(view)
        {
            base.DisplayName = eClinicSpaResources.Report_Customer;

            CustomerServiceAdapter = new ServiceAdapter<ICustomerService>();
            EmployeeServiceAdapter = new ServiceAdapter<IEmployeeService>();

            var consultantList = EmployeeServiceAdapter.Execute(service => service.FindAllConsultant());
            var therapistList = EmployeeServiceAdapter.Execute(service => service.FindAllTherapist());

            List<EmployeeDto> initConsultantList = new List<EmployeeDto>() { new EmployeeDto() };
            initConsultantList.AddRange(consultantList.Employees);

            List<EmployeeDto> initTherapistList = new List<EmployeeDto>() { new EmployeeDto() };
            initTherapistList.AddRange(therapistList.Employees);

            Model = new CustomerReportModel
            {
                IsEnabled = true,
                ReportTimeOption = ReportTimeFrame.ByDate,
                SelectReportDate = DateTime.Today,
                VIPCustomerOnly = false,
                //SelectReportMonth = DateTime.Today,
                ConsultantList = initConsultantList,
                SelectConsultant = initConsultantList.ElementAt(0),
                TherapistList = initTherapistList,
                SelectTherapist = initTherapistList.ElementAt(0)
            };
        }

        #region Properties

        public CustomerReportModel Model { get; set; }

        public IList<CustomerViewDto> CustomerList { get; set; }

        #endregion

        #region Services

        public IList<CustomerViewDto> GetCustomerReportData()
        {
            var result = CustomerServiceAdapter.Execute(service => 
                service.GetCustomerViewByFilter(Model.VIPCustomerOnly, Model.SelectConsultant.Id, Model.SelectTherapist.Id));
            return result.Customers;
        }

        #endregion
    }
}
