﻿using System;
using System.Collections.Generic;
using System.Linq;

using eClinicSpa.Common.ServiceContract;
using eClinicSpa.Common.Properties;
using eClinicSpa.WPF.Services;
using eClinicSpa.WPF.Report.View;
using eClinicSpa.WPF.Wokspace.ViewModel;
using eClinicSpa.WPF.Report.Model;
using eClinicSpa.Common.Dto.Transaction;
using eClinicSpa.Common.Dto.Employee;
using eClinicSpa.Common.Dto.Product;
using eClinicSpa.Common.Constant;

namespace eClinicSpa.WPF.Report.ViewModel
{
    public class CashierReportViewModel : WorkspaceViewModel
    {
        protected readonly ServiceAdapter<ITransactionService> TransactionServiceAdapter;
        private readonly ServiceAdapter<IEmployeeService> EmployeeServiceAdapter;
        private readonly ServiceAdapter<IProductCategoryService> ProductCategoryServiceAdapter;

        public CashierReportViewModel(CashierReportView view)
            : base(view)
        {
            base.DisplayName = eClinicSpaResources.Report_Cashier;

            TransactionServiceAdapter = new ServiceAdapter<ITransactionService>();
            EmployeeServiceAdapter = new ServiceAdapter<IEmployeeService>();
            ProductCategoryServiceAdapter = new ServiceAdapter<IProductCategoryService>();

            var cashierList = EmployeeServiceAdapter.Execute(service => service.FindAllCashier());
            var consultantList = EmployeeServiceAdapter.Execute(service => service.FindAllConsultant());
            var therapistList = EmployeeServiceAdapter.Execute(service => service.FindAllTherapist());
            var products = ProductCategoryServiceAdapter.Execute(service => service.FindAllActiveProduct());

            List<EmployeeDto> initCashierList = new List<EmployeeDto>() { new EmployeeDto() };
            initCashierList.AddRange(cashierList.Employees);
            initCashierList.AddRange(consultantList.Employees);

            List<EmployeeDto> initTherapistList = new List<EmployeeDto>() { new EmployeeDto() };
            initTherapistList.AddRange(therapistList.Employees);

            List<ProductDto> initProductList = new List<ProductDto>() { new ProductDto() };
            initProductList.AddRange(products.Products);

            Model = new CashierReportModel
            {
                IsEnabled = true,
                ReportTimeOption = ReportTimeFrame.ByDate,
                SelectReportDate = DateTime.Today,
                SelectReportMonth = DateTime.Today,
                CashierList = initCashierList,
                SelectCashier = initCashierList.ElementAt(0),
                TherapistList = initTherapistList,
                SelectTherapist = initTherapistList.ElementAt(0),
                ProductList = initProductList,
                SelectProduct = initProductList.ElementAt(0),
                ReportByAdmin = false,
                ByProductOnly = false
            };
        }

        #region Properties

        public CashierReportModel Model { get; set; }

        public bool ByProductOnly
        {
            get { return Model.ByProductOnly; }
            set
            {
                if (Model.ByProductOnly != value)
                {
                    Model.ByProductOnly = value;
                    RaisePropertyChanged("ByProductOnly");
                }
            }
        }

        public bool ReportByAdmin
        {
            get { return Model.ReportByAdmin; }
            set
            {
                if (Model.ReportByAdmin != value)
                {
                    Model.ReportByAdmin = value;
                    RaisePropertyChanged("ReportByAdmin");
                }
            }
        }

        #endregion

        #region Services

        public void SetReportByAdmin()
        {
            if (Model.LoggedInUser.EmployeeType == EmployeeRole.Manager ||
                Model.LoggedInUser.EmployeeType == EmployeeRole.TreatmentConsultantAndTherapist)
            {
                ReportByAdmin = true;
            }
            else
            {
                ReportByAdmin = false;
                Model.SelectCashier = Model.LoggedInUser;
            }
        }

        public IList<InvoiceDto> GetInvoices()
        {
            if (Model.ReportTimeOption == ReportTimeFrame.ByDate)
            {
                var result = TransactionServiceAdapter.Execute(service => service.FindInvoiceByFilter(Model.SelectReportDate, PaymentStatus.Paid, Model.SelectCashier.Id, Model.SelectTherapist.Id));
                return result.Invoices;
            }
            else
            {
                var result = TransactionServiceAdapter.Execute(service => service.FindInvoiceByMonthFilter(Model.SelectReportMonth, PaymentStatus.Paid, Model.SelectCashier.Id, Model.SelectTherapist.Id));
                return result.Invoices;
            }
        }

        #endregion
    }
}
