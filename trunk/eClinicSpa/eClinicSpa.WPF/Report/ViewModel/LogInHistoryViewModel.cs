﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.Common.ServiceContract;
using eClinicSpa.Common.Properties;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.MainWindow.View;
using eClinicSpa.WPF.MainWindow.Model;
using eClinicSpa.WPF.Services;
using eClinicSpa.WPF.Util;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Report.View;
using eClinicSpa.WPF.MainWorking.View;
using eClinicSpa.WPF.Wokspace.ViewModel;
using eClinicSpa.WPF.Report.Model;
using eClinicSpa.Common.Dto.Transaction;
using eClinicSpa.Common.Dto.Employee;
using eClinicSpa.Common.Dto.Product;
using eClinicSpa.Common.Constant;

namespace eClinicSpa.WPF.Report.ViewModel
{
    public class LogInHistoryViewModel : WorkspaceViewModel
    {
        protected readonly ServiceAdapter<IEmployeeService> EmployeeServiceAdapter;

        public LogInHistoryViewModel(LogInHistoryView view)
            : base(view)
        {
            base.DisplayName = eClinicSpaResources.LogInHistory_Title;

            EmployeeServiceAdapter = new ServiceAdapter<IEmployeeService>();

            Model = new LogInHistoryModel
            {
                IsEnabled = true,
                HistorySearchFromDate = DateTime.Today.AddDays(-7),
                HistorySearchToDate = DateTime.Today,
                IsAdminOnly = false
            };
        }

        #region Properties

        public LogInHistoryModel Model { get; set; }

        public bool IsAdminOnly
        {
            get { return Model.IsAdminOnly; }
            set
            {
                if (Model.IsAdminOnly != value)
                {
                    Model.IsAdminOnly = value;
                    RaisePropertyChanged("IsAdminOnly");
                    RaisePropertyChanged("LogInRecords");
                }
            }
        }

        public IList<LogInHistoryDto> LogInRecords 
        { 
            get
            {
                if (IsAdminOnly && Model.LogInRecords != null)
                {
                    return Model.LogInRecords.Where(lr => lr.LogInUser.EmployeeType == EmployeeRole.Manager).ToList();
                }
                else
                {
                    return Model.LogInRecords;
                }
            }
        }

        #endregion

        #region Commands

        private RelayCommand SearchHistoryCommandInstance;
        public RelayCommand SearchHistoryCommand
        {
            get
            {
                if (SearchHistoryCommandInstance != null) return SearchHistoryCommandInstance;
                SearchHistoryCommandInstance = new RelayCommand(a => SearchHistory());
                return SearchHistoryCommandInstance;
            }
        }

        private void SearchHistory()
        {
            Model.IsEnabled = false;
            RaisePropertyChanged(() => Model);

            var logInRecords = EmployeeServiceAdapter.Execute(service => service.FindLogInHistory(Model.HistorySearchFromDate, Model.HistorySearchToDate));
            Model.LogInRecords = logInRecords.LogInHistories;

            Model.IsEnabled = true;
            RaisePropertyChanged("LogInRecords");
            RaisePropertyChanged(() => Model);
        }

        #endregion
    }
}
