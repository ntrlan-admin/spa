﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.Common.ServiceContract;
using eClinicSpa.Common.Properties;
using eClinicSpa.WPF.Core;
using eClinicSpa.WPF.MainWindow.View;
using eClinicSpa.WPF.MainWindow.Model;
using eClinicSpa.WPF.Services;
using eClinicSpa.WPF.Util;
using eClinicSpa.WPF.Applications;
using eClinicSpa.WPF.Report.View;
using eClinicSpa.WPF.MainWorking.View;
using eClinicSpa.WPF.Wokspace.ViewModel;
using eClinicSpa.WPF.Report.Model;
using eClinicSpa.Common.Dto.Transaction;
using eClinicSpa.Common.Dto.Employee;
using eClinicSpa.Common.Dto.Product;
using eClinicSpa.Common.Constant;

namespace eClinicSpa.WPF.Report.ViewModel
{
    public class StockOutReportViewModel : WorkspaceViewModel
    {
        protected readonly ServiceAdapter<ITransactionService> TransactionServiceAdapter;
        private readonly ServiceAdapter<IEmployeeService> EmployeeServiceAdapter;
        private readonly ServiceAdapter<IProductCategoryService> ProductCategoryServiceAdapter;

        public StockOutReportViewModel(StockOutReportView view)
            : base(view)
        {
            base.DisplayName = eClinicSpaResources.Report_StockOut;

            TransactionServiceAdapter = new ServiceAdapter<ITransactionService>();
            EmployeeServiceAdapter = new ServiceAdapter<IEmployeeService>();
            ProductCategoryServiceAdapter = new ServiceAdapter<IProductCategoryService>();

            var therapistList = EmployeeServiceAdapter.Execute(service => service.FindAllTherapist());
            var products = ProductCategoryServiceAdapter.Execute(service => service.FindAllActiveProduct());

            List<EmployeeDto> initTherapistList = new List<EmployeeDto>() { new EmployeeDto() };
            initTherapistList.AddRange(therapistList.Employees);

            List<ProductDto> initProductList = new List<ProductDto>() { new ProductDto() };
            initProductList.AddRange(products.Products);

            Model = new CashierReportModel
            {
                IsEnabled = true,
                ReportTimeOption = ReportTimeFrame.ByDate,
                SelectReportDate = DateTime.Today,
                SelectReportMonth = DateTime.Today,
                TherapistList = initTherapistList,
                SelectTherapist = initTherapistList.ElementAt(0),
                ProductList = initProductList,
                SelectProduct = initProductList.ElementAt(0),
                ByProductOnly = false
            };
        }

        #region Properties

        public CashierReportModel Model { get; set; }

        public bool ByProductOnly
        {
            get { return Model.ByProductOnly; }
            set
            {
                if (Model.ByProductOnly != value)
                {
                    Model.ByProductOnly = value;
                    RaisePropertyChanged("ByProductOnly");
                }
            }
        }

        #endregion

        #region Services

        public IList<InvoiceDto> GetInvoices()
        {
            if (Model.ReportTimeOption == ReportTimeFrame.ByDate)
            {
                var result = TransactionServiceAdapter.Execute(service => service.FindInvoiceByFilter(Model.SelectReportDate, PaymentStatus.Paid, 0L, Model.SelectTherapist.Id));
                return result.Invoices;
            }
            else
            {
                var result = TransactionServiceAdapter.Execute(service => service.FindInvoiceByMonthFilter(Model.SelectReportMonth, PaymentStatus.Paid, 0L, Model.SelectTherapist.Id));
                return result.Invoices;
            }
        }

        #endregion
    }
}
