﻿using System.Reflection;

[assembly: AssemblyCompanyAttribute("AcClear - Acne & Skin Care Clinic")]
[assembly: AssemblyProductAttribute("AcClear - Acne & Skin Care Clinic")]
[assembly: AssemblyCopyrightAttribute("Bản quyền 2014 AcClear - Acne & Skin Care Clinic")]
[assembly: AssemblyTrademarkAttribute("")]
[assembly: AssemblyVersionAttribute("1.0")]
[assembly: AssemblyFileVersionAttribute("1.0")]