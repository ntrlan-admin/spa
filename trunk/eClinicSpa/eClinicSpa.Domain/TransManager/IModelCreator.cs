using System.Data.Entity;

namespace eClinicSpa.Domain.TransManager
{
    public interface IModelCreator
    {
        void OnModelCreating(DbModelBuilder modelBuilder);
    }
}