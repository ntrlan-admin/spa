﻿using System.Collections.Generic;
using AutoMapper;
using eClinicSpa.Common.Dto.Employee;
using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.Common.Dto.Transaction;
using eClinicSpa.Common.Dto.Address;
using eClinicSpa.Common.Dto.Product;
using eClinicSpa.Common.Dto.ProductCategory;

namespace eClinicSpa.Domain.Entities
{
    /// <summary>
    /// version 0.13 Chapter XIII: Business Domain Extension
    /// </summary>
    public class AutoMapperConfigurator
    {
        public static void Install()
        {
            Mapper.CreateMap<Employee, EmployeeDto>();

            Mapper.CreateMap<ConsultantStatusView, ConsultantStatusViewDto>();
            Mapper.CreateMap<TherapistStatusView, TherapistStatusViewDto>();
            Mapper.CreateMap<CustomerView, CustomerViewDto>();
            Mapper.CreateMap<CustomerTransactionView, CustomerTransactionViewDto>();

            Mapper.CreateMap<LogInHistory, LogInHistoryDto>()
                .ForMember(d => d.LogInUser,
                           opt => opt.MapFrom(src => src.LogInUser));
            
            Mapper.CreateMap<Customer, CustomerDto>()
                .ForMember(d => d.ReferralCustomer,
                           opt => opt.MapFrom(src => src.ReferralCustomer))
                .ForMember(d => d.TreatmentConsultant,
                           opt => opt.MapFrom(src => src.TreatmentConsultant))
                .ForMember(d => d.Therapist,
                           opt => opt.MapFrom(src => src.Therapist))
                .ForMember(d => d.Addresses, 
                           opt => opt.MapFrom(src => Mapper.Map<ICollection<Address>, List<AddressDto>>(src.Addresses())));

            Mapper.CreateMap<Transaction, TransactionDto>()
                .ForMember(d => d.Customer,
                           opt => opt.MapFrom(src => src.Customer))
                .ForMember(d => d.InConsultant,
                           opt => opt.MapFrom(src => src.InConsultant))
                .ForMember(d => d.OutConsultant,
                           opt => opt.MapFrom(src => src.OutConsultant))
                .ForMember(d => d.AlternativeTherapist,
                           opt => opt.MapFrom(src => src.AlternativeTherapist))
                .ForMember(d => d.Creator,
                           opt => opt.MapFrom(src => src.Creator));

            Mapper.CreateMap<Product, ProductDto>()
                .ForMember(d => d.Category,
                           opt => opt.MapFrom(src => src.Category))
                .ForMember(d => d.CreatedBy,
                           opt => opt.MapFrom(src => src.CreatedBy))
                .ForMember(d => d.ModifiedBy,
                           opt => opt.MapFrom(src => src.ModifiedBy));

            Mapper.CreateMap<Invoice, InvoiceDto>()
                .ForMember(d => d.Transaction,
                           opt => opt.MapFrom(src => src.Transaction))
                .ForMember(d => d.Creator,
                           opt => opt.MapFrom(src => src.Creator))
                .ForMember(d => d.CollectedBy,
                           opt => opt.MapFrom(src => src.CollectedBy))
                .ForMember(d => d.InvoiceItems,
                           opt => opt.MapFrom(src => Mapper.Map<ICollection<InvoiceRow>, List<InvoiceRowDto>>(src.InvoiceItems())));

            Mapper.CreateMap<InvoiceRow, InvoiceRowDto>()
                .ForMember(d => d.InvoiceId,
                           opt => opt.MapFrom(src => src.Invoice.Id));

            Mapper.CreateMap<Address, AddressDto>()
                .ForMember(d => d.CustomerId,
                           opt => opt.MapFrom(src => src.Customer.Id));

            Mapper.CreateMap<ProductCategory, ProductCategoryDto>();
                //.ForMember(d => d.SuperId,
                //           opt => opt.MapFrom(src => src.SuperCategory.Id));
        }
    }
}
