﻿using System.Linq;
using System.Collections.Generic;

using Newtonsoft.Json;

using eClinicSpa.Common.Dto.Product;
using eClinicSpa.Common.Dto.ProductCategory;
using eClinicSpa.Common.Message;
using eClinicSpa.Domain.Repository;
using System.Collections.ObjectModel;
using System.Data.Entity.ModelConfiguration;
using System;

namespace eClinicSpa.Domain.Entities
{
    /// <summary>
    /// version 0.13 Chapter XIII: Business Domain Extension
    /// version 0.14 Chapter XIV:  Validation
    /// </summary>
    [JsonObject(IsReference = true)]    
    public class Product
        :EntityBase
    {
        protected Product()
        {
            Status = 1;
            CreatedOn = ModifiedOn = DateTime.Now;
        }

        public virtual ProductCategory Category { get; private set; }
        public virtual string Name { get; private set; }

        public virtual Decimal Price { get; private set; }
        public virtual int Status { get; private set; }

        public virtual DateTime? CreatedOn { get; private set; }
        public virtual Employee CreatedBy { get; private set; }
        public virtual DateTime? ModifiedOn { get; private set; }
        public virtual Employee ModifiedBy { get; private set; }


        public static Product Create(IRepositoryLocator locator, ProductDto operation)
        {
            ValidateOperation(locator,operation);
            CheckForDuplicates(locator, operation);
            var instance = new Product
            {
                Category = operation.Category == null ? null : locator.GetById<ProductCategory>(operation.Category.Id),
                Name = operation.Name,
                Price = operation.Price,
                Status = operation.Status,
                CreatedOn = operation.CreatedOn,
                CreatedBy = operation.CreatedBy == null ? null : locator.GetById<Employee>(operation.CreatedBy.Id),
                ModifiedOn = operation.ModifiedOn,
                ModifiedBy = operation.ModifiedBy == null ? null : locator.GetById<Employee>(operation.ModifiedBy.Id)
            };

            locator.Save(instance);
            return instance;
        }

        public virtual void Update(IRepositoryLocator locator, ProductDto operation)
        {
            ValidateOperation(locator, operation);
            CheckForDuplicates(locator, operation);
            Category = operation.Category == null ? null : locator.GetById<ProductCategory>(operation.Category.Id);
            Name = operation.Name;
            Price = operation.Price;
            Status = operation.Status;
            CreatedOn = operation.CreatedOn;
            CreatedBy = operation.CreatedBy == null ? null : locator.GetById<Employee>(operation.CreatedBy.Id);
            ModifiedOn = operation.ModifiedOn;
            ModifiedBy = operation.ModifiedBy == null ? null : locator.GetById<Employee>(operation.ModifiedBy.Id);
            locator.Update(this);
        }


        private static void CheckForDuplicates(IRepositoryLocator locator, ProductDto op)
        {
            var result = locator.FindAll<Product>().Where(p => p.Category.Id == op.Category.Id &&
                                                               p.Name == op.Name &&
                                                               p.Id != op.Id);

            if (!result.Any()) return;
            throw new BusinessException(BusinessExceptionEnum.Validation,
                                        "A product instance for this product category already exist with the same details.");
        }
    }
}
