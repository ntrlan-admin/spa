﻿using System.Linq;
using System.Collections.Generic;

using Newtonsoft.Json;

using eClinicSpa.Common.Dto.Transaction;
using eClinicSpa.Common.Message;
using eClinicSpa.Domain.Repository;
using System.Collections.ObjectModel;
using System.Data.Entity.ModelConfiguration;
using System;

namespace eClinicSpa.Domain.Entities
{
    /// <summary>
    /// version 0.13 Chapter XIII: Business Domain Extension
    /// version 0.14 Chapter XIV:  Validation
    /// </summary>
    [JsonObject(IsReference = true)]    
    public class InvoiceRow
        :EntityBase
    {
        protected InvoiceRow()
        {
        }

        public virtual Invoice Invoice { get; private set; }
        public virtual int ItemNo { get; private set; }
        public virtual Product Product { get; private set; }
        public virtual int Quantity { get; private set; }
        public virtual Decimal UnitPrice { get; private set; }
        public virtual Decimal Amount { get; private set; }
        public virtual Decimal ProductDiscountRate { get; private set; }
        public virtual Decimal ProductDiscountAmount { get; private set; }
        public virtual bool NotPaid { get; private set; }


        public static InvoiceRow Create(IRepositoryLocator locator, InvoiceRowDto operation)
        {
            ValidateOperation(locator,operation);
            //CheckForDuplicates(locator, operation);
            var instance = new InvoiceRow
            {
                Invoice = locator.GetById<Invoice>(operation.InvoiceId),
                ItemNo = operation.ItemNo,
                Product = operation.Product == null ? null : locator.GetById<Product>(operation.Product.Id),
                Quantity = operation.Quantity,
                UnitPrice = operation.UnitPrice,
                Amount = operation.Amount,
                ProductDiscountRate = operation.ProductDiscountRate,
                ProductDiscountAmount = operation.ProductDiscountAmount,
                NotPaid = operation.NotPaid
            };

            instance = locator.Save(instance);
            locator.FlushModifications();
            return instance;
        }

        public virtual void Update(IRepositoryLocator locator, InvoiceRowDto operation)
        {
            ValidateOperation(locator, operation);
            //CheckForDuplicates(locator, operation);
            Invoice = locator.GetById<Invoice>(operation.InvoiceId);
            ItemNo = operation.ItemNo;
            Product = operation.Product == null ? null : locator.GetById<Product>(operation.Product.Id);
            Quantity = operation.Quantity;
            UnitPrice = operation.UnitPrice;
            Amount = operation.Amount;
            ProductDiscountRate = operation.ProductDiscountRate;
            ProductDiscountAmount = operation.ProductDiscountAmount;
            NotPaid = operation.NotPaid;
            locator.Update(this);
        }


        private static void CheckForDuplicates(IRepositoryLocator locator, InvoiceRowDto op)
        {
            var result = locator.FindAll<InvoiceRow>().Where(ir => ir.Invoice.Id == op.InvoiceId &&
                                                                   ir.Product.Id == op.Product.Id &&
                                                                   ir.ProductDiscountRate == op.ProductDiscountRate &&
                                                                   ir.Quantity == op.Quantity &&
                                                                   /* p.ItemNo == op.ItemNo && */
                                                                   ir.Id != op.Id);

            if (!result.Any()) return;
            throw new BusinessException(BusinessExceptionEnum.Validation,
                                        "An invoice row instance for this invoice row already exist with the same details.");
        }
    }
}
