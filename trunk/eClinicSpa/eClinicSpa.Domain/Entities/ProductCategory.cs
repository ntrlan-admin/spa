﻿using System.Linq;
using System.Collections.Generic;

using Newtonsoft.Json;

using eClinicSpa.Common.Dto.ProductCategory;
using eClinicSpa.Common.Message;
using eClinicSpa.Domain.Repository;
using System.Collections.ObjectModel;
using System.Data.Entity.ModelConfiguration;

namespace eClinicSpa.Domain.Entities
{
    /// <summary>
    /// version 0.13 Chapter XIII: Business Domain Extension
    /// version 0.14 Chapter XIV:  Validation
    /// </summary>
    [JsonObject(IsReference = true)]    
    public class ProductCategory
        :EntityBase
    {
        protected ProductCategory()
        {
            //ProductCategorySet = new HashSet<ProductCategory>();
        }

        //public virtual ProductCategory SuperCategory { get; private set; }
        public virtual string Code { get; private set; }
        public virtual string Name { get; private set; }

        //protected virtual ICollection<ProductCategory> ProductCategorySet { get; set; }

        public static ProductCategory Create(IRepositoryLocator locator, ProductCategoryDto operation)
        {
            ValidateOperation(locator,operation);
            CheckForDuplicates(locator, operation);
            //var superCategory = locator.GetById<ProductCategory>(operation.SuperId);
            var instance = new ProductCategory
            {
                //SuperCategory = superCategory,
                Code = operation.Code,
                Name = operation.Name
            };

            locator.Save(instance);
            return instance;
        }

        public virtual void Update(IRepositoryLocator locator, ProductCategoryDto operation)
        {
            ValidateOperation(locator, operation);
            CheckForDuplicates(locator, operation);
            Code = operation.Code;
            Name = operation.Name;
            locator.Update(this);
        }


        private static void CheckForDuplicates(IRepositoryLocator locator, ProductCategoryDto op)
        {
            var result = locator.FindAll<ProductCategory>().Where(a => /*a.SuperCategory.Id == op.SuperId &&*/
                                                                a.Code == op.Code &&
                                                                a.Name == op.Name &&
                                                                a.Id != op.Id);

            if (!result.Any()) return;
            throw new BusinessException(BusinessExceptionEnum.Validation,
                                        "A product category instance for this product category already exist with the same details.");
        }

        //public virtual ReadOnlyCollection<ProductCategory> SubProductCategories()
        //{
        //    if (ProductCategorySet == null) return null;
        //    return new ReadOnlyCollection<ProductCategory>(ProductCategorySet.ToArray());
        //}

        //public virtual ProductCategory AddProductCategory(IRepositoryLocator locator, ProductCategoryDto operation)
        //{
        //    AddProductCategoryValidate(locator, operation);
        //    var address = ProductCategory.Create(locator, operation);
        //    ProductCategorySet.Add(address);
        //    return address;
        //}

        //private void AddProductCategoryValidate(IRepositoryLocator locator, ProductCategoryDto operation)
        //{
        //    if (operation.SuperId.Equals(Id)) return;
        //    throw new BusinessException(BusinessExceptionEnum.Validation, "Incorrect SuperId was passed with the product category details");
        //}

        //public virtual void DeleteProductCategory(IRepositoryLocator locator, long productCategoryId)
        //{
        //    DeleteProductCategoryValidate(locator, productCategoryId);
        //    var productCategory = ProductCategorySet.Single(a => a.Id.Equals(productCategoryId));
        //    ProductCategorySet.Remove(productCategory);
        //    locator.Remove(productCategory);
        //}

        //private void DeleteProductCategoryValidate(IRepositoryLocator locator, long productCategoryId)
        //{
        //    return;
        //}

        //public class Mapping : EntityTypeConfiguration<ProductCategory>
        //{
        //    public Mapping()
        //    {
        //        HasMany(c => c.ProductCategorySet).WithRequired(a => a.SuperCategory);
        //    }
        //}
    }
}
