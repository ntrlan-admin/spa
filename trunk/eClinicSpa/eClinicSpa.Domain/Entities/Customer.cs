﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using eClinicSpa.Common.Dto.Address;
using eClinicSpa.Common.Message;
using eClinicSpa.Domain.Repository;
using eClinicSpa.Common.Dto.Customer;
using System.Collections.ObjectModel;
//using Iesi.Collections.Generic;


namespace eClinicSpa.Domain.Entities
{
    /// <remarks>
    /// version 0.03 Chapter III:  The Response
    /// version 0.13 Chapter XIII: Business Domain Extension
    /// </remarks>
    [JsonObject(IsReference = true)]
    public class Customer
        :EntityBase
    {
        protected Customer()
        {
            AddressSet = new HashSet<Address>();
        }

        public virtual string FirstName { get; private set; }
        public virtual string LastName { get; private set; }

        public virtual int Gender { get; private set; }
        public virtual DateTime DayOfBirth { get; private set; }
        public virtual string CurrentAddress { get; private set; }
        public virtual int DistrictNo { get; private set; }
        public virtual string Phone { get; private set; }
        public virtual string Email { get; private set; }
        public virtual bool Vip { get; private set; }
        public virtual bool FellowPartner { get; private set; }
        public virtual bool NotUseCream { get; private set; }
        public virtual string UrgentNote { get; private set; }
        public virtual string Note { get; private set; }
        public virtual String MedicalRecord { get; private set; }
        public virtual string CardNumber { get; private set; }
        public virtual string VipCardNumber { get; private set; }
        public virtual string PictureNoBefore { get; private set; }
        public virtual string PictureNoAfter { get; private set; }
        public virtual bool KnownByAdvertise { get; private set; }
        public virtual bool KnownByWeb { get; private set; }
        public virtual Customer ReferralCustomer { get; private set; }
        public virtual string KnownByOther { get; private set; }
        public virtual Employee TreatmentConsultant { get; private set; }
        public virtual Employee Therapist { get; private set; }

        public virtual DateTime? CreatedOn { get; private set; }
        public virtual DateTime? ModifiedOn { get; private set; }

        protected virtual ICollection<Address> AddressSet { get; set; }

        public static Customer Create(IRepositoryLocator locator, CustomerDto operation)
        {
            ValidateOperation(locator, operation);
            CheckForDuplicates(locator, operation);
            Customer customer = null;
            if (operation.ReferralCustomer != null)
            {
                customer = locator.GetById<Customer>(operation.ReferralCustomer.Id);
            }
            Employee consultant = null;
            if (operation.TreatmentConsultant != null)
            {
                consultant = locator.GetById<Employee>(operation.TreatmentConsultant.Id);
            }
            Employee therapist = null;
            if (operation.Therapist != null)
            {
                therapist = locator.GetById<Employee>(operation.Therapist.Id);
            }
            var instance = new Customer
                {
                    FirstName = operation.FirstName,
                    LastName = operation.LastName,
                    Gender = operation.Gender,
                    DayOfBirth = operation.DayOfBirth,
                    CurrentAddress = operation.CurrentAddress,
                    DistrictNo = operation.DistrictNo,
                    Phone = operation.Phone,
                    Email = operation.Email,
                    Vip = operation.Vip,
                    FellowPartner = operation.FellowPartner,
                    NotUseCream = operation.NotUseCream,
                    UrgentNote = String.IsNullOrWhiteSpace(operation.UrgentNote) ? null : operation.UrgentNote,
                    Note = String.IsNullOrWhiteSpace(operation.Note) ? null : operation.Note,
                    MedicalRecord = String.IsNullOrWhiteSpace(operation.MedicalRecord) ? null : operation.MedicalRecord,
                    CardNumber = operation.CardNumber,
                    VipCardNumber = operation.VipCardNumber,
                    PictureNoBefore = operation.PictureNoBefore,
                    PictureNoAfter = operation.PictureNoAfter,
                    KnownByAdvertise = operation.KnownByAdvertise,
                    ReferralCustomer = customer,
                    KnownByWeb = operation.KnownByWeb,
                    KnownByOther = operation.KnownByOther,
                    TreatmentConsultant = consultant,
                    Therapist = therapist,
                    CreatedOn = operation.CreatedOn,
                    ModifiedOn = operation.ModifiedOn
                };

            instance = locator.Save(instance);
            locator.FlushModifications();
            return instance;
        }

        private static void CheckForDuplicates(IRepositoryLocator locator, CustomerDto op)
        {
            var result = locator.FindAll<Customer>().Where(c => c.FirstName == op.FirstName &&
                                                                c.LastName == op.LastName &&
                                                                c.Phone == op.Phone &&
                                                                c.Id != op.Id).ToArray();

            if (!result.Any()) return;
            throw new BusinessException(BusinessExceptionEnum.Validation, 
                                        "A customer instance already exist with these first and last names.");
        }

        public virtual void Update(IRepositoryLocator locator, CustomerDto operation)
        {
            ValidateOperation(locator, operation);
            //CheckForDuplicates(locator, operation);
            FirstName = operation.FirstName;
            LastName = operation.LastName;
            Gender = operation.Gender;
            DayOfBirth = operation.DayOfBirth;
            CurrentAddress = String.IsNullOrWhiteSpace(operation.CurrentAddress) ? null : operation.CurrentAddress;
            DistrictNo = operation.DistrictNo;
            Phone = operation.Phone;
            Email = operation.Email;
            Vip = operation.Vip;
            FellowPartner = operation.FellowPartner;
            NotUseCream = operation.NotUseCream;
            UrgentNote = String.IsNullOrWhiteSpace(operation.UrgentNote) ? null : operation.UrgentNote;
            Note = String.IsNullOrWhiteSpace(operation.Note) ? null : operation.Note;
            MedicalRecord = String.IsNullOrWhiteSpace(operation.MedicalRecord) ? null : operation.MedicalRecord;
            CardNumber = operation.CardNumber;
            VipCardNumber = operation.VipCardNumber;
            PictureNoBefore = operation.PictureNoBefore;
            PictureNoAfter = operation.PictureNoAfter;
            KnownByAdvertise = operation.KnownByAdvertise;
            ReferralCustomer = null;
            if (operation.ReferralCustomer != null)
            {
                ReferralCustomer = locator.GetById<Customer>(operation.ReferralCustomer.Id);
            }
            KnownByWeb = operation.KnownByWeb;
            KnownByOther = String.IsNullOrWhiteSpace(operation.KnownByOther) ? null : operation.KnownByOther;
            TreatmentConsultant = null;
            if (operation.TreatmentConsultant != null)
            {
                TreatmentConsultant = locator.GetById<Employee>(operation.TreatmentConsultant.Id);
            }
            Therapist = null;
            if (operation.Therapist != null)
            {
                Therapist = locator.GetById<Employee>(operation.Therapist.Id);
            }
            CreatedOn = operation.CreatedOn;
            ModifiedOn = operation.ModifiedOn;
            locator.Update(this);
            locator.FlushModifications();
        }
        
        public virtual ReadOnlyCollection<Address> Addresses()
        {
            if (AddressSet == null) return null;
            return new ReadOnlyCollection<Address>(AddressSet.ToArray());
        }

        public virtual Address AddAddress(IRepositoryLocator locator, AddressDto operation)
        {
            AddAddressValidate(locator, operation);
            var address = Address.Create(locator, operation);
            AddressSet.Add(address);
            return address;
        }

        private void AddAddressValidate(IRepositoryLocator locator, AddressDto operation)
        {            
            if (operation.CustomerId.Equals(Id)) return;
            throw new BusinessException(BusinessExceptionEnum.Validation, "Incorrect Customer Id was passed with the address details");
        }

        public virtual void DeleteAddress(IRepositoryLocator locator, long addressId)
        {
            DeleteAddressValidate(locator, addressId);
            var address = AddressSet.Single(a => a.Id.Equals(addressId));
            AddressSet.Remove(address);
            locator.Remove(address);
        }

        private void DeleteAddressValidate(IRepositoryLocator locator, long addressId)
        {
            return;
        }

        public class Mapping : EntityTypeConfiguration<Customer>
        {
            public Mapping()
            {
                HasMany(c => c.AddressSet).WithRequired(a => a.Customer);
            }
        }
    }
}
