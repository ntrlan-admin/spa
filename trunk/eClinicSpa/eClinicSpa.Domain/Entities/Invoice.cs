﻿using System.Linq;
using System.Collections.Generic;

using Newtonsoft.Json;

using eClinicSpa.Common.Dto.Transaction;
using eClinicSpa.Common.Message;
using eClinicSpa.Domain.Repository;
using System.Collections.ObjectModel;
using System.Data.Entity.ModelConfiguration;
using System;
using eClinicSpa.Common.Constant;

namespace eClinicSpa.Domain.Entities
{
    /// <summary>
    /// version 0.13 Chapter XIII: Business Domain Extension
    /// version 0.14 Chapter XIV:  Validation
    /// </summary>
    [JsonObject(IsReference = true)]    
    public class Invoice
        :EntityBase
    {
        protected Invoice()
        {
            DocType = InvoiceConstant.BILLING;
            Status = 1;
        }

        public virtual Transaction Transaction { get; private set; }
        public virtual int DocType { get; private set; }
        public virtual DateTime DocDate { get; private set; }
        public virtual int PaymentType { get; private set; }

        public virtual Decimal CustomerDiscount { get; private set; }

        public virtual Decimal Deposit { get; private set; }
        public virtual DateTime? DepositDate { get; private set; }
        public virtual Decimal PaidAmount { get; private set; }

        public virtual int Status { get; private set; }

        public virtual Employee Creator { get; private set; }
        public virtual Employee CollectedBy { get; private set; }

        protected virtual ICollection<InvoiceRow> InvoiceItemSet { get; set; }

        public static Invoice Create(IRepositoryLocator locator, InvoiceDto operation)
        {
            ValidateOperation(locator,operation);
            CheckForDuplicates(locator, operation);
            var instance = new Invoice
            {
                Transaction = operation.Transaction == null ? null : locator.GetById<Transaction>(operation.Transaction.Id),
                DocType = operation.DocType,
                DocDate = operation.DocDate,
                PaymentType = operation.PaymentType,
                CustomerDiscount = operation.CustomerDiscount,
                Deposit = operation.Deposit,
                DepositDate = operation.DepositDate,
                PaidAmount = operation.PaidAmount,
                Status = operation.Status,
                Creator = operation.Creator == null ? null : locator.GetById<Employee>(operation.Creator.Id),
                CollectedBy = operation.CollectedBy == null ? null : locator.GetById<Employee>(operation.CollectedBy.Id)
            };
            instance = locator.Save(instance);
            locator.FlushModifications();
            return instance;
        }

        public virtual void Update(IRepositoryLocator locator, InvoiceDto operation)
        {
            ValidateOperation(locator, operation);
            CheckForDuplicates(locator, operation);
            Transaction = operation.Transaction == null ? null : locator.GetById<Transaction>(operation.Transaction.Id);
            DocType = operation.DocType;
            DocDate = operation.DocDate;
            PaymentType = operation.PaymentType;
            CustomerDiscount = operation.CustomerDiscount;
            Deposit = operation.Deposit;
            DepositDate = operation.DepositDate;
            PaidAmount = operation.PaidAmount;
            Status = operation.Status;
            Creator = operation.Creator == null ? null : locator.GetById<Employee>(operation.Creator.Id);
            CollectedBy = operation.CollectedBy == null ? null : locator.GetById<Employee>(operation.CollectedBy.Id);
            locator.Update(this);
        }


        private static void CheckForDuplicates(IRepositoryLocator locator, InvoiceDto op)
        {
            var result = locator.FindAll<Invoice>().Where(p => p.Transaction.Id == op.Transaction.Id &&
                                                               p.DocType == op.DocType &&
                                                               p.DocDate == op.DocDate &&
                                                               p.Id != op.Id);

            if (!result.Any()) return;
            throw new BusinessException(BusinessExceptionEnum.Validation,
                                        "An invoice instance for this invoice already exist with the same details.");
        }

        
        #region Invoice items

        public virtual ReadOnlyCollection<InvoiceRow> InvoiceItems()
        {
            if (InvoiceItemSet == null) return null;
            return new ReadOnlyCollection<InvoiceRow>(InvoiceItemSet.OrderBy(ir => ir.ItemNo).ToArray());
        }

        public virtual InvoiceRow AddInvoiceItem(IRepositoryLocator locator, InvoiceRowDto operation)
        {
            AddInvoiceRowValidate(locator, operation);
            var invoiceRow = InvoiceRow.Create(locator, operation);
            InvoiceItemSet.Add(invoiceRow);
            return invoiceRow;
        }

        private void AddInvoiceRowValidate(IRepositoryLocator locator, InvoiceRowDto operation)
        {
            if (operation.InvoiceId.Equals(Id)) return;
            throw new BusinessException(BusinessExceptionEnum.Validation, "Incorrect Invoice Id was passed with the invoice item details");
        }

        public virtual void DeleteInvoiceRow(IRepositoryLocator locator, long invoiceRowId)
        {
            DeleteInvoiceRowValidate(locator, invoiceRowId);
            var invoiceRow = InvoiceItemSet.Single(ir => ir.Id.Equals(invoiceRowId));
            InvoiceItemSet.Remove(invoiceRow);
            locator.Remove(invoiceRow);
        }

        private void DeleteInvoiceRowValidate(IRepositoryLocator locator, long invoiceRowId)
        {
            return;
        }

        public class Mapping : EntityTypeConfiguration<Invoice>
        {
            public Mapping()
            {
                HasMany(i => i.InvoiceItemSet).WithRequired(ir => ir.Invoice);
            }
        }

        #endregion
    }
}
