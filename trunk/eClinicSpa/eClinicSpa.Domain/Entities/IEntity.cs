﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace eClinicSpa.Domain.Entities
{
    /// <remarks>
    /// version 0.2 Chapter: Repository
    /// </remarks>
    public interface IEntity
    {
        long Id { get; }
    }
}
