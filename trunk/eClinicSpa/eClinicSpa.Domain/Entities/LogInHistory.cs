﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using eClinicSpa.Common.Dto.Address;
using eClinicSpa.Common.Message;
using eClinicSpa.Domain.Repository;
using eClinicSpa.Common.Dto.Employee;
using System.Collections.ObjectModel;


namespace eClinicSpa.Domain.Entities
{
    /// <remarks>
    /// version 0.03 Chapter III:  The Response
    /// version 0.13 Chapter XIII: Business Domain Extension
    /// </remarks>
    [JsonObject(IsReference = true)]
    public class LogInHistory
        :EntityBase
    {
        protected LogInHistory()
        {
        }

        public virtual DateTime LogInDate { get; private set; }
        public virtual Employee LogInUser { get; private set; }

        public static LogInHistory Create(IRepositoryLocator locator, LogInHistoryDto operation)
        {
            ValidateOperation(locator, operation);
            CheckForDuplicates(locator, operation);
            var instance = new LogInHistory
            {
                LogInDate = operation.LogInDate,
                LogInUser = operation.LogInUser == null ? null : locator.GetById<Employee>(operation.LogInUser.Id)
            };

            locator.Save(instance);
            return instance;
        }

        private static void CheckForDuplicates(IRepositoryLocator locator, LogInHistoryDto op)
        {
            var result = locator.FindAll<LogInHistory>().Where(e => e.LogInDate == op.LogInDate &&
                                                                   e.LogInUser.Id == op.LogInUser.Id &&
                                                                   e.Id != op.Id).ToArray();

            if (!result.Any()) return;
            throw new BusinessException(BusinessExceptionEnum.Validation, 
                                        "A log in record instance already exist.");
        }

        public virtual void Update(IRepositoryLocator locator, LogInHistoryDto operation)
        {
            ValidateOperation(locator, operation);
            CheckForDuplicates(locator, operation);
            LogInDate = operation.LogInDate;
            LogInUser = operation.LogInUser == null ? null : locator.GetById<Employee>(operation.LogInUser.Id);
            locator.Update(this);
        }
    }
}
