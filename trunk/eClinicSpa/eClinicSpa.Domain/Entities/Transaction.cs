﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using eClinicSpa.Common.Dto.Address;
using eClinicSpa.Common.Message;
using eClinicSpa.Domain.Repository;
using eClinicSpa.Common.Dto.Transaction;
using System.Collections.ObjectModel;


namespace eClinicSpa.Domain.Entities
{
    /// <remarks>
    /// version 0.03 Chapter III:  The Response
    /// version 0.13 Chapter XIII: Business Domain Extension
    /// </remarks>
    [JsonObject(IsReference = true)]
    public class Transaction
        :EntityBase
    {
        protected Transaction()
        {
        }

        public virtual DateTime TransactionDate { get; private set; }
        public virtual int Order { get; private set; }
        public virtual Customer Customer { get; private set; }
        public virtual Employee InConsultant { get; private set; }
        public virtual Employee OutConsultant { get; private set; }
        public virtual Employee AlternativeTherapist { get; private set; }
        public virtual DateTime? TreatmentDate { get; private set; }
        public virtual DateTime? OutGoingDate { get; private set; }
        public virtual int Status { get; private set; }
        public virtual Employee Creator { get; private set; }

        public static Transaction Create(IRepositoryLocator locator, TransactionDto operation)
        {
            ValidateOperation(locator, operation);
            CheckForDuplicates(locator, operation);
            var instance = new Transaction
                            {
                                TransactionDate = operation.TransactionDate,
                                Order = operation.Order,
                                Customer = operation.Customer == null ? null : locator.GetById<Customer>(operation.Customer.Id),
                                InConsultant = operation.InConsultant == null ? null : locator.GetById<Employee>(operation.InConsultant.Id),
                                OutConsultant = operation.OutConsultant == null ? null : locator.GetById<Employee>(operation.OutConsultant.Id),
                                AlternativeTherapist = operation.AlternativeTherapist == null ? null : locator.GetById<Employee>(operation.AlternativeTherapist.Id),
                                TreatmentDate = operation.TreatmentDate,
                                OutGoingDate = operation.OutGoingDate,
                                Status = operation.Status,
                                Creator = operation.Creator == null ? null : locator.GetById<Employee>(operation.Creator.Id)
                            };

            locator.Save(instance);
            return instance;
        }

        private static void CheckForDuplicates(IRepositoryLocator locator, TransactionDto op)
        {
            var result = locator.FindAll<Transaction>().Where(e => e.TransactionDate == op.TransactionDate &&
                                                                   e.Customer.Id == op.Customer.Id &&
                                                                   e.Order == op.Order &&
                                                                   e.Id != op.Id).ToArray();

            if (!result.Any()) return;
            throw new BusinessException(BusinessExceptionEnum.Validation, 
                                        "A transaction instance already exist.");
        }

        public virtual void Update(IRepositoryLocator locator, TransactionDto operation)
        {
            ValidateOperation(locator, operation);
            CheckForDuplicates(locator, operation);
            TransactionDate = operation.TransactionDate;
            Order = operation.Order;
            Customer = operation.Customer == null ? null : locator.GetById<Customer>(operation.Customer.Id);
            InConsultant = operation.InConsultant == null ? null : locator.GetById<Employee>(operation.InConsultant.Id);
            OutConsultant = operation.OutConsultant == null ? null : locator.GetById<Employee>(operation.OutConsultant.Id);
            AlternativeTherapist = operation.AlternativeTherapist == null ? null : locator.GetById<Employee>(operation.AlternativeTherapist.Id);
            TreatmentDate = operation.TreatmentDate;
            OutGoingDate = operation.OutGoingDate;
            Status = operation.Status;
            Creator = operation.Creator == null ? null : locator.GetById<Employee>(operation.Creator.Id);
            locator.Update(this);
        }
    }
}
