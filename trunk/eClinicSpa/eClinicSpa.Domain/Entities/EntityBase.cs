﻿using System.ComponentModel.DataAnnotations;
using eClinicSpa.Common.Message;
using eClinicSpa.Domain.Repository;

namespace eClinicSpa.Domain.Entities
{
    /// <remarks>
    /// version 0.2 Chapter: Repository
    /// </remarks>
    public abstract class EntityBase
        :IEntity
    {
        [Key]
        public virtual long Id { get; protected set; }

        protected static void ValidateOperation(IRepositoryLocator locator, ValidatorDtoBase operation)
        {
            if (operation.IsValid()) return;
            throw new BusinessException(BusinessExceptionEnum.Validation, operation.Error);
        }
    }
}
