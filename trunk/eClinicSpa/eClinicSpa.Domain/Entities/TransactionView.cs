﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using eClinicSpa.Common.Dto.Address;
using eClinicSpa.Common.Message;
using eClinicSpa.Domain.Repository;
using eClinicSpa.Common.Dto.Employee;
using System.Collections.ObjectModel;
//using Iesi.Collections.Generic;


namespace eClinicSpa.Domain.Entities
{
    /// <remarks>
    /// version 0.03 Chapter III:  The Response
    /// version 0.13 Chapter XIII: Business Domain Extension
    /// </remarks>
    [JsonObject(IsReference = true)]
    public class TransactionView
        : EntityBase
    {
        protected TransactionView()
        {
        }

        public virtual DateTime DocDate { get; private set; }

        public virtual long CustomerId { get; private set; }
        public virtual string CustomerName { get; private set; }

        public virtual Decimal CustomerDiscount { get; private set; }

        public virtual Decimal Deposit { get; private set; }
        public virtual DateTime? DepositDate { get; private set; }
        public virtual Decimal PaidAmount { get; private set; }

        public virtual long ProductId { get; private set; }
        public virtual string ProductName { get; private set; }

        public virtual int Quantity { get; private set; }
        public virtual Decimal UnitPrice { get; private set; }
        public virtual Decimal Amount { get; private set; }
        public virtual Decimal ProductDiscountAmount { get; private set; }
    }
}
