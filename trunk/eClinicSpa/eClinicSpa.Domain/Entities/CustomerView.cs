﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using eClinicSpa.Common.Dto.Address;
using eClinicSpa.Common.Message;
using eClinicSpa.Domain.Repository;
using eClinicSpa.Common.Dto.Employee;
using System.Collections.ObjectModel;
//using Iesi.Collections.Generic;


namespace eClinicSpa.Domain.Entities
{
    /// <remarks>
    /// version 0.03 Chapter III:  The Response
    /// version 0.13 Chapter XIII: Business Domain Extension
    /// </remarks>
    [JsonObject(IsReference = true)]
    public class CustomerView
        : EntityBase
    {
        protected CustomerView()
        {
        }
        public virtual string FirstName { get; private set; }
        public virtual string LastName { get; private set; }

        public virtual string Phone { get; private set; }
        public virtual int Gender { get; private set; }
        public virtual DateTime DayOfBirth { get; private set; }
        public virtual string CurrentAddress { get; private set; }
        public virtual int DistrictNo { get; private set; }
        public virtual bool Vip { get; private set; }
        public virtual string VipCardNumber { get; private set; }
        public virtual string CardNumber { get; private set; }

        public virtual long TreatmentConsultantId { get; private set; }
        public virtual string TreatmentConsultantName { get; private set; }
        public virtual long TherapistId { get; private set; }
        public virtual string TherapistName { get; private set; }
    }
}
