﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using eClinicSpa.Common.Dto.Address;
using eClinicSpa.Common.Message;
using eClinicSpa.Domain.Repository;
using eClinicSpa.Common.Dto.Employee;
using System.Collections.ObjectModel;
//using Iesi.Collections.Generic;


namespace eClinicSpa.Domain.Entities
{
    /// <remarks>
    /// version 0.03 Chapter III:  The Response
    /// version 0.13 Chapter XIII: Business Domain Extension
    /// </remarks>
    [JsonObject(IsReference = true)]
    public class Employee
        :EntityBase
    {
        protected Employee()
        {
        }

        public virtual string Name { get; private set; }

        public virtual string CurrentAddress { get; private set; }
        public virtual string District { get; private set; }
        public virtual string Phone { get; private set; }
        public virtual string Email { get; private set; }
        public virtual int EmployeeType { get; private set; }
        public virtual int Status { get; private set; }
        public virtual string UserName { get; private set; }
        public virtual string Password { get; private set; }

        public static Employee Create(IRepositoryLocator locator, EmployeeDto operation)
        {
            ValidateOperation(locator, operation);
            CheckForDuplicates(locator, operation);
            var instance = new Employee
                            {
                                Name = operation.Name,
                                CurrentAddress = operation.CurrentAddress,
                                District = operation.District,
                                Phone = operation.Phone,
                                Email = operation.Email,
                                EmployeeType = operation.EmployeeType,
                                Status = operation.Status,
                                UserName = operation.UserName,
                                Password = operation.Password
                            };

            locator.Save(instance);
            return instance;
        }

        private static void CheckForDuplicates(IRepositoryLocator locator, EmployeeDto op)
        {
            var result = locator.FindAll<Employee>().Where(e => e.Name == op.Name &&
                                                                e.UserName == op.UserName &&
                                                                e.Id != op.Id).ToArray();

            if (!result.Any()) return;
            throw new BusinessException(BusinessExceptionEnum.Validation, 
                                        "An employee instance already exist.");
        }

        public virtual void Update(IRepositoryLocator locator, EmployeeDto operation)
        {
            ValidateOperation(locator, operation);
            CheckForDuplicates(locator, operation);
            Name = operation.Name;
            CurrentAddress = String.IsNullOrWhiteSpace(operation.CurrentAddress) ? null : operation.CurrentAddress;
            District = operation.District;
            Phone = operation.Phone;
            Email = operation.Email;
            EmployeeType = operation.EmployeeType;
            Status = operation.Status;
            UserName = operation.UserName;
            Password = operation.Password;
            locator.Update(this);
        }
    }
}
