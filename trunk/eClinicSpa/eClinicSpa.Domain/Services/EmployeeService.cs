﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using AutoMapper;
using eClinicSpa.Common.ServiceContract;
using eClinicSpa.Common.Dto.Employee;
using eClinicSpa.Common.Filter;
using eClinicSpa.Domain.AppServices.WcfRequestContext;
using eClinicSpa.Domain.Entities;
using eClinicSpa.Domain.Repository;
using eClinicSpa.Domain.AppServices;
using eClinicSpa.Common.Message;
using System;
using eClinicSpa.Common.Constant;
using System.Data.Entity;

namespace eClinicSpa.Domain.Services
{
    /// <remarks>
    /// version 0.04 Chapter IV:   Transaction Manager
    /// version 0.07               Context Re-Factor
    /// version 0.13 Chapter XIII: Business Domain Extension
    /// </remarks>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [InstanceCreation]
    public class EmployeeService
        :ServiceBase, IEmployeeService
    {
        private readonly IBusinessNotifier BusinessNotifier;

        public EmployeeService()
        {
            BusinessNotifier = Container.RequestContext.Notifier;
        }

        #region IEmployeeService Members

        public EmployeeDto CreateNewEmployee(EmployeeDto dto)
        {
            return ExecuteCommand(locator => CreateNewEmployeeCommand(locator, dto));
        }

        private EmployeeDto CreateNewEmployeeCommand(IRepositoryLocator locator, EmployeeDto dto)
        {
            var employee = Employee.Create(locator, dto);
            locator.FlushModifications();
            //Thread.Sleep(10000);
            return Employee_to_Dto(employee);
        }

        public EmployeeDto GetById(long id)
        {
            return ExecuteCommand(locator => Employee_to_Dto(locator.GetById<Employee>(id)));
        }

        public EmployeeDto UpdateEmployee(EmployeeDto dto)
        {
            return ExecuteCommand(locator => UpdateEmployeeCommand(locator, dto));
        }

        private EmployeeDto UpdateEmployeeCommand(IRepositoryLocator locator, EmployeeDto dto)
        {
            var instance = locator.GetById<Employee>(dto.Id);
            instance.Update(locator, dto);            
            return Employee_to_Dto(instance);
        }

        public EmployeeDtos FindAll()
        {
            return ExecuteCommand(FindAllCommand);
        }

        public EmployeeDtos FindByFilter(string SearchFilter)
        {
            return ExecuteCommand(locator => FindByFilterCommand(locator, SearchFilter));
        }

        public EmployeeDtos FindAllConsultant()
        {
            return ExecuteCommand(FindAllConsultantCommand);
        }

        public EmployeeDtos FindAllTherapist()
        {
            return ExecuteCommand(FindAllTherapistCommand);
        }

        public EmployeeDtos FindAllCashier()
        {
            return ExecuteCommand(FindAllCashierCommand);
        }

        public EmployeeDtos GetByCredential(string username, string password)
        {
            return ExecuteCommand(locator => FindByCredentialCommand(locator, username, password));
        }

        public DtoResponse DeleteEmployee(long id)
        {
            return ExecuteCommand(locator => DeleteEmployeeCommand(locator, id));
        }

        private DtoResponse DeleteEmployeeCommand(IRepositoryLocator locator, long id)
        {
            var employee = locator.GetById<Employee>(id);            
            BusinessNotifier.AddWarning(BusinessWarningEnum.Default,
                                        string.Format("Employee {0} [id:{1}] was deleted",
                                                      employee.Name,
                                                      employee.Id));

            locator.Remove(employee);
            return new DtoResponse();
        }

        private EmployeeDtos FindAllCommand(IRepositoryLocator locator)
        {
            var result = new EmployeeDtos { Employees = new List<EmployeeDto>() };
            var employees = locator.FindAll<Employee>().ToList();
            result.Employees = Mapper.Map<List<Employee>, List<EmployeeDto>>(employees);
            if (result.Employees.Count() == 0) 
            { 
                BusinessNotifier.AddWarning(BusinessWarningEnum.Default, "No employee instances were found"); 
            }
            return result;
        }

        private EmployeeDtos FindByFilterCommand(IRepositoryLocator locator, string SearchFilter)
        {
            var result = new EmployeeDtos { Employees = new List<EmployeeDto>() };
            if (string.IsNullOrEmpty(SearchFilter))
            {
                return result;
            }
            var query = locator.FindAll<Employee>().Where(p => p.Name.Contains(SearchFilter));
            var employees = query.ToList();
            result.Employees = Mapper.Map<List<Employee>, List<EmployeeDto>>(employees);
            if (result.Employees.Count() == 0)
            {
                BusinessNotifier.AddWarning(BusinessWarningEnum.Default, "No employee instances were found");
            }
            return result;
        }

        private EmployeeDtos FindAllConsultantCommand(IRepositoryLocator locator)
        {
            var result = new EmployeeDtos { Employees = new List<EmployeeDto>() };
            var employees = locator.FindAll<Employee>()
                .Where(e => e.Status == EmployeeStatus.Active)
                .Where(e => e.EmployeeType == EmployeeRole.TreatmentConsultant || e.EmployeeType == EmployeeRole.TreatmentConsultantAndTherapist)
                .ToList();
            result.Employees = Mapper.Map<List<Employee>, List<EmployeeDto>>(employees);
            if (result.Employees.Count() == 0)
            {
                BusinessNotifier.AddWarning(BusinessWarningEnum.Default, "No employee instances were found");
            }
            return result;
        }

        private EmployeeDtos FindAllTherapistCommand(IRepositoryLocator locator)
        {
            var result = new EmployeeDtos { Employees = new List<EmployeeDto>() };
            var employees = locator.FindAll<Employee>()
                .Where(e => e.Status == EmployeeStatus.Active)
                .Where(e => e.EmployeeType == EmployeeRole.Therapist || e.EmployeeType == EmployeeRole.TreatmentConsultantAndTherapist)
                .ToList();
            result.Employees = Mapper.Map<List<Employee>, List<EmployeeDto>>(employees);
            if (result.Employees.Count() == 0)
            {
                BusinessNotifier.AddWarning(BusinessWarningEnum.Default, "No employee instances were found");
            }
            return result;
        }

        private EmployeeDtos FindAllCashierCommand(IRepositoryLocator locator)
        {
            var result = new EmployeeDtos { Employees = new List<EmployeeDto>() };
            var query = locator.FindAll<Employee>().Where(e => e.Status == EmployeeStatus.Active)
                .Where(e => e.EmployeeType == EmployeeRole.Cashier);
            var employees = query.ToList();
            result.Employees = Mapper.Map<List<Employee>, List<EmployeeDto>>(employees);
            if (result.Employees.Count() == 0)
            {
                BusinessNotifier.AddWarning(BusinessWarningEnum.Default, "No employee instances were found");
            }
            return result;
        }

        private EmployeeDtos FindByCredentialCommand(IRepositoryLocator locator, string username, string password)
        {
            var result = new EmployeeDtos { Employees = new List<EmployeeDto>() };
            var employees = locator.FindAll<Employee>()
                .Where(e => e.UserName.Equals(username, StringComparison.OrdinalIgnoreCase) 
                         && e.Password.Equals(password))
                .ToList();
            result.Employees = Mapper.Map<List<Employee>, List<EmployeeDto>>(employees);
            if (result.Employees.Count() == 0)
            {
                BusinessNotifier.AddWarning(BusinessWarningEnum.Default, "No employee instances were found");
            }
            return result;
        }

        #endregion

        #region LogInHistory Members

        public LogInHistoryDto CreateNewLogInHistory(LogInHistoryDto dto)
        {
            return ExecuteCommand(locator => CreateNewLogInHistoryCommand(locator, dto));
        }
        private LogInHistoryDto CreateNewLogInHistoryCommand(IRepositoryLocator locator, LogInHistoryDto dto)
        {
            var logInRecord = LogInHistory.Create(locator, dto);
            locator.FlushModifications();
            return LogInHistory_to_Dto(logInRecord);
        }

        public LogInHistoryDtos FindLogInHistory(DateTime from, DateTime to)
        {
            return ExecuteCommand(locator => FindLogInHistoryCommand(locator, from, to));
        }
        private LogInHistoryDtos FindLogInHistoryCommand(IRepositoryLocator locator, DateTime from, DateTime to)
        {
            var result = new LogInHistoryDtos { LogInHistories = new List<LogInHistoryDto>() };
            var logInRecords = locator.FindAll<LogInHistory>().Where(e => DbFunctions.TruncateTime(e.LogInDate) >= from &&
                                                                          DbFunctions.TruncateTime(e.LogInDate) <= to)
                                                              .OrderByDescending(e => e.LogInDate).ToList();
            result.LogInHistories = Mapper.Map<List<LogInHistory>, List<LogInHistoryDto>>(logInRecords);
            if (result.LogInHistories.Count() == 0)
            {
                BusinessNotifier.AddWarning(BusinessWarningEnum.Default, "No log in record instances were found");
            }
            return result;
        }

        #endregion

        #region Employee Status View Members

        public ConsultantStatusViewDtos GetConsultantStatusView()
        {
            return ExecuteCommand(GetConsultantStatusViewCommand);
        }
        private ConsultantStatusViewDtos GetConsultantStatusViewCommand(IRepositoryLocator locator)
        {
            var result = new ConsultantStatusViewDtos { ConsultantStatusViews = new List<ConsultantStatusViewDto>() };
            var consultants = locator.FindAll<ConsultantStatusView>().ToList();
            result.ConsultantStatusViews = Mapper.Map<List<ConsultantStatusView>, List<ConsultantStatusViewDto>>(consultants);
            if (result.ConsultantStatusViews.Count() == 0)
            {
                BusinessNotifier.AddWarning(BusinessWarningEnum.Default, "No ConsultantStatusView instances were found");
            }
            return result;
        }

        public TherapistStatusViewDtos GetTherapistStatusView()
        {
            return ExecuteCommand(GetTherapistStatusViewCommand);
        }
        private TherapistStatusViewDtos GetTherapistStatusViewCommand(IRepositoryLocator locator)
        {
            var result = new TherapistStatusViewDtos { TherapistStatusViews = new List<TherapistStatusViewDto>() };
            var consultants = locator.FindAll<TherapistStatusView>().ToList();
            result.TherapistStatusViews = Mapper.Map<List<TherapistStatusView>, List<TherapistStatusViewDto>>(consultants);
            if (result.TherapistStatusViews.Count() == 0)
            {
                BusinessNotifier.AddWarning(BusinessWarningEnum.Default, "No TherapistStatusView instances were found");
            }
            return result;
        }

        #endregion

        private EmployeeDto Employee_to_Dto(Employee employee)
        {
            return Mapper.Map<Employee, EmployeeDto>(employee);          
        }

        private LogInHistoryDto LogInHistory_to_Dto(LogInHistory employee)
        {
            return Mapper.Map<LogInHistory, LogInHistoryDto>(employee);
        }
    }
}
