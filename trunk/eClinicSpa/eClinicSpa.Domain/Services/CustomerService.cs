﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using AutoMapper;
using eClinicSpa.Common.ServiceContract;
using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.Common.Filter;
using eClinicSpa.Domain.AppServices.WcfRequestContext;
using eClinicSpa.Domain.Entities;
using eClinicSpa.Domain.Repository;
using eClinicSpa.Domain.AppServices;
using eClinicSpa.Common.Message;
using System;
using System.Globalization;

namespace eClinicSpa.Domain.Services
{
    /// <remarks>
    /// version 0.04 Chapter IV:   Transaction Manager
    /// version 0.07               Context Re-Factor
    /// version 0.13 Chapter XIII: Business Domain Extension
    /// </remarks>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [InstanceCreation]
    public class CustomerService
        :ServiceBase, ICustomerService
    {
        private readonly IBusinessNotifier BusinessNotifier;

        public CustomerService()
        {
            BusinessNotifier = Container.RequestContext.Notifier;
        }

        #region ICustomerService Members

        public CustomerDto CreateNewCustomer(CustomerDto dto)
        {
            return ExecuteCommand(locator => CreateNewCustomerCommand(locator, dto));
        }

        private CustomerDto CreateNewCustomerCommand(IRepositoryLocator locator, CustomerDto dto)
        {
            var customer = Customer.Create(locator, dto);
            locator.FlushModifications();
            //Thread.Sleep(10000);
            return Customer_to_Dto(customer);
        }

        public CustomerDto GetById(long id)
        {
            return ExecuteCommand(locator => Customer_to_Dto(locator.GetById<Customer>(id)));
        }

        public CustomerDto UpdateCustomer(CustomerDto dto)
        {
            return ExecuteCommand(locator => UpdateCustomerCommand(locator, dto));
        }

        private CustomerDto UpdateCustomerCommand(IRepositoryLocator locator, CustomerDto dto)
        {
            var instance = locator.GetById<Customer>(dto.Id);
            instance.Update(locator, dto);            
            return Customer_to_Dto(instance);
        }

        public CustomerDtos FindAll()
        {
            return ExecuteCommand(FindAllCommand);
        }

        public CustomerDtos FindByFilter(CustomerSearchFilter filter)
        {
            return ExecuteCommand(locator => FindByFilterCommand(locator, filter));
        }

        public DtoResponse DeleteCustomer(long id)
        {
            return ExecuteCommand(locator => DeleteCustomerCommand(locator, id));
        }

        public DtoResponse DeleteAddress(long customerId, long addressId)
        {
            return ExecuteCommand(locator => DeleteAddressCommand(locator, customerId, addressId));
        }

        private DtoResponse DeleteAddressCommand(IRepositoryLocator locator, long customerId, long addressId)
        {
            var customer = locator.GetById<Customer>(customerId);
            customer.DeleteAddress(locator, addressId);
            return new DtoResponse();

        }

        private DtoResponse DeleteCustomerCommand(IRepositoryLocator locator, long id)
        {
            var customer = locator.GetById<Customer>(id);            
            BusinessNotifier.AddWarning(BusinessWarningEnum.Default,
                                        string.Format("Customer {0} {1} [id:{2}] was deleted",
                                                      customer.FirstName,
                                                      customer.LastName,
                                                      customer.Id));

            locator.Remove(customer);
            return new DtoResponse();
        }

        private CustomerDtos FindAllCommand(IRepositoryLocator locator)
        {
            var result = new CustomerDtos { Customers = new List<CustomerDto>() };
            var customers = locator.FindAll<Customer>().ToList();
            result.Customers = Mapper.Map<List<Customer>, List<CustomerDto>>(customers);
            if (result.Customers.Count() == 0) 
            { 
                BusinessNotifier.AddWarning(BusinessWarningEnum.Default, "No customer instances were found"); 
            }
            return result;
        }

        private CustomerDtos FindByFilterCommand(IRepositoryLocator locator, CustomerSearchFilter filter)
        {
            var result = new CustomerDtos { Customers = new List<CustomerDto>() };
            if (filter.IsEmpty)
            {
                return result;
            }
            var query = locator.FindAll<Customer>();
            if (!string.IsNullOrWhiteSpace(filter.FirstName))
            {
                query = query.Where(c => c.FirstName.Contains(filter.FirstName));
            }
            if (!string.IsNullOrWhiteSpace(filter.LastName))
            {
                query = query.Where(c => c.LastName.Contains(filter.LastName));
            }
            if (!string.IsNullOrWhiteSpace(filter.Phone))
            {
                query = query.Where(c => c.Phone.Contains(filter.Phone));
            }
            var customers = query.ToList();
            result.Customers = Mapper.Map<List<Customer>, List<CustomerDto>>(customers);
            if (result.Customers.Count() == 0)
            {
                BusinessNotifier.AddWarning(BusinessWarningEnum.Default, "No customer instances were found");
            }
            return result;
        }

        #endregion
        
        #region Customer View Members

        public CustomerViewDtos FindViewByFilter(CustomerSearchFilter filter)
        {
            return ExecuteCommand(locator => FindViewByFilterCommand(locator, filter));
        }
        private CustomerViewDtos FindViewByFilterCommand(IRepositoryLocator locator, CustomerSearchFilter filter)
        {
            var result = new CustomerViewDtos { Customers = new List<CustomerViewDto>() };
            if (filter.IsEmpty)
            {
                return result;
            }
            var query = locator.FindAll<CustomerView>();
            if (!string.IsNullOrWhiteSpace(filter.FirstName))
            {
                query = query.Where(c => c.FirstName.Trim().Equals(filter.FirstName.Trim()));
            }
            if (!string.IsNullOrWhiteSpace(filter.LastName))
            {
                query = query.Where(c => c.LastName.Contains(filter.LastName));
            }
            if (!string.IsNullOrWhiteSpace(filter.Phone))
            {
                query = query.Where(c => c.Phone.Contains(filter.Phone));
            }
            var customers = query.ToList();
            result.Customers = Mapper.Map<List<CustomerView>, List<CustomerViewDto>>(customers);
            if (result.Customers.Count() == 0)
            {
                BusinessNotifier.AddWarning(BusinessWarningEnum.Default, "No customer instances were found");
            }
            return result;
        }

        public CustomerViewDtos GetAllCustomerView()
        {
            return ExecuteCommand(locator => GetCustomerViewCommand(locator, false, 0L, 0L));
        }

        public CustomerViewDtos GetCustomerViewByFilter(bool vip, long consultantId, long therapistId)
        {
            return ExecuteCommand(locator => GetCustomerViewCommand(locator, vip, consultantId, therapistId));
        }

        private CustomerViewDtos GetCustomerViewCommand(IRepositoryLocator locator, bool vip, long consultantId, long therapistId)
        {
            var result = new CustomerViewDtos { Customers = new List<CustomerViewDto>() };
            var query = locator.FindAll<CustomerView>();//.Where(c => c.Id > 40000L);
            if (vip == true)
            {
                query = query.Where(c => c.Vip == true);
            }
            if (consultantId != 0L)
            {
                query = query.Where(c => c.TreatmentConsultantId == consultantId);
            }
            if (therapistId != 0L)
            {
                query = query.Where(c => c.TherapistId == therapistId);
            }
            var customers = query.OrderBy(c => c.FirstName + " " + c.LastName).ToList();
            result.Customers = Mapper.Map<List<CustomerView>, List<CustomerViewDto>>(customers);
            if (result.Customers.Count() == 0)
            {
                BusinessNotifier.AddWarning(BusinessWarningEnum.Default, "No customer instances were found");
            }
            return result;
        }

        #endregion

        private CustomerDto Customer_to_Dto(Customer customer)
        {
            return Mapper.Map<Customer, CustomerDto>(customer);          
        }
    }
}
