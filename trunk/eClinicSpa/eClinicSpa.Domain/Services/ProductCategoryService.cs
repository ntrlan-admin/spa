﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using AutoMapper;
using eClinicSpa.Common.Constant;
using eClinicSpa.Common.Dto.Product;
using eClinicSpa.Common.Dto.ProductCategory;
using eClinicSpa.Common.Message;
using eClinicSpa.Common.ServiceContract;
using eClinicSpa.Domain.AppServices;
using eClinicSpa.Domain.AppServices.WcfRequestContext;
using eClinicSpa.Domain.Entities;
using eClinicSpa.Domain.Repository;

namespace eClinicSpa.Domain.Services
{
    /// <summary>
    /// version 0.13 Chapter XIII - Business Domain Extension
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [InstanceCreation]
    public class ProductCategoryService
        : ServiceBase, IProductCategoryService
    {

        private readonly IBusinessNotifier BusinessNotifier;

        public ProductCategoryService()
        {
            BusinessNotifier = Container.RequestContext.Notifier;
        }

        #region Implementation of IProductService

        public ProductDto GetProductById(long id)
        {
            return ExecuteCommand(locator => ProductToProductDto(locator.GetById<Product>(id)));
        }

        public ProductDtos FindAllActiveProduct()
        {
            return ExecuteCommand(locator => FindAllProductCommand(locator, ProductStatus.Active));
        }
        public ProductDtos FindAllProduct()
        {
            return ExecuteCommand(locator => FindAllProductCommand(locator));
        }
        private ProductDtos FindAllProductCommand(IRepositoryLocator locator, int status = -1)
        {
            var result = new ProductDtos { Products = new List<ProductDto>() };
            var query = locator.FindAll<Product>();
            if (status != ProductStatus.AllProducts)
            {
                query = query.Where(p => p.Status == status);
            }
            var products = query.OrderBy(p => p.Name).ToList();
            result.Products = Mapper.Map<List<Product>, List<ProductDto>>(products);
            if (result.Products.Count() == 0)
            {
                BusinessNotifier.AddWarning(BusinessWarningEnum.Default, "No product instances were found");
            }
            return result;
        }

        public ProductDtos FindByFilter(string SearchFilter)
        {
            return ExecuteCommand(locator => FindByFilterCommand(locator, SearchFilter));
        }
        private ProductDtos FindByFilterCommand(IRepositoryLocator locator, string SearchFilter)
        {
            var result = new ProductDtos { Products = new List<ProductDto>() };
            if (string.IsNullOrEmpty(SearchFilter))
            {
                return result;
            }
            var query = locator.FindAll<Product>().Where(p => p.Name.Contains(SearchFilter));
            var products = query.ToList();
            result.Products = Mapper.Map<List<Product>, List<ProductDto>>(products);
            if (result.Products.Count() == 0)
            {
                BusinessNotifier.AddWarning(BusinessWarningEnum.Default, "No product instances were found");
            }
            return result;
        }

        public ProductDto UpdateProduct(ProductDto product)
        {
            return ExecuteCommand(locator => UpdateProductCommand(locator, product));
        }

        private static ProductDto UpdateProductCommand(IRepositoryLocator locator, ProductDto dto)
        {
            var product = locator.GetById<Product>(dto.Id);
            product.Update(locator, dto);
            return ProductToProductDto(product);
        }

        public ProductDto CreateNewProduct(ProductDto product)
        {
            return ExecuteCommand(locator => CreateNewProductCommand(locator, product));
        }

        private static ProductDto CreateNewProductCommand(IRepositoryLocator locator, ProductDto dto)
        {
            var product = Product.Create(locator, dto);
            locator.FlushModifications();

            return ProductToProductDto(product);
        }
        #endregion


        #region Implementation of IProductCategoryService

        public ProductCategoryDto GetProductCategoryById(long id)
        {
            return ExecuteCommand(locator => ProductCategoryToProductCategoryDto(locator.GetById<ProductCategory>(id)));
        }

        public ProductCategoryDtos FindAllProductCategory()
        {
            return ExecuteCommand(FindAllCommand);
        }

        private ProductCategoryDtos FindAllCommand(IRepositoryLocator locator)
        {
            var result = new ProductCategoryDtos { ProductCategories = new List<ProductCategoryDto>() };
            var productCategories = locator.FindAll<ProductCategory>().ToList();
            result.ProductCategories = Mapper.Map<List<ProductCategory>, List<ProductCategoryDto>>(productCategories);
            if (result.ProductCategories.Count() == 0)
            {
                BusinessNotifier.AddWarning(BusinessWarningEnum.Default, "No product category instances were found");
            }
            return result;
        }

        public ProductCategoryDto UpdateProductCategory(ProductCategoryDto category)
        {
            return ExecuteCommand(locator => UpdateProductCategoryCommand(locator, category));
        }

        private static ProductCategoryDto UpdateProductCategoryCommand(IRepositoryLocator locator, ProductCategoryDto dto)
        {
            var category = locator.GetById<ProductCategory>(dto.Id);
            category.Update(locator, dto);
            return ProductCategoryToProductCategoryDto(category);
        }

        public ProductCategoryDto CreateNewProductCategory(ProductCategoryDto category)
        {
            return ExecuteCommand(locator => CreateNewProductCategoryCommand(locator, category));
        }

        private static ProductCategoryDto CreateNewProductCategoryCommand(IRepositoryLocator locator, ProductCategoryDto dto)
        {
            var category = ProductCategory.Create(locator, dto);
            locator.FlushModifications();

            return ProductCategoryToProductCategoryDto(category);
        }

        private DtoResponse DeleteProductCategoryCommand(IRepositoryLocator locator, long id)
        {
            var category = locator.GetById<ProductCategory>(id);
            BusinessNotifier.AddWarning(BusinessWarningEnum.Default,
                                        string.Format("category with id:{0} was deleted",
                                        category.Id));

            locator.Remove(category);
            return new DtoResponse();
        }

        #endregion
        #region Private Methods

        private static ProductDto ProductToProductDto(Product product)
        {
            var dto = Mapper.Map<Product, ProductDto>(product);
            return dto;
        }

        private static ProductCategoryDto ProductCategoryToProductCategoryDto(ProductCategory category)
        {
            var dto = Mapper.Map<ProductCategory, ProductCategoryDto>(category);
            return dto;
        }

        #endregion
    }
}
