﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System;
using System.Data.Entity;

using AutoMapper;
using eClinicSpa.Common.ServiceContract;
using eClinicSpa.Common.Dto.Transaction;
using eClinicSpa.Common.Filter;
using eClinicSpa.Domain.AppServices.WcfRequestContext;
using eClinicSpa.Domain.Entities;
using eClinicSpa.Domain.Repository;
using eClinicSpa.Domain.AppServices;
using eClinicSpa.Common.Message;
using eClinicSpa.Common.Constant;

namespace eClinicSpa.Domain.Services
{
    /// <remarks>
    /// version 0.04 Chapter IV:   Transaction Manager
    /// version 0.07               Context Re-Factor
    /// version 0.13 Chapter XIII: Business Domain Extension
    /// </remarks>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [InstanceCreation]
    public class TransactionService
        :ServiceBase, ITransactionService
    {
        private readonly IBusinessNotifier BusinessNotifier;

        public TransactionService()
        {
            BusinessNotifier = Container.RequestContext.Notifier;
        }

        #region ITransactionService Members

        public TransactionDto CreateNewTransaction(TransactionDto dto)
        {
            return ExecuteCommand(locator => CreateNewTransactionCommand(locator, dto));
        }

        private TransactionDto CreateNewTransactionCommand(IRepositoryLocator locator, TransactionDto dto)
        {
            var transaction = Transaction.Create(locator, dto);
            locator.FlushModifications();
            //Thread.Sleep(10000);
            return Transaction_to_Dto(transaction);
        }

        public TransactionDto GetById(long id)
        {
            return ExecuteCommand(locator => Transaction_to_Dto(locator.GetById<Transaction>(id)));
        }

        public TransactionDto UpdateTransaction(TransactionDto dto)
        {
            return ExecuteCommand(locator => UpdateTransactionCommand(locator, dto));
        }

        private TransactionDto UpdateTransactionCommand(IRepositoryLocator locator, TransactionDto dto)
        {
            var instance = locator.GetById<Transaction>(dto.Id);
            instance.Update(locator, dto);            
            return Transaction_to_Dto(instance);
        }

        public TransactionDtos FindAll()
        {
            return ExecuteCommand(FindAllCommand);
        }

        public TransactionDtos FindByDate(DateTime date)
        {
            return ExecuteCommand(locator => FindByDateCommand(locator, date));
        }
        private TransactionDtos FindByDateCommand(IRepositoryLocator locator, DateTime date)
        {
            var result = new TransactionDtos { Transactions = new List<TransactionDto>() };
            if (date == null)
            {
                return result;
            }
            var query = locator.FindAll<Transaction>().Where(t => DbFunctions.TruncateTime(t.TransactionDate) == date.Date);
            var transactions = query.ToList();
            result.Transactions = Mapper.Map<List<Transaction>, List<TransactionDto>>(transactions);
            if (result.Transactions.Count() == 0)
            {
                BusinessNotifier.AddWarning(BusinessWarningEnum.Default, "No transaction instances were found");
            }
            return result;
        }

        public CustomerTransactionViewDtos FindCustomerTransactionByDate(DateTime date, CustomerTransactionSearchFilter searchFilter)
        {
            return ExecuteCommand(locator => FindCustomerTransactionByDateCommand(locator, date, searchFilter));
        }
        private CustomerTransactionViewDtos FindCustomerTransactionByDateCommand(IRepositoryLocator locator, DateTime date, CustomerTransactionSearchFilter searchFilter)
        {
            var result = new CustomerTransactionViewDtos { Transactions = new List<CustomerTransactionViewDto>() };
            if (date == null)
            {
                return result;
            }
            var query = locator.FindAll<CustomerTransactionView>()
                               .Where(t => DbFunctions.TruncateTime(t.TransactionDate) == date.Date);
            if (!searchFilter.IsEmpty)
            {
                query = query.Where(t => t.Status == searchFilter.Status);
            }
            var transactions = query.OrderByDescending(t => t.Order).ToList();
            result.Transactions = Mapper.Map<List<CustomerTransactionView>, List<CustomerTransactionViewDto>>(transactions);
            if (result.Transactions.Count() == 0)
            {
                BusinessNotifier.AddWarning(BusinessWarningEnum.Default, "No transaction instances were found");
            }
            return result;
        }

        public DtoResponse DeleteTransaction(long id)
        {
            return ExecuteCommand(locator => DeleteTransactionCommand(locator, id));
        }

        private DtoResponse DeleteTransactionCommand(IRepositoryLocator locator, long id)
        {
            var transaction = locator.GetById<Transaction>(id);            
            BusinessNotifier.AddWarning(BusinessWarningEnum.Default,
                                        string.Format("Transaction [id:{0}] was deleted",
                                                      transaction.Id));

            locator.Remove(transaction);
            return new DtoResponse();
        }

        private TransactionDtos FindAllCommand(IRepositoryLocator locator)
        {
            var result = new TransactionDtos { Transactions = new List<TransactionDto>() };
            var transactions = locator.FindAll<Transaction>().ToList();
            result.Transactions = Mapper.Map<List<Transaction>, List<TransactionDto>>(transactions);
            if (result.Transactions.Count() == 0) 
            { 
                BusinessNotifier.AddWarning(BusinessWarningEnum.Default, "No transaction instances were found"); 
            }
            return result;
        }

        public TransactionDtos GetTotalNumberByDate(DateTime date)
        {
            return ExecuteCommand(locator => GetTotalNumberByDateCommand(locator, date));
        }
        private TransactionDtos GetTotalNumberByDateCommand(IRepositoryLocator locator, DateTime date)
        {
            var result = new TransactionDtos { TotalNumberOfTransactions = 0 };
            if (date == null)
            {
                return result;
            }
            var query = locator.FindAll<Transaction>().Where(t => DbFunctions.TruncateTime(t.TransactionDate) == date.Date);
            result.TotalNumberOfTransactions = query.Count();
            return result;
        }

        #endregion


        #region Invoice Members

        public InvoiceDto CreateNewInvoice(InvoiceDto dto)
        {
            return ExecuteCommand(locator => CreateNewInvoiceCommand(locator, dto));
        }
        private InvoiceDto CreateNewInvoiceCommand(IRepositoryLocator locator, InvoiceDto dto)
        {
            var invoice = Invoice.Create(locator, dto);
            locator.FlushModifications();
            return Invoice_to_Dto(invoice);
        }

        public InvoiceDto GetInvoiceById(long id)
        {
            return ExecuteCommand(locator => Invoice_to_Dto(locator.GetById<Invoice>(id)));
        }

        private static List<Invoice> GetInvoiceList(IRepositoryLocator locator, long transactionId, int docType)
        {
            var invoices = locator.FindAll<Invoice>().Where(i => i.Transaction.Id == transactionId &&
                                                                 i.DocType == docType)
                                                     .OrderByDescending(i => i.Id).ToList();
            return invoices;
        }

        public InvoiceDto GetInvoiceByTransactionAndType(long transactionId, int docType)
        {
            return ExecuteCommand(locator => GetInvoiceByTransactionAndTypeCommand(locator, transactionId, docType));
        }
        private InvoiceDto GetInvoiceByTransactionAndTypeCommand(IRepositoryLocator locator, long transactionId, int docType)
        {
            var invoices = GetInvoiceList(locator, transactionId, docType);
            if (invoices.Count == 0)
            {
                BusinessNotifier.AddWarning(BusinessWarningEnum.Default, "No invoice instances were found");
                return new InvoiceDto { PaymentType = InvoiceConstant.PaymentType_Cash };
            }
            var invoice = invoices.First();
            return Invoice_to_Dto(invoice); ;
        }

        public InvoiceDtos GetInvoiceListByTransactionAndType(long transactionId, int docType)
        {
            return ExecuteCommand(locator => GetInvoiceListByTransactionAndTypeCommand(locator, transactionId, docType));
        }
        private InvoiceDtos GetInvoiceListByTransactionAndTypeCommand(IRepositoryLocator locator, long transactionId, int docType)
        {
            var query = GetInvoiceList(locator, transactionId, docType);
            var invoices = query.ToList();
            var result = new InvoiceDtos { Invoices = new List<InvoiceDto>() };
            result.Invoices = Mapper.Map<List<Invoice>, List<InvoiceDto>>(invoices);
            if (result.Invoices.Count() == 0)
            {
                BusinessNotifier.AddWarning(BusinessWarningEnum.Default, "No invoice instances were found");
            }
            return result;
        }

        public InvoiceDtos FindInvoiceByDateAndStatus(DateTime date, int status)
        {
            return ExecuteCommand(locator => FindInvoiceByDateAndStatusCommand(locator, date, status));
        }
        private InvoiceDtos FindInvoiceByDateAndStatusCommand(IRepositoryLocator locator, DateTime date, int status)
        {
            return FindInvoiceByFilterCommand(locator, date, status, 0L, 0L);
        }

        public InvoiceDtos FindInvoiceByFilter(DateTime date, int status, long cashierId, long therapistId)
        {
            return ExecuteCommand(locator => FindInvoiceByFilterCommand(locator, date, status, cashierId, therapistId));
        }
        private InvoiceDtos FindInvoiceByFilterCommand(IRepositoryLocator locator, DateTime date, int status, long cashierId, long therapistId)
        {
            var result = new InvoiceDtos { Invoices = new List<InvoiceDto>() };
            if (date == null)
            {
                return result;
            }
            var query = locator.FindAll<Invoice>().Where(i => DbFunctions.TruncateTime(i.DocDate) == date.Date &&
                                                              i.Status == status);
            if (cashierId != 0L)
            {
                query = query.Where(i => i.CollectedBy.Id == cashierId);
            }
            if (therapistId != 0L)
            {
                query = query.Where(i => i.Transaction.Customer.Therapist.Id == therapistId);
            }
            var invoices = query.OrderBy(i => i.DocDate).ToList();
            result.Invoices = Mapper.Map<List<Invoice>, List<InvoiceDto>>(invoices);
            if (result.Invoices.Count() == 0)
            {
                BusinessNotifier.AddWarning(BusinessWarningEnum.Default, "No invoice instances were found");
            }
            return result;
        }

        public InvoiceDtos FindInvoiceByMonthAndStatus(DateTime month, int status)
        {
            return ExecuteCommand(locator => FindInvoiceByMonthAndStatusCommand(locator, month, status));
        }
        private InvoiceDtos FindInvoiceByMonthAndStatusCommand(IRepositoryLocator locator, DateTime month, int status)
        {
            return FindInvoiceByMonthFilterCommand(locator, month, status, 0L, 0L);
        }


        public InvoiceDtos FindInvoiceByMonthFilter(DateTime month, int status, long cashierId, long therapistId)
        {
            return ExecuteCommand(locator => FindInvoiceByMonthFilterCommand(locator, month, status, cashierId, therapistId));
        }
        private InvoiceDtos FindInvoiceByMonthFilterCommand(IRepositoryLocator locator, DateTime month, int status, long cashierId, long therapistId)
        {
            var result = new InvoiceDtos { Invoices = new List<InvoiceDto>() };
            if (month == null)
            {
                return result;
            }
            var query = locator.FindAll<Invoice>().Where(i => i.DocDate.Month == month.Month &&
                                                              i.DocDate.Year == month.Year &&
                                                              i.Status == status);
            if (cashierId != 0L)
            {
                query = query.Where(i => i.CollectedBy.Id == cashierId);
            }
            if (therapistId != 0L)
            {
                query = query.Where(i => i.Transaction.Customer.Therapist.Id == therapistId);
            }
            var invoices = query.OrderBy(i => i.DocDate).ToList();
            result.Invoices = Mapper.Map<List<Invoice>, List<InvoiceDto>>(invoices);
            if (result.Invoices.Count() == 0)
            {
                BusinessNotifier.AddWarning(BusinessWarningEnum.Default, "No invoice instances were found");
            }
            return result;
        }

        public InvoiceDto UpdateInvoice(InvoiceDto dto)
        {
            return ExecuteCommand(locator => UpdateInvoiceCommand(locator, dto));
        }
        private InvoiceDto UpdateInvoiceCommand(IRepositoryLocator locator, InvoiceDto dto)
        {
            var instance = locator.GetById<Invoice>(dto.Id);
            instance.Update(locator, dto);
            return Invoice_to_Dto(instance);
        }

        public InvoiceDtos FindAllInvoices()
        {
            return ExecuteCommand(FindAllInvoiceCommand);
        }
        private InvoiceDtos FindAllInvoiceCommand(IRepositoryLocator locator)
        {
            var result = new InvoiceDtos { Invoices = new List<InvoiceDto>() };
            var invoices = locator.FindAll<Invoice>().ToList();
            result.Invoices = Mapper.Map<List<Invoice>, List<InvoiceDto>>(invoices);
            if (result.Invoices.Count() == 0)
            {
                BusinessNotifier.AddWarning(BusinessWarningEnum.Default, "No invoice instances were found");
            }
            return result;
        }

        public InvoiceDtos FindInvoiceByDate(DateTime date)
        {
            return ExecuteCommand(locator => FindInvoiceByDateCommand(locator, date));
        }
        private InvoiceDtos FindInvoiceByDateCommand(IRepositoryLocator locator, DateTime date)
        {
            var result = new InvoiceDtos { Invoices = new List<InvoiceDto>() };
            if (date == null)
            {
                return result;
            }
            var query = locator.FindAll<Invoice>().Where(t => DbFunctions.TruncateTime(t.DocDate) == date.Date);
            var invoices = query.ToList();
            result.Invoices = Mapper.Map<List<Invoice>, List<InvoiceDto>>(invoices);
            if (result.Invoices.Count() == 0)
            {
                BusinessNotifier.AddWarning(BusinessWarningEnum.Default, "No invoice instances were found");
            }
            return result;
        }

        public DtoResponse DeleteInvoice(long id)
        {
            return ExecuteCommand(locator => DeleteInvoiceCommand(locator, id));
        }
        private DtoResponse DeleteInvoiceCommand(IRepositoryLocator locator, long id)
        {
            var invoice = locator.GetById<Invoice>(id);
            BusinessNotifier.AddWarning(BusinessWarningEnum.Default,
                                        string.Format("Invoice [id:{0}] was deleted",
                                                      invoice.Id));

            locator.Remove(invoice);
            return new DtoResponse();
        }

        #endregion



        #region Invoice Row Members

        public InvoiceRowDto CreateNewInvoiceRow(InvoiceRowDto dto)
        {
            return ExecuteCommand(locator => CreateNewInvoiceRowCommand(locator, dto));
        }
        private InvoiceRowDto CreateNewInvoiceRowCommand(IRepositoryLocator locator, InvoiceRowDto dto)
        {
            var invoice = locator.GetById<Invoice>(dto.InvoiceId);
            return InvoiceRow_to_Dto(invoice.AddInvoiceItem(locator, dto));
        }

        public InvoiceRowDto GetInvoiceRowById(long id)
        {
            return ExecuteCommand(locator => InvoiceRow_to_Dto(locator.GetById<InvoiceRow>(id)));
        }

        public InvoiceRowDto UpdateInvoiceRow(InvoiceRowDto dto)
        {
            return ExecuteCommand(locator => UpdateInvoiceRowCommand(locator, dto));
        }
        private InvoiceRowDto UpdateInvoiceRowCommand(IRepositoryLocator locator, InvoiceRowDto dto)
        {
            var instance = locator.GetById<InvoiceRow>(dto.Id);
            instance.Update(locator, dto);
            return InvoiceRow_to_Dto(instance);
        }

        public DtoResponse DeleteInvoiceRow(long id)
        {
            return ExecuteCommand(locator => DeleteInvoiceRowCommand(locator, id));
        }
        private DtoResponse DeleteInvoiceRowCommand(IRepositoryLocator locator, long id)
        {
            var invoiceRow = locator.GetById<InvoiceRow>(id);
            BusinessNotifier.AddWarning(BusinessWarningEnum.Default,
                                        string.Format("InvoiceRow [id:{0}] was deleted",
                                                      invoiceRow.Id));

            locator.Remove(invoiceRow);
            return new DtoResponse();
        }

        #endregion


        #region Entity to Dto

        private TransactionDto Transaction_to_Dto(Transaction transaction)
        {
            return Mapper.Map<Transaction, TransactionDto>(transaction);          
        }

        private InvoiceDto Invoice_to_Dto(Invoice invoice)
        {
            return Mapper.Map<Invoice, InvoiceDto>(invoice);
        }

        private InvoiceRowDto InvoiceRow_to_Dto(InvoiceRow invoiceRow)
        {
            return Mapper.Map<InvoiceRow, InvoiceRowDto>(invoiceRow);
        }
        #endregion
    }
}
