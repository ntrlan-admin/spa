﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using eClinicSpa.Domain.TransManager;
using eClinicSpa.Domain.Repository;
using eClinicSpa.Common.Message;
using eClinicSpa.Domain.AppServices;

namespace eClinicSpa.Domain.Services
{
    /// <remarks>
    /// version 0.50 Chapter V: Service Locator
    /// version 0.71 Context Re-Factor
    /// </remarks>
    public class ServiceBase
    {

        protected TResult ExecuteCommand<TResult>(Func<IRepositoryLocator, TResult> command) 
            where TResult : class, IDtoResponseEnvelop
        {
            using (ITransManager manager = Container.GlobalContext.TransFactory.CreateManager())
            {
                return manager.ExecuteCommand(command);
            }
        }
    }
}
