using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using eClinicSpa.Domain.Entities;
using eClinicSpa.Domain.TransManager;

namespace eClinicSpa.Domain.Mappings
{
    public class ModelCreator:IModelCreator
    {
        #region Implementation of IModelCreator

        public void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Configurations.Add(new Customer.Mapping());
            modelBuilder.Configurations.Add(new Invoice.Mapping());

            modelBuilder.Entity<Employee>();
            modelBuilder.Entity<ConsultantStatusView>();
            modelBuilder.Entity<TherapistStatusView>();
            modelBuilder.Entity<LogInHistory>();
            modelBuilder.Entity<Customer>();
            modelBuilder.Entity<CustomerView>();
            modelBuilder.Entity<Transaction>();
            modelBuilder.Entity<CustomerTransactionView>();
            modelBuilder.Entity<Invoice>();
            modelBuilder.Entity<InvoiceRow>();
            modelBuilder.Entity<ProductCategory>();
            modelBuilder.Entity<Product>();
            modelBuilder.Entity<Address>();

            modelBuilder.Entity<TransactionView>();
        }

        #endregion
    }
}