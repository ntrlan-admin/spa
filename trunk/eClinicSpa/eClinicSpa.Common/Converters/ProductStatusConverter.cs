﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using eClinicSpa.Common.Constant;
using eClinicSpa.Common.Properties;

namespace eClinicSpa.Common.Converters
{

    public class ProductStatusConverter : IValueConverter
    {
        private static readonly ProductStatusConverter defaultInstance = new ProductStatusConverter();
        public static ProductStatusConverter Default { get { return defaultInstance; } }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int status = (int) value;
            try
            {
                return AllProductStatus.GetProductStatusDescription(status);
            }
            catch (Exception)
            {
                return String.Empty;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
