﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace eClinicSpa.Common.Converters
{
    public class NumberMatchToBooleanConverter : IValueConverter
    {
        private static readonly NumberMatchToBooleanConverter defaultInstance = new NumberMatchToBooleanConverter();

        public static NumberMatchToBooleanConverter Default { get { return defaultInstance; } }

        public object Convert(object value, Type targetType,
                              object parameter, CultureInfo culture)
        {
            if (value == null || parameter == null)
                return false;
            int targetValue = int.Parse(parameter.ToString());
            return value.Equals(targetValue);
        }

        public object ConvertBack(object value, Type targetType,
                                  object parameter, CultureInfo culture)
        {
            if (value == null || parameter == null)
                return null;

            bool useValue = (bool)value;
            if (useValue)
                return int.Parse(parameter.ToString());

            return null;
        }
    }
}
