﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace eClinicSpa.Common.Converters
{
    public class NullToBooleanConverter : IValueConverter
    {
        private static readonly NullToBooleanConverter defaultInstance = new NullToBooleanConverter();

        public static NullToBooleanConverter Default { get { return defaultInstance; } }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (value != null);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
