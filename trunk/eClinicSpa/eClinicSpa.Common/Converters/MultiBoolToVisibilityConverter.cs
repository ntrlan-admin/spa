﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace eClinicSpa.Common.Converters
{
    public class MultiBoolToVisibilityConverter : IMultiValueConverter
    {
        private static readonly MultiBoolToVisibilityConverter defaultInstance = new MultiBoolToVisibilityConverter();

        public static MultiBoolToVisibilityConverter Default { get { return defaultInstance; } }


        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null || values.Length < 2)
            {
                return DependencyProperty.UnsetValue;
            }

            bool returnValue = false;
            foreach (object value in values) returnValue = (returnValue || (bool)value);

            return returnValue ? Visibility.Visible : Visibility.Hidden;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
