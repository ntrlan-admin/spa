﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using eClinicSpa.Common.Constant;

namespace eClinicSpa.Common.Converters
{
    public class TransactionStatusToBooleanConverter : IValueConverter
    {
        private static readonly TransactionStatusToBooleanConverter defaultInstance = new TransactionStatusToBooleanConverter();

        public static TransactionStatusToBooleanConverter Default { get { return defaultInstance; } }

        public object Convert(object value, Type targetType,
                              object parameter, CultureInfo culture)
        {
            int status = (int)value;
            int targetStatus = int.Parse((string)parameter);
            if (targetStatus <= CustomerTransactionStatus.Incoming || targetStatus == CustomerTransactionStatus.Finished)
            {
                return status < targetStatus;
            }
            else
            {
                return status <= targetStatus;
            }
        }

        public object ConvertBack(object value, Type targetType,
                                  object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
