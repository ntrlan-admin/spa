﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using eClinicSpa.Common.Constant;
using eClinicSpa.Common.Properties;

namespace eClinicSpa.Common.Converters
{

    public class TransactionStatusConverter : IValueConverter
    {
        private static readonly TransactionStatusConverter defaultInstance = new TransactionStatusConverter();
        public static TransactionStatusConverter Default { get { return defaultInstance; } }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                int status = (int)value;
                int type = parameter == null ? 2 : int.Parse((string)parameter);
                return CustomerTransactionStatus.GetTransactionStatus(status, type);
            }
            catch (Exception)
            {
                return String.Empty;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
