﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using eClinicSpa.Common.Constant;
using eClinicSpa.Common.Properties;

namespace eClinicSpa.Common.Converters
{

    public class EmployeeStatusConverter : IValueConverter
    {
        private static readonly EmployeeStatusConverter defaultInstance = new EmployeeStatusConverter();
        public static EmployeeStatusConverter Default { get { return defaultInstance; } }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int status = (int) value;
            try
            {
                return AllEmployeeStatus.GetEmployeeStatusDescription(status);
            }
            catch (Exception)
            {
                return String.Empty;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
