﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using eClinicSpa.Common.Constant;
using eClinicSpa.Common.Properties;

namespace eClinicSpa.Common.Converters
{

    public class RolePermissionConverter : IValueConverter
    {
        private static readonly RolePermissionConverter defaultInstance = new RolePermissionConverter();
        public static RolePermissionConverter Default { get { return defaultInstance; } }

        Dictionary<int, IList<int>> RolePermissions = new Dictionary<int, IList<int>>()
        {
            { EmployeeRole.Receptionist,
                new List<int> { FunctionConstant.ReceptionOperator } },
            { EmployeeRole.Operator,
                new List<int> { FunctionConstant.ReceptionOperator } },
            { EmployeeRole.Cashier,
                new List<int> { FunctionConstant.Cashier } },
            { EmployeeRole.Staff, 
                new List<int> { FunctionConstant.ReceptionOperator } },
            { EmployeeRole.TreatmentConsultant, 
                new List<int> { FunctionConstant.ReceptionOperator, FunctionConstant.Consultant, 
                                FunctionConstant.StockReceiptProcess, FunctionConstant.Cashier } },
            { EmployeeRole.Manager,
                new List<int> { FunctionConstant.ReceptionOperator, FunctionConstant.Consultant, 
                                FunctionConstant.StockReceiptProcess, FunctionConstant.Cashier,
                                FunctionConstant.Product, FunctionConstant.Employee, FunctionConstant.Customer,
                                FunctionConstant.Report, FunctionConstant.Management
                } },
            { EmployeeRole.Therapist,
                new List<int> { FunctionConstant.ReceptionOperator, FunctionConstant.Consultant, 
                                FunctionConstant.StockReceiptProcess, FunctionConstant.Cashier } },
            { EmployeeRole.TreatmentConsultantAndTherapist, 
                new List<int> { FunctionConstant.ReceptionOperator, FunctionConstant.Consultant, 
                                FunctionConstant.StockReceiptProcess, FunctionConstant.Cashier } }
        };

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int role = (int) value;
            int function = int.Parse((String) parameter);
            try
            {
                return RolePermissions[role].Contains(function);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
