﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace eClinicSpa.Common.Converters
{
    public class TextToBooleanConverter : IValueConverter
    {
        private static readonly TextToBooleanConverter defaultInstance = new TextToBooleanConverter();

        public static TextToBooleanConverter Default { get { return defaultInstance; } }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (value != null);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return string.Empty;
        }
    }
}
