﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

using eClinicSpa.Common.Properties;

namespace eClinicSpa.Common.Converters
{
    public class PersonNameConverter : IMultiValueConverter
    {
        private static readonly PersonNameConverter defaultInstance = new PersonNameConverter();

        public static PersonNameConverter Default { get { return defaultInstance; } }


        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            Tuple<string, string> tuple = new Tuple<string, string>((string)values[0], (string)values[1]);
            return (object)tuple;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
