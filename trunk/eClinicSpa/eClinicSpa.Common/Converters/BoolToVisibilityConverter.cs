﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace eClinicSpa.Common.Converters
{
    public class BoolToVisibilityConverter : IValueConverter
    {
        private static readonly BoolToVisibilityConverter defaultInstance = new BoolToVisibilityConverter();

        public static BoolToVisibilityConverter Default { get { return defaultInstance; } }


        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int hiddenType = int.Parse((string) parameter);
            return (bool)value ? Visibility.Visible : (hiddenType == 1 ? Visibility.Collapsed : Visibility.Hidden);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
