﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace eClinicSpa.Common.Constant
{
    public class PaymentStatus
    {
        public const int NotPaid = 1;
        public const int Paying = 2;
        public const int Paid = 3;
        public const int PrepareStockOut = 4;
        public const int StockOut = 5;
    }
}
