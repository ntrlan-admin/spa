﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace eClinicSpa.Common.Constant
{
    public class ProductDiscountTypes
    {
        public static List<ProductDiscount> ProductDiscountList = new List<ProductDiscount>
            {
                new ProductDiscount { Type = 0, Rate = 0, Description = "" },
                new ProductDiscount { Type = 1, Rate = 0, Description = "Khác" },
                new ProductDiscount { Type = 2, Rate = 0.05M, Description = "Giảm 5%" },
                new ProductDiscount { Type = 3, Rate = 0.10M, Description = "Giảm 10%" },
                new ProductDiscount { Type = 4, Rate = 0.15M, Description = "Giảm 15%" },
                new ProductDiscount { Type = 5, Rate = 0.20M, Description = "Giảm 20%" },
                new ProductDiscount { Type = 6, Rate = 0.25M, Description = "Giảm 25%" },
                new ProductDiscount { Type = 7, Rate = 0.30M, Description = "Giảm 30%" },
                new ProductDiscount { Type = 8, Rate = 0.35M, Description = "Giảm 35%" },
                new ProductDiscount { Type = 9, Rate = 0.40M, Description = "Giảm 40%" },
                new ProductDiscount { Type = 10, Rate = 0.45M, Description = "Giảm 45%" },
                new ProductDiscount { Type = 11, Rate = 0.50M, Description = "Giảm 50%" },
                new ProductDiscount { Type = 12, Rate = 1, Description = "Miễn phí" }
            };

        public static string GetTypeDescription(decimal discountRate)
        {
            var result = ProductDiscountList.FirstOrDefault(d => d.Rate == discountRate);
            return result.Description;
        }
    }
}
