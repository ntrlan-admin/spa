﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace eClinicSpa.Common.Constant
{
    public class InvoiceConstant
    {
        public const int BILLING = 1;
        public const int REFERENCE = 2;
        public const int INVOICE = 3;
        public const int STOCK = 4;

        public const int PaymentType_Cash = 1;
        public const int PaymentType_VisaMaster = 2;
        public const int PaymentType_Funds = 3;
    }
}
