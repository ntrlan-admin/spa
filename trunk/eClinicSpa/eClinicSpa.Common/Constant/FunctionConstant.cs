﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace eClinicSpa.Common.Constant
{
    public class FunctionConstant
    {
        public static int ReceptionOperator = 1;
        public static int Consultant = 2;
        public static int StockReceiptProcess = 3;
        public static int Cashier = 4;
        public static int Product = 5;
        public static int Employee = 6;
        public static int Customer = 7;
        public static int Report = 8;
        public static int Management = 9;
    }
}
