﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace eClinicSpa.Common.Constant
{
    public class ProductStatus
    {
        public static int AllProducts = -1;
        public static int Inactive = 0;
        public static int Active = 1;

        public int Status { get; set; }
        public string Description { get; set; }
    }

    public class AllProductStatus
    {
        public static List<ProductStatus> ProductStatusList = new List<ProductStatus>
            {
                new ProductStatus { Status = 0, Description = "Không bán" },
                new ProductStatus { Status = 1, Description = "Đang bán" }
            };

        public static string GetProductStatusDescription(int status)
        {
            var result = ProductStatusList.Find(ps => ps.Status == status);
            return result == null ? String.Empty : result.Description;
        }
    }
}
