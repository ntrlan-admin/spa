﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace eClinicSpa.Common.Constant
{
    public class ProductDiscount
    {
        public int Type { get; set; }
        public decimal Rate { get; set; }
        public string Description { get; set; }
    }
}
