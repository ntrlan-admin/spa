﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace eClinicSpa.Common.Constant
{
    public class UserDistrict
    {
        public int Id { get; set; }
        public string ShortName { get; set; }
        public string FullName { get; set; }

        public static List<UserDistrict> UserDistrictList = new List<UserDistrict>()
            {
                new UserDistrict { Id = 1, ShortName = "Q.01", FullName = "Quận 1" },
                new UserDistrict { Id = 2, ShortName = "Q.02", FullName = "Quận 2" },
                new UserDistrict { Id = 3, ShortName = "Q.03", FullName = "Quận 3" },
                new UserDistrict { Id = 4, ShortName = "Q.04", FullName = "Quận 4" },
                new UserDistrict { Id = 5, ShortName = "Q.05", FullName = "Quận 5" },
                new UserDistrict { Id = 6, ShortName = "Q.06", FullName = "Quận 6" },
                new UserDistrict { Id = 7, ShortName = "Q.07", FullName = "Quận 7" },
                new UserDistrict { Id = 8, ShortName = "Q.08", FullName = "Quận 8" },
                new UserDistrict { Id = 9, ShortName = "Q.09", FullName = "Quận 9" },
                new UserDistrict { Id = 10, ShortName = "Q.10", FullName = "Quận 10" },
                new UserDistrict { Id = 11, ShortName = "Q.11", FullName = "Quận 11" },
                new UserDistrict { Id = 12, ShortName = "Q.12", FullName = "Quận 12" },
                new UserDistrict { Id = 13, ShortName = "Q.BTH", FullName = "Bình Thạnh" },
                new UserDistrict { Id = 14, ShortName = "Q.PN", FullName = "Phú Nhuận" },
                new UserDistrict { Id = 15, ShortName = "Q.GV", FullName = "Gò Vấp" },
                new UserDistrict { Id = 16, ShortName = "Q.TB", FullName = "Tân Bình" },
                new UserDistrict { Id = 17, ShortName = "Q.TĐ", FullName = "Thủ Đức" },
                new UserDistrict { Id = 18, ShortName = "Q.TP", FullName = "Tân Phú" },
                new UserDistrict { Id = 19, ShortName = "Q.BTA", FullName = "Bình Tân" },
                new UserDistrict { Id = 20, ShortName = "H.HM", FullName = "Hóc Môn" },
                new UserDistrict { Id = 21, ShortName = "H.CC", FullName = "Củ Chi" },
                new UserDistrict { Id = 22, ShortName = "H.BC", FullName = "Bình Chánh" }
            };

        public static string GetDistrictName(int DistrictNo)
        {
            var result = UserDistrictList.Find(r => r.Id == DistrictNo);
            return result == null ? String.Empty : result.FullName;
        }
    }
}
