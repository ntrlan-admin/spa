﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace eClinicSpa.Common.Constant
{
    public class CustomerTransactionStatus
    {
        public const int Cancelled = -1;
        public const int Unknown = 0;
        public const int Appointment = 1;
        public const int Incoming = 2;
        public const int Consulting = 3;
        public const int Treating = 4;
        public const int Finished = 5;

        public int StatusCode { get; set; }
        public string ShortDescription { get; set; }
        public string FullDescription { get; set; }

        public static List<CustomerTransactionStatus> TransactionStatusList = new List<CustomerTransactionStatus>
            {
                new CustomerTransactionStatus { StatusCode = Unknown, ShortDescription = "", FullDescription = "" },
                new CustomerTransactionStatus { StatusCode = Appointment, ShortDescription = "Hẹn", FullDescription = "Hẹn" },
                new CustomerTransactionStatus { StatusCode = Cancelled, ShortDescription = "Hủy", FullDescription = "Hủy" },
                new CustomerTransactionStatus { StatusCode = Incoming, ShortDescription = "Vào", FullDescription = "Vào" },
                new CustomerTransactionStatus { StatusCode = Consulting, ShortDescription = "TV", FullDescription = "Tư Vấn" },
                new CustomerTransactionStatus { StatusCode = Treating, ShortDescription = "ĐT", FullDescription = "Điều Trị" },
                new CustomerTransactionStatus { StatusCode = Finished, ShortDescription = "Về", FullDescription = "Về" }
            };

        public static string GetTransactionStatus(int status, int type)
        {
            var result = TransactionStatusList.Find(r => r.StatusCode == status);
            return result == null ? String.Empty : (type == 1 ? result.ShortDescription : result.FullDescription);
        }
    }
}
