﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace eClinicSpa.Common.Constant
{
    public class CustomerConstant
    {
        public static int GENDER_MALE = 1;
        public static int GENDER_FEMALE = 2;
    }
}
