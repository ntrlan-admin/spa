﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace eClinicSpa.Common.Constant
{
    public class PaymentType
    {
        public const int Cash = 1;
        public const int Card = 2;
        public const int Fund = 3;

        static Dictionary<int, string> PaymentTypes = new Dictionary<int, string>()
        {
            { Cash, "Tiền mặt" },
            { Card, "VISA/Master Card" },
            { Fund, "Chuyển khoản" }
        };

        public static string GetPaymentTypeDesc(int type) { return PaymentTypes[type]; }
    }
}
