﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace eClinicSpa.Common.Constant
{
    public class EmployeeRole
    {
        public static int Receptionist = 1;
        public static int Operator = 2;
        public static int Cashier = 3;
        public static int TreatmentConsultant = 4;
        public static int Manager = 5;
        public static int Therapist = 6;
        public static int TreatmentConsultantAndTherapist = 7;
        public static int Staff = 8;

        public static int ReceptionView = 1;
        public static int ConsultantView = 2;


        public int Code { get; set; }
        public string Description { get; set; }

        public static List<EmployeeRole> EmployeeRoleList = new List<EmployeeRole>
            {
                new EmployeeRole { Code = 1, Description = "Tiếp tân" },
                new EmployeeRole { Code = 2, Description = "Tổng đài" },
                new EmployeeRole { Code = 3, Description = "Thu ngân" },
                new EmployeeRole { Code = 4, Description = "Tư vấn" },
                new EmployeeRole { Code = 5, Description = "Quản lý" },
                new EmployeeRole { Code = 6, Description = "Điều trị viên" },
                new EmployeeRole { Code = 7, Description = "Tư vấn kiêm Điều trị viên" },
                new EmployeeRole { Code = 8, Description = "Nhân viên" }
            };

        public static string GetRoleName(int role)
        {
            var result = EmployeeRoleList.Find(r => r.Code == role);
            return result == null ? String.Empty : result.Description;
        }
    }
}
