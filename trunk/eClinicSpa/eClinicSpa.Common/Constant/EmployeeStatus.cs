﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace eClinicSpa.Common.Constant
{
    public class EmployeeStatus
    {
        public static int Inactive = 0;
        public static int Active = 1;

        public int Status { get; set; }
        public string Description { get; set; }
    }

    public class AllEmployeeStatus
    {
        public static List<EmployeeStatus> EmployeeStatusList = new List<EmployeeStatus>
            {
                new EmployeeStatus { Status = 0, Description = "Nghỉ việc" },
                new EmployeeStatus { Status = 1, Description = "Làm việc" }
            };

        public static string GetEmployeeStatusDescription(int status)
        {
            var result = EmployeeStatusList.Find(ps => ps.Status == status);
            return result == null ? String.Empty : result.Description;
        }
    }
}
