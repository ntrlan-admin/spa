﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace eClinicSpa.Common.Constant
{
    public enum ReportPrintPaperSize
    {
        A4,
        A6
    }
    public enum ReportTimeFrame
    {
        ByDate,
        ByMonth
    }
}
