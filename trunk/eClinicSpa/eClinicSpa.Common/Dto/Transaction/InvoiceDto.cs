﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.Common.Dto.Employee;
using eClinicSpa.Common.Message;
using eClinicSpa.Common.Constant;

namespace eClinicSpa.Common.Dto.Transaction
{
    /// <remarks>
    /// version 0.03 Chapter III:  The Response
    /// version 0.13 Chapter XIII: Business Domain Extension
    /// version 0.14 Chapter XIV:  Validation
    /// </remarks>
    public class InvoiceDto
        : ValidatorDtoBase
    {
        public InvoiceDto()
        {
            DocDate = DateTime.Now;
            InvoiceItems = new List<InvoiceRowDto>();
            Status = PaymentStatus.NotPaid;
            PaymentType = eClinicSpa.Common.Constant.PaymentType.Cash;
        }
        
        public long Id { get; set; }

        public TransactionDto Transaction { get; set; }
        public int DocType { get; set; }
        public DateTime DocDate { get; set; }
        public int PaymentType { get; set; }

        public Decimal CustomerDiscount { get; set; }
        public Decimal Deposit { get; set; }
        public DateTime? DepositDate { get; set; }
        public Decimal PaidAmount { get; set; }

        public int Status { get; set; }
        public EmployeeDto Creator { get; set; }
        public EmployeeDto CollectedBy { get; set; }

        public List<InvoiceRowDto> InvoiceItems { get; set; }

        public Decimal TotalAmount
        {
            get
            {
                Decimal _total = 0;
                if (InvoiceItems == null) return _total;
                foreach (InvoiceRowDto item in InvoiceItems)
                {
                    _total += item.TotalAmount;
                }
                return _total;
            }
        }
        public Decimal TotalPaidAmount
        {
            get
            {
                return TotalAmount - CustomerDiscount;
            }
        }
        public Decimal RemainAmount
        {
            get
            {
                return TotalAmount - CustomerDiscount - Deposit;
            }
        }
        public Decimal DebitAmount
        {
            get
            {
                return RemainAmount - PaidAmount;
            }
        }
        public bool IsPaidItems
        {
            get
            {
                foreach (InvoiceRowDto item in InvoiceItems)
                {
                    if (item.NotPaid == false)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public override bool Equals(object obj)
        {
            InvoiceDto other = obj as InvoiceDto;

            if (other != null && other.Id == this.Id)
            {
                return true;
            }

            return false;
        }
    }
}
