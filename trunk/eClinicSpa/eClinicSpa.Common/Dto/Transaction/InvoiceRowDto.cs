﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using eClinicSpa.Common.Dto.Product;
using eClinicSpa.Common.Dto.Employee;
using eClinicSpa.Common.Message;
using eClinicSpa.Common.Constant;

namespace eClinicSpa.Common.Dto.Transaction
{
    /// <remarks>
    /// version 0.03 Chapter III:  The Response
    /// version 0.13 Chapter XIII: Business Domain Extension
    /// version 0.14 Chapter XIV:  Validation
    /// </remarks>
    public class InvoiceRowDto
        : ValidatorDtoBase
    {
        public InvoiceRowDto()
        {
        }
        
        public long Id { get; set; }
        public long InvoiceId { get; set; }

        public int ItemNo { get; set; }
        [Required(ErrorMessage = "Product is required")]
        public ProductDto Product { get; set; }
        public int Quantity { get; set; }
        public Decimal UnitPrice { get; set; }
        public Decimal Amount { get; set; }
        public Decimal ProductDiscountRate { get; set; }
        public Decimal ProductDiscountAmount { get; set; }
        public bool NotPaid { get; set; }

        public bool IsSelected { get; set; }
        public Decimal TotalAmount { get { return Amount - ProductDiscountAmount; } }
        public String ProductDiscountType { get { return ProductDiscountTypes.GetTypeDescription(ProductDiscountRate); ; } }

        public override bool Equals(object obj)
        {
            InvoiceRowDto other = obj as InvoiceRowDto;

            if (other != null && other.Id == this.Id)
            {
                return true;
            }

            return false;
        }
    }
}
