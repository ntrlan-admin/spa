﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using eClinicSpa.Common.Message;

namespace eClinicSpa.Common.Dto.Transaction
{
    /// <remarks>
    /// version 0.3 Chapter III: The Response
    /// </remarks>
    public class InvoiceDtos
        :DtoBase
    {
        public IList<InvoiceDto> Invoices { get; set; }
    }
}
