﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.Common.Dto.Employee;
using eClinicSpa.Common.Message;
using eClinicSpa.Common.Constant;

namespace eClinicSpa.Common.Dto.Transaction
{
    /// <remarks>
    /// version 0.03 Chapter III:  The Response
    /// version 0.13 Chapter XIII: Business Domain Extension
    /// version 0.14 Chapter XIV:  Validation
    /// </remarks>
    public class TransactionDto
        : ValidatorDtoBase
    {
        public TransactionDto()
        {
            TransactionDate = DateTime.Now;
        }
        
        public long Id { get; set; }

        public DateTime TransactionDate { get; set; }
        public int Order { get; set; }
        public CustomerDto Customer { get; set; }
        public EmployeeDto InConsultant { get; set; }
        public EmployeeDto OutConsultant { get; set; }
        public EmployeeDto AlternativeTherapist { get; set; }
        public DateTime? TreatmentDate { get; set; }
        public DateTime? OutGoingDate { get; set; }
        
        public int Status { get; set; }
        public EmployeeDto Creator { get; set; }


        public override bool Equals(object obj)
        {
            TransactionDto other = obj as TransactionDto;

            if (other != null && other.Id == this.Id)
            {
                return true;
            }

            return false;
        }
    }
}
