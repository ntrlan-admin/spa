﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.Common.Dto.Employee;
using eClinicSpa.Common.Message;
using eClinicSpa.Common.Constant;

namespace eClinicSpa.Common.Dto.Transaction
{
    /// <remarks>
    /// version 0.03 Chapter III:  The Response
    /// version 0.13 Chapter XIII: Business Domain Extension
    /// version 0.14 Chapter XIV:  Validation
    /// </remarks>
    public class CustomerTransactionViewDto
        : ValidatorDtoBase
    {
        public CustomerTransactionViewDto()
        {
        }
        
        public long Id { get; set; }

        public DateTime TransactionDate { get; set; }
        public int Order { get; set; }
        public int Status { get; set; }

        public long CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public DateTime DayOfBirth { get; set; }
        public string Phone { get; set; }

        //public long? TreatmentConsultantId { get; set; }
        public string TreatmentConsultantName { get; set; }
        //public long? TherapistId { get; set; }
        public string TherapistName { get; set; }

        public int Age
        {
            get
            {
                return (DayOfBirth == null || DayOfBirth.Year < 1900 ? 0 : DateTime.Now.Year - DayOfBirth.Year);
            }
        }

        public String MaskedPhone
        {
            get
            {
                string maskedPhone = String.IsNullOrEmpty(Phone) ? String.Empty :
                    Phone.Length > 4 ? "..." + Phone.Substring(Phone.Length - 4, 4) : Phone;
                return (String.Empty.Equals(maskedPhone) ? String.Empty :
                    Phone.IndexOf("VIP", StringComparison.OrdinalIgnoreCase) >= 0 ? "VIP" + maskedPhone : maskedPhone);
            }
        }

        public override bool Equals(object obj)
        {
            CustomerTransactionViewDto other = obj as CustomerTransactionViewDto;

            if (other != null && other.Id == this.Id)
            {
                return true;
            }

            return false;
        }
    }
}
