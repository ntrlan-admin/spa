﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using eClinicSpa.Common.Dto.Address;
using eClinicSpa.Common.Message;
using eClinicSpa.Common.Constant;
using eClinicSpa.Common.Dto.Employee;

namespace eClinicSpa.Common.Dto.Customer
{
    /// <remarks>
    /// version 0.03 Chapter III:  The Response
    /// version 0.13 Chapter XIII: Business Domain Extension
    /// version 0.14 Chapter XIV:  Validation
    /// </remarks>
    public class CustomerDto
        : ValidatorDtoBase
    {
        public CustomerDto()
        {
            Gender = CustomerConstant.GENDER_FEMALE;
            DayOfBirth = new DateTime(DateTime.Now.Year - 20, 1, 1);
            Addresses = new List<AddressDto>();
        }
        
        public long Id { get; set; }

        [Required(ErrorMessage = "First Name is required")]
        [StringLength(50, ErrorMessage = "First Name is 50 chars max")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Last Name is required")]
        public string LastName { get; set; }

        public int Gender { get; set; }
        [Required(ErrorMessage = "Birthday is required")]
        public DateTime DayOfBirth { get; set; }
        public string CurrentAddress { get; set; }
        public int DistrictNo { get; set; }
        [Required(ErrorMessage = "Phone is required")]
        public string Phone { get; set; }
        public string Email { get; set; }
        public bool Vip { get; set; }
        public bool FellowPartner { get; set; }
        public bool NotUseCream { get; set; }
        public string UrgentNote { get; set; }
        public string Note { get; set; }
        public String MedicalRecord { get; set; }
        public string CardNumber { get; set; }
        public string VipCardNumber { get; set; }
        public string PictureNoBefore { get; set; }
        public string PictureNoAfter { get; set; }
        public bool KnownByAdvertise { get; set; }
        public bool KnownByWeb { get; set; }
        public CustomerDto ReferralCustomer { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = true)]
        public string KnownByOther { get; set; }
        public EmployeeDto TreatmentConsultant { get; set; }
        public EmployeeDto Therapist { get; set; }

        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public List<AddressDto> Addresses { get; set; }

        public int Age
        {
            get
            {
                return (DayOfBirth == null || DayOfBirth.Year < 1900 ? 0 : DateTime.Now.Year - DayOfBirth.Year);
            }
        }

        public String PhotoPath
        {
            get
            {
                string PhotoPathMale = "/eClinicSpa.WPF;component/Resources/Images/profile-placeholder-male.jpg";
                string PhotoPathFemale = "/eClinicSpa.WPF;component/Resources/Images/profile-placeholder-female.jpg";
                return (Gender == 1 ? PhotoPathMale : PhotoPathFemale);
            }
        }

        public String MaskedPhone
        {
            get
            {
                string maskedPhone = String.IsNullOrEmpty(Phone) ? String.Empty :
                    Phone.Length > 4 ? "..." + Phone.Substring(Phone.Length - 4, 4) : Phone;
                return (String.Empty.Equals(maskedPhone) ? String.Empty : 
                    Phone.IndexOf("VIP", StringComparison.OrdinalIgnoreCase) >= 0 ? "VIP" + maskedPhone : maskedPhone);
            }
        }

        public String MaskedAddress
        {
            get
            {
                string masked = String.IsNullOrEmpty(CurrentAddress) ? String.Empty : CurrentAddress.Trim();
                int index = masked.IndexOf(" ");
                string maskedAddress = index > 0 ? masked.Substring(index).TrimStart() : masked;
                return maskedAddress;
            }
        }

        public override bool Equals(object obj)
        {
            CustomerDto other = obj as CustomerDto;

            if (other != null && other.Id == this.Id)
            {
                return true;
            }

            return false;
        }
    }
}
