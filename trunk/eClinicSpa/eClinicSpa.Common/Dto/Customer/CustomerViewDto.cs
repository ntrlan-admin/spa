﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using eClinicSpa.Common.Dto.Customer;
using eClinicSpa.Common.Dto.Employee;
using eClinicSpa.Common.Message;
using eClinicSpa.Common.Constant;

namespace eClinicSpa.Common.Dto.Customer
{
    /// <remarks>
    /// version 0.03 Chapter III:  The Response
    /// version 0.13 Chapter XIII: Business Domain Extension
    /// version 0.14 Chapter XIV:  Validation
    /// </remarks>
    public class CustomerViewDto
        : ValidatorDtoBase
    {
        public CustomerViewDto()
        {
        }
        
        public long Id { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Phone { get; set; }
        public int Gender { get; set; }
        public DateTime DayOfBirth { get; set; }
        public string CurrentAddress { get; set; }
        public int DistrictNo { get; set; }
        public bool Vip { get; set; }
        public string VipCardNumber { get; set; }
        public string CardNumber { get; set; }

        public long TreatmentConsultantId { get; set; }
        public string TreatmentConsultantName { get; set; }
        public long TherapistId { get; set; }
        public string TherapistName { get; set; }

        public String DistrictName { get { return UserDistrict.GetDistrictName(DistrictNo); } }

        public String MaskedPhone
        {
            get
            {
                string maskedPhone = String.IsNullOrEmpty(Phone) ? String.Empty :
                    Phone.Length > 4 ? "..." + Phone.Substring(Phone.Length - 4, 4) : Phone;
                return (String.Empty.Equals(maskedPhone) ? String.Empty :
                    Phone.IndexOf("VIP", StringComparison.OrdinalIgnoreCase) >= 0 ? "VIP" + maskedPhone : maskedPhone);
            }
        }

        public override bool Equals(object obj)
        {
            CustomerViewDto other = obj as CustomerViewDto;

            if (other != null && other.Id == this.Id)
            {
                return true;
            }

            return false;
        }
    }
}
