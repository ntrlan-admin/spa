﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using eClinicSpa.Common.Message;

namespace eClinicSpa.Common.Dto.Product
{
    /// <summary>
    /// version 0.13 - Chapter XIII - Business Domain Extension
    /// </summary>
    public class ProductDtos
        :DtoBase
    {
        public IList<ProductDto> Products { get; set; }
    }

}
