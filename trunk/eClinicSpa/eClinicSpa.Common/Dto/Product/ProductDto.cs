﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using eClinicSpa.Common.Dto.Employee;
using eClinicSpa.Common.Dto.ProductCategory;
using eClinicSpa.Common.Message;

namespace eClinicSpa.Common.Dto.Product
{
    /// <summary>
    /// version 0.13 Chapter XIII: Business Domain Extension
    /// version 0.14 Chapter XIV:  Validation
    /// </summary>
    public class ProductDto
        : ValidatorDtoBase
    {
        public long Id { get; set; }
        public ProductCategoryDto Category { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [StringLength(500, ErrorMessage = "Name is 500 chars max")]
        public string Name { get; set; }

        public Decimal Price { get; set; }
        public int Status { get; set; }

        public DateTime? CreatedOn { get; set; }
        public EmployeeDto CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public EmployeeDto ModifiedBy { get; set; }

        public override bool Equals(object obj)
        {
            ProductDto other = obj as ProductDto;

            if (other != null && other.Id == this.Id)
            {
                return true;
            }
            return false;
        }
    }
}
