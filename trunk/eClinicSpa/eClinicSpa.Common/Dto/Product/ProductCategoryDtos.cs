﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using eClinicSpa.Common.Message;

namespace eClinicSpa.Common.Dto.ProductCategory
{
    /// <summary>
    /// version 0.13 - Chapter XIII - Business Domain Extension
    /// </summary>
    public class ProductCategoryDtos
        :DtoBase
    {
        public IList<ProductCategoryDto> ProductCategories { get; set; }
    }

}
