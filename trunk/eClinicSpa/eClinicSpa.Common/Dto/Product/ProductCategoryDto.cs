﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using eClinicSpa.Common.Message;

namespace eClinicSpa.Common.Dto.ProductCategory
{
    /// <summary>
    /// version 0.13 Chapter XIII: Business Domain Extension
    /// version 0.14 Chapter XIV:  Validation
    /// </summary>
    public class ProductCategoryDto
        : ValidatorDtoBase
    {
        public long Id { get; set; }
        //public long SuperId { get; set; }

        [Required(ErrorMessage = "Code is required")]
        [StringLength(100, ErrorMessage = "Code is 100 chars max")]
        public string Code { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [StringLength(400, ErrorMessage = "Name is 400 chars max")]
        public string Name { get; set; }

        public override bool Equals(object obj)
        {
            ProductCategoryDto other = obj as ProductCategoryDto;

            if (other != null && other.Id == this.Id)
            {
                return true;
            }

            return false;
        }
    }
}
