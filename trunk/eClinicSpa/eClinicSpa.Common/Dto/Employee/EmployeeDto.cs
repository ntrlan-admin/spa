﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using eClinicSpa.Common.Dto.Address;
using eClinicSpa.Common.Message;
using eClinicSpa.Common.Constant;

namespace eClinicSpa.Common.Dto.Employee
{
    /// <remarks>
    /// version 0.03 Chapter III:  The Response
    /// version 0.13 Chapter XIII: Business Domain Extension
    /// version 0.14 Chapter XIV:  Validation
    /// </remarks>
    public class EmployeeDto
        : ValidatorDtoBase
    {
        public EmployeeDto()
        {
            Status = EmployeeStatus.Active;
        }
        
        public long Id { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [StringLength(300, ErrorMessage = "Name is 300 chars max")]
        public string Name { get; set; }

        public string CurrentAddress { get; set; }
        public string District { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public int EmployeeType { get; set; }
        public int Status { get; set; }
        [StringLength(200, ErrorMessage = "User Name is 200 chars max")]
        public string UserName { get; set; }
        [StringLength(300, ErrorMessage = "Password is 300 chars max")]
        public string Password { get; set; }


        public string RoleName { get { return EmployeeRole.GetRoleName(EmployeeType); } }

        public override bool Equals(object obj)
        {
            EmployeeDto other = obj as EmployeeDto;

            if (other != null && other.Id == this.Id)
            {
                return true;
            }

            return false;
        }
    }
}
