﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using eClinicSpa.Common.Message;

namespace eClinicSpa.Common.Dto.Employee
{
    /// <remarks>
    /// version 0.3 Chapter III: The Response
    /// </remarks>
    public class EmployeeDtos
        :DtoBase
    {
        public IList<EmployeeDto> Employees { get; set; }
    }
}
