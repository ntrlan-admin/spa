﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using eClinicSpa.Common.Dto.Address;
using eClinicSpa.Common.Message;
using eClinicSpa.Common.Constant;

namespace eClinicSpa.Common.Dto.Employee
{
    /// <remarks>
    /// version 0.03 Chapter III:  The Response
    /// version 0.13 Chapter XIII: Business Domain Extension
    /// version 0.14 Chapter XIV:  Validation
    /// </remarks>
    public class TherapistStatusViewDto
        : ValidatorDtoBase
    {
        public TherapistStatusViewDto()
        {
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public int CountCustomer { get; set; }

        public bool IsBusy { get { return (CountCustomer > 0); } }

        public override bool Equals(object obj)
        {
            EmployeeDto other = obj as EmployeeDto;

            if (other != null && other.Id == this.Id)
            {
                return true;
            }

            return false;
        }
    }
}
