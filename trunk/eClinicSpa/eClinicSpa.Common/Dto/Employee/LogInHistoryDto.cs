﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using eClinicSpa.Common.Dto.Address;
using eClinicSpa.Common.Message;
using eClinicSpa.Common.Constant;

namespace eClinicSpa.Common.Dto.Employee
{
    /// <remarks>
    /// version 0.03 Chapter III:  The Response
    /// version 0.13 Chapter XIII: Business Domain Extension
    /// version 0.14 Chapter XIV:  Validation
    /// </remarks>
    public class LogInHistoryDto
        : ValidatorDtoBase
    {
        public LogInHistoryDto()
        {
        }
        
        public long Id { get; set; }

        public DateTime LogInDate { get; set; }
        public EmployeeDto LogInUser { get; set; }


        public override bool Equals(object obj)
        {
            LogInHistoryDto other = obj as LogInHistoryDto;

            if (other != null && other.Id == this.Id)
            {
                return true;
            }

            return false;
        }
    }
}
