﻿using Spring.Context.Support;

namespace eClinicSpa.Common.DependencyInjection
{
    /// <remarks>
    /// version 0.10 Chapter X: Dependency Injection
    /// </remarks>
    public class DiContext
    {
        public static XmlApplicationContext AppContext { get; set; }
    }
}
