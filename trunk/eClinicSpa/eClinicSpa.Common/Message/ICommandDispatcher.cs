﻿using System;
using eClinicSpa.Common.ServiceContract;

namespace eClinicSpa.Common.Message
{
    public interface ICommandDispatcher
    {
        TResult ExecuteCommand<TService, TResult>(Func<TService, TResult> command)
            where TResult : IDtoResponseEnvelop
            where TService : class, IContract;

    }
}