﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

using eClinicSpa.Common.Dto.Employee;
using eClinicSpa.Common.Message;
using eClinicSpa.Common.Filter;

namespace eClinicSpa.Common.ServiceContract
{
    /// <summary>
    /// Exposes the customer services
    /// </summary>
    /// <remarks>
    /// version 0.03 Chapter III:  The Response
	/// version 0.07 Chapter VII:  Contract Locator
    /// version 0.13 Chapter XIII: Business Domain Extension
    /// </remarks>
    [ServiceContract(Namespace = "http://eclinicspawcf/employeeservices/")]
    public interface IEmployeeService
        :IContract
    {

        [OperationContract]
        EmployeeDto CreateNewEmployee(EmployeeDto employee);

        [OperationContract]
        EmployeeDto GetById(long id);
        [OperationContract]
        EmployeeDtos GetByCredential(string username, string password);

        [OperationContract]
        EmployeeDto UpdateEmployee(EmployeeDto employee);

        [OperationContract]
        EmployeeDtos FindAll();
        [OperationContract]
        EmployeeDtos FindByFilter(string SearchFilter);
        [OperationContract]
        EmployeeDtos FindAllConsultant();
        [OperationContract]
        EmployeeDtos FindAllTherapist();
        [OperationContract]
        EmployeeDtos FindAllCashier();

        [OperationContract]
        DtoResponse DeleteEmployee(long id);

        [OperationContract]
        LogInHistoryDto CreateNewLogInHistory(LogInHistoryDto logInRecord);
        [OperationContract]
        LogInHistoryDtos FindLogInHistory(DateTime from, DateTime to);

        [OperationContract]
        ConsultantStatusViewDtos GetConsultantStatusView();
        [OperationContract]
        TherapistStatusViewDtos GetTherapistStatusView();
    }
}
