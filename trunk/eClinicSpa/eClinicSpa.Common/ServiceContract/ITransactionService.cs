﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

using eClinicSpa.Common.Dto.Transaction;
using eClinicSpa.Common.Message;
using eClinicSpa.Common.Filter;

namespace eClinicSpa.Common.ServiceContract
{
    /// <summary>
    /// Exposes the customer services
    /// </summary>
    /// <remarks>
    /// version 0.03 Chapter III:  The Response
	/// version 0.07 Chapter VII:  Contract Locator
    /// version 0.13 Chapter XIII: Business Domain Extension
    /// </remarks>
    [ServiceContract(Namespace = "http://eclinicspawcf/transactionservices/")]
    public interface ITransactionService
        :IContract
    {

        // Transaction
        [OperationContract]
        TransactionDto CreateNewTransaction(TransactionDto transaction);

        [OperationContract]
        TransactionDto GetById(long id);

        [OperationContract]
        TransactionDto UpdateTransaction(TransactionDto transaction);

        [OperationContract]
        TransactionDtos FindAll();
        [OperationContract]
        TransactionDtos FindByDate(DateTime date);
        [OperationContract]
        CustomerTransactionViewDtos FindCustomerTransactionByDate(DateTime date, CustomerTransactionSearchFilter searchFilter);
        [OperationContract]
        TransactionDtos GetTotalNumberByDate(DateTime date);

        [OperationContract]
        DtoResponse DeleteTransaction(long id);


        // Invoice
        [OperationContract]
        InvoiceDto CreateNewInvoice(InvoiceDto invoice);

        [OperationContract]
        InvoiceDto GetInvoiceById(long id);
        [OperationContract]
        InvoiceDto GetInvoiceByTransactionAndType(long transactionId, int docType);
        [OperationContract]
        InvoiceDtos GetInvoiceListByTransactionAndType(long transactionId, int docType);

        [OperationContract]
        InvoiceDtos FindInvoiceByDateAndStatus(DateTime date, int status);
        [OperationContract]
        InvoiceDtos FindInvoiceByFilter(DateTime date, int status, long cashierId, long therapistId);
        
        [OperationContract]
        InvoiceDtos FindInvoiceByMonthAndStatus(DateTime month, int status);
        [OperationContract]
        InvoiceDtos FindInvoiceByMonthFilter(DateTime month, int status, long cashierId, long therapistId);

        [OperationContract]
        InvoiceDto UpdateInvoice(InvoiceDto invoice);

        [OperationContract]
        InvoiceDtos FindAllInvoices();
        [OperationContract]
        InvoiceDtos FindInvoiceByDate(DateTime date);

        [OperationContract]
        DtoResponse DeleteInvoice(long id);


        // Invoice Row
        [OperationContract]
        InvoiceRowDto CreateNewInvoiceRow(InvoiceRowDto invoiceRow);

        [OperationContract]
        InvoiceRowDto GetInvoiceRowById(long id);

        [OperationContract]
        InvoiceRowDto UpdateInvoiceRow(InvoiceRowDto invoiceRow);

        [OperationContract]
        DtoResponse DeleteInvoiceRow(long id);
    }
}
