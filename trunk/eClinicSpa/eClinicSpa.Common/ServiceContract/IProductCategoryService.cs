﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using eClinicSpa.Common.Dto.Product;
using eClinicSpa.Common.Dto.ProductCategory;

namespace eClinicSpa.Common.ServiceContract
{
    /// <summary>
    /// version 0.13 Chapter XIII: Business Domain Extension
    /// </summary>
    [ServiceContract(Namespace = "http://eclinicspawcf/productcategoryservices/")]
    public interface IProductCategoryService
        :IContract
    {
        [OperationContract]
        ProductDto GetProductById(long id);

        [OperationContract]
        ProductDtos FindAllProduct();
        [OperationContract]
        ProductDtos FindAllActiveProduct();

        [OperationContract]
        ProductDtos FindByFilter(string SearchFilter);

        [OperationContract]
        ProductDto UpdateProduct(ProductDto product);

        [OperationContract]
        ProductDto CreateNewProduct(ProductDto product);

        
        [OperationContract]
        ProductCategoryDto GetProductCategoryById(long id);

        [OperationContract]
        ProductCategoryDtos FindAllProductCategory();

        [OperationContract]
        ProductCategoryDto UpdateProductCategory(ProductCategoryDto category);

        [OperationContract]
        ProductCategoryDto CreateNewProductCategory(ProductCategoryDto category);
    }
}
