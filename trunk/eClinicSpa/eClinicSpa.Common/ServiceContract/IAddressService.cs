﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using eClinicSpa.Common.Dto.Address;

namespace eClinicSpa.Common.ServiceContract
{
    /// <summary>
    /// version 0.13 Chapter XIII: Business Domain Extension
    /// </summary>
    [ServiceContract(Namespace = "http://eclinicspawcf/addressservices/")]
    public interface IAddressService
        :IContract
    {
        [OperationContract]
        AddressDto GetById(long id);

        [OperationContract]
        AddressDto UpdateAddress(AddressDto address);

        [OperationContract]
        AddressDto CreateNewAddress(AddressDto address);
    }
}
