﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using eClinicSpa.Common.Dto.Customer;

namespace eClinicSpa.Common.Filter
{
    /// <remarks>
    /// version 0.06 Chapter VI:   MVVM Pattern
    /// version 0.13 Chapter XIII: Async service methods
    /// </remarks> 
    public class CustomerSearchFilter
    {
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Phone { get; set; }

        public bool IsEmpty
        {
            get
            {
                return String.IsNullOrWhiteSpace(this.FirstName) && String.IsNullOrWhiteSpace(this.LastName)
                    && String.IsNullOrWhiteSpace(this.Phone);
            }
        }
    }
}
