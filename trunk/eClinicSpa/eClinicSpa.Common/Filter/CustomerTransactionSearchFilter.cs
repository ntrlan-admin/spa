﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using eClinicSpa.Common.Constant;
using eClinicSpa.Common.Dto.Customer;

namespace eClinicSpa.Common.Filter
{
    /// <remarks>
    /// version 0.06 Chapter VI:   MVVM Pattern
    /// version 0.13 Chapter XIII: Async service methods
    /// </remarks> 
    public class CustomerTransactionSearchFilter
    {
        public int Status { get; set; }

        public bool IsEmpty
        {
            get
            {
                return Status == CustomerTransactionStatus.Unknown;
            }
        }
    }
}
